<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//後台
define('HEAD_TITLE', '腦動能評量 | 後台管理系統');
define('LOGIN_TITLE', '腦動能評量 | 後台管理系統');
define('FOOTER_COMPANY', ' Mind Testing');
define('FOOTER_COMPANY_URL', '#');
define('FOOTER_NEXT_YEAR', date('Y'));
define('FOOTER_YEAR', '2020');
define('FOOTER_COPY', ' &copy; ');
define('FOOTER_COPY_TEXT', ' Backend System ');		//Copyright 
define('FOOTER_COMPANY_NUMBER', '');
define('FOOTER_COMPANY_NAME', '');
define('FOOTER_ADDRESS', '');
define('FOOTER_EMAIL', 'E-mail：');
define('FOOTER_PHONE', '電話：');
define('FOOTER_FAX', '傳真：');
define('FOOTER_SRTING', '');
date_default_timezone_set('Asia/Taipei');  

$config['keyFileName'] = array('wanda_bms.key', 'wanda_bms_hmac.key');