<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exportexcel extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/bonuscalculation_model');
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/organization_model');
		$this->load->model('amadis_sys/employee_model');
		$this->load->model('amadis_sys/occupation_model');
		$this->load->model('amadis_sys/order_model');
		$this->load->model('amadis_sys/stock_model');
		$this->load->model('amadis_sys/profit_model');
		$this->load->model('amadis_sys/bank_model');
		$this->load->model('amadis_sys/branch_model');
		$this->load->model('amadis_sys/post_model');
		$this->load->model('amadis_sys/order_info_model');
		$this->load->model('amadis_sys/customers_model');
		$this->load->model('amadis_sys/bonus_model');
		$this->load->model('amadis_sys/users_bonus_model');
		$this->load->model('amadis_sys/users_bonus_extra_model');
		$this->load->model('amadis_sys/goods_model');
		$this->load->model('amadis_sys/api/exportexcel_model');
		$this->unitName = '獎金報表管理';

		$this->session->set_flashdata('sidebarselected','bonussummary');
		$this->session->set_flashdata('mainsidebar','bounsreport');

		$this->competence_id = $this->session->userdata('competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);
		
		$admin_id = $this->session->userdata('users_id');
		$username = $this->session->userdata('users_name');
		
		if(!$admin_id){
			redirect('home/login');
		}
	}

	//營業處總獎金總表輸出
	public function orgexport2(){
		ini_set("max_execution_time", "60");
		$competence_id = $this->session->userdata('competence_id');
		$rightid = $this->competence_model->getaction($competence_id);
		foreach($rightid as $r){
			if($r['sidebar_sub_id'] == 37)
			{
				if($r['actions_export'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有匯出的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('bonussummary');
				}
			}
		}
		$data = [];
		$data_arr = [];
		$product_info = [];
		$selection['status'] = 1;
		$data['organization'] = $this->organization_model->getList($selection);
		$data['occupation'] = $this->occupation_model->getList($selection);
		$product = $this->goods_model->getList($selection);

		$year = $this->input->get('year');
		$month = $this->input->get('month');
		$currentmonth = $year.'-'.$month;
		$selectdate = $currentmonth;
		$keyword['year'] = $year;
		$keyword['month'] = $month;

		if($this->input->get('year') == '' && $this->input->get('month') == ''){
			$currentmonth = date("Y-m",time());
			$keyword['year'] = date("Y",time());
			$keyword['month'] = date("m",time());
			$year = date("Y",time());
			$month = date("m",time());
			$selectdate = '';
		}

		//報表日期
		$date['year'] = $year;
		$date['month'] = $month;

		//所有商品名稱
		foreach($product as $key => $productlist){
			$product_info[$key]['product_name'] = $productlist['product_name'];
		}

		$rowcount = 3;
		foreach($data['organization'] as $index => $orgid){
			if($competence_id == 3){
				if($orgid['organization_id'] != $this->session->userdata('manager_id'))
				{
					continue;
				}
				$rowcount = 3;
			}
			$orgpv = 0;
			$ordernum = 0;
			$id = $orgid['organization_id'];
			$data['certainone'] = $this->organization_model->getData($id);
             //業績獎金計算
			$performance = $this->users_bonus_model->org_total_1($id,$currentmonth);
			if($performance['total'] == null){
				$data['performance'] = 0;
			}
			else{
				$data['performance'] = $performance['total'];
			}
		    //業績獎金計算

		    //服務津貼
			$option['organization_id'] = $data['certainone']['organization_id'];
			$totalallowance = $this->users_bonus_model->count_allowance($option,$selectdate);
		    //服務津貼
			$data['totalallowance'] = round($totalallowance);
		    //服務津貼

		   //行政津貼
			$susidy = $this->users_bonus_model->org_total_6($id,$currentmonth);
			$data['subsidypv'] = $susidy['total'];
			if($susidy['total'] == null){
				$data['subsidypv'] = 0;
			}
			else{
				$data['subsidypv'] = $susidy['total'];
			}
		    //行政津貼

		   //衍生營業處津貼
			$boAllowance = $this->users_bonus_model->org_total_7($id,$currentmonth);
			$data['boAllowance'] = $boAllowance['total'];
			if($boAllowance['total'] == null){
				$data['boAllowance'] = 0;
			}
			else{
				$data['boAllowance'] = $boAllowance['total'];
			}
		    //衍生營業處津貼計算

		    //全國分紅
		    //全國當月總PV
			$nationalpv = 0;
			$natorganizanum = 0;
			$orgcount = 0;
			$data['dividend'] = 0;
			if(isset($data['certainone']['organization_id'])){
				$pv=0;
			    //該營業處當月單數
				$ordernum = 0;
			    //該營業處當月PV(ok)

			    //單數(承購單位)
				foreach($data['organization'] as $organiz){
					$organizid = $organiz['organization_id'];
				    //各營業處當月總PV
					${"eachpv".$organizid}=0;
					$orgpv = $this->order_info_model->get_org_totalpv($organizid,$currentmonth);
					${"eachpv".$organizid} = $orgpv['monthpv'];
					$nationalpv = $nationalpv + $orgpv['monthpv'];
					if(${"eachpv".$organizid} > 5000000){
						$orgcount = $this->order_info_model->get_org_totalcount($organizid,$currentmonth);
						$natorganizanum = $natorganizanum + $orgcount['total'];
					}
					if($organizid == $data['certainone']['organization_id']){
						$pv = $orgpv['monthpv'];
						$ordernum = $orgcount['total'];
					}
				}

				if($natorganizanum != 0 && $pv > 5000000){
					$data['dividend'] = round($nationalpv*0.01*$ordernum/$natorganizanum);
				}
				else{
					$data['dividend'] = 0;
				}
			}
		    //全國分紅

		    //差額獎金
			$difference = $this->users_bonus_model->org_total_3($id,$currentmonth);
			if($difference['total'] == null){
				$allowance = 0;
			}
			else{
				$allowance = $difference['total'];
			}
		    //差額獎金

		    //同階獎金
			$samelvallowancelist = $this->users_bonus_model->org_total_4($id,$currentmonth);
			if($samelvallowancelist['total'] == null){
				$samelvallowance = 0;
			}
			else{
				$samelvallowance = $samelvallowancelist['total'];
			}
	     	//同階獎金

		    //營業處總pv
			$orgpv = 0;
			$ordernum = 0;

		    //查詢各商品銷售單位
			foreach($product as $productlist){
				${'productnum'.$productlist['product_id']} = 0;
			}

			$keyword['organization_id'] = $data['certainone']['organization_id'];
			$orderlist = $this->order_model->getselectorder($keyword);
			foreach($orderlist as $order){
				$orderinfo = $this->order_info_model->getcertaininfo($order['order_id']);
				foreach($orderinfo as $infolist){
					foreach($product as $productlist){
						if($infolist['product_id'] == $productlist['product_id']){
							${'productnum'.$productlist['product_id']} = ${'productnum'.$productlist['product_id']} + $infolist['order_info_count'];
							$orgpv = $orgpv + $infolist['order_info_count'] * $productlist['product_a_pv'];
							$ordernum = $ordernum + $infolist['order_info_count'];
						}
					}
				}
			}

			$data['addtxt'] = "";
			$data['addmoney'] = 0;
			$data['lesstxt'] = "";
			$data['lessmoney'] = 0;
			$orgextrabonuslist = $this->users_bonus_extra_model->getOrgExtraBonus($orgid['organization_id']);
			foreach($orgextrabonuslist as $row){
				if(substr($row['users_bonus_extra_created_date'],0,7) == $currentmonth){
					$data['addtxt'] = $row['users_bonus_extra_add_txt'];
					$data['addmoney'] = $row['users_bonus_extra_add_money'];
					$data['lesstxt'] = $row['users_bonus_extra_less_txt'];
					$data['lessmoney'] = $row['users_bonus_extra_less_money'];
				}
			}

			$totalbonusnum = $data['performance'] + $samelvallowance + $data['boAllowance'] + round($totalallowance) + $allowance + $data['dividend'] + $data['subsidypv'] + $data['addmoney'] - $data['lessmoney'];
		    //查詢各商品銷售單位

			$acsnum = 66;
			$productrow = 0;
			foreach($product as $productlist){
				$productrow++;
			}
			$lastrow = $acsnum + $productrow + 13;

			//每項商品數量
			foreach($product as $key => $productlist){
				$product_info[$key][$index]['product_num'] = ${'productnum'.$productlist['product_id']};
			}

			//營業處名稱
			$data_arr[$index]['organization_name'] = $orgid['organization_name'];
			//總單位
			$data_arr[$index]['ordernum'] = $ordernum;
			//總PV
			$data_arr[$index]['orgpv'] = $orgpv;
			//業績獎金
			$data_arr[$index]['performance'] = $data['performance'];
			//同階獎金
			$data_arr[$index]['samelvallowance'] = $samelvallowance;
			//全國分紅
			$data_arr[$index]['dividend'] = $data['dividend'];
			//衍生營業處津貼
			$data_arr[$index]['boAllowance'] = $data['boAllowance'];
			//服務津貼
			$data_arr[$index]['totalallowance'] = $totalallowance;
			//行政津貼
			$data_arr[$index]['subsidypv'] = $data['subsidypv'];
			//差額獎金
			$data_arr[$index]['allowance'] = $allowance;
			//其他加項
			$data_arr[$index]['addmoney'] = $data['addmoney'];
			//加項說明
			$data_arr[$index]['addtxt'] = $data['addtxt'];
			//其他減項
			$data_arr[$index]['lessmoney'] = $data['lessmoney'];
			//減項說明
			$data_arr[$index]['lesstxt'] = $data['lesstxt'];
			//合計
			$data_arr[$index]['totalbonusnum'] = $totalbonusnum;
		}
		$this->exportexcel_model->organizationBouns($data_arr,$product_info,$date);
	}

	//業務人員獎金總表輸出
	public function emexport2(){
		ini_set("max_execution_time", "60");
		$competence_id = $this->session->userdata('competence_id');
		$rightid = $this->competence_model->getaction($competence_id);
		foreach($rightid as $r){
			if($r['sidebar_sub_id'] == 38)
			{
				if($r['actions_export']==0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有匯出的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('usersbonus','refresh');
				}
			}
		}
		$employeeid = $this->input->get('emid');
		$data = [];
		$data_arr = [];
		$product_info = [];
		$selectid = $this->input->get('orgid');
		$selection['status'] = 1;
		$data['occupation'] = $this->occupation_model->getList($selection);
		$product = $this->goods_model->getList($selection);

		if($this->input->get('year') == '' && $this->input->get('month') == ''){
			$currentmonth = date("Y-m",time());
			$selectdate = date("Y-m-01");
			$selection['year'] = date("Y");
			$selection['month'] = date("m");
		}
		else{
			$year = $this->input->get('year');
			$month = $this->input->get('month');
			$currentmonth = $year.'-'.$month;
			$selectdate = date("Y-m-01",strtotime($currentmonth));
			$selection['year'] = $year;
			$selection['month'] = $month;
		}

		if($competence_id == 3){
			if($this->session->userdata('manager_id') != $selectid && $selectid !=''){
				redirect('usersbonus');
			}
			$selection['organization_id'] = $this->session->userdata('manager_id');
			$selection['status'] = 1;
			$data['employee'] = $this->employee_model->getList($selection);
		}

		if($competence_id < 3){
			if($selectid != ''){
				$selection['organization_id'] = $selectid;
				$selection['status'] = 1;
				$data['employee'] = $this->employee_model->getList($selection);
			}
			else{
				$keyword['status'] = 1;
				$data['employee'] = $this->employee_model->getList($keyword);
			}
		}

		$this->load->library('excel');

		// $this->load->library('tcpdf/tcpdf');
		$objPHPExcel = new PHPExcel();

		$rendererName = PHPExcel_Settings::PDF_RENDERER_TCPDF;
		$rendererLibrary = 'tcpdf';
		$rendererLibraryPath = dirname(__FILE__).'/../libraries/'.$rendererLibrary;

		if (!PHPExcel_Settings::setPdfRenderer(
			$rendererName,
			$rendererLibraryPath
		)) {
			die(
				'NOTICE: Please set the $rendererName and $rendererLibraryPath values' .
				EOL .
				'at the top of this script as appropriate for your directory structure'
			);
		}

		$row = 2;
		$acsnum = 67;
		$productrow = 0;
		foreach($product as $productlist){
			$productrow++;
		}
		$lastrow = $acsnum + $productrow + 9;

		foreach($product as $key => $productlist){
			$objPHPExcel->getActiveSheet()->setCellValue(chr($acsnum).$row, $productlist['product_name']);
			$objPHPExcel->getActiveSheet()->getColumnDimension(chr($acsnum))->setWidth(40);
			$acsnum++;
			$product_info[$key]['product_name'] = $productlist['product_name'];
		}

		$rowcount = 2;		
		foreach($data['employee'] as $index => $emid){
			// $employeeinfo = $this->employee_model->getidData($emid);
			$rowcount++;
			//員工姓名
			$employeename = "";
			//營業處名稱
			$orgname = "";
			//業績獎金
			$count = 0;

			//業績獎金
			$eminfo = $this->employee_model->getidData($emid['employee_id']);
			$employeename = $eminfo['employee_name'];
			$orginfo = $this->organization_model->getData($eminfo['organization_id']);
			$orgname = $orginfo['organization_name'];
			$performance = $this->users_bonus_model->em_total_1($emid['employee_id'],$currentmonth);
			if($performance['total'] == null){
				$count = 0;
			}
			else{
				$count = $performance['total'];
			}
			//業績獎金

			//差額獎金
			$allowance = 0;
			//差額獎金

			//同階獎金
			$samelvallowance=0;
			//同階獎金

			//服務津貼
			//今日日期
			$nowdate = date("Y-m-d",time());
			//6個月之前的日期
			$begindate = date("Y-m-01",strtotime("$selectdate-6month"));
			$totalallowance = 0;
			$option['emplyee_id'] = $emid['employee_id'];
			$users_bonus_list = $this->users_bonus_model->get_month_allowance($option,$begindate,$selectdate);
			foreach($users_bonus_list as $u){
				$empv = $this->order_info_model->get_employee_pv($u['employee_id'],$currentmonth);
				$emMonthpv = $empv['pv'];

				if($emMonthpv >= 100000){
					$totalallowance = $totalallowance + $u['users_bonus_money'];
				}
			}		
			//服務津貼

			//差額獎金
			$differencebonus = $this->users_bonus_model->em_total_3($emid['employee_id'],$currentmonth);
			if($differencebonus['total'] == null){
				$allowance = 0;
			}
			else{
				$allowance = $differencebonus['total'];
			}

			//同階獎金
			$samelevelbonus = $this->users_bonus_model->em_total_4($emid['employee_id'],$currentmonth);
			if($samelevelbonus['total'] == null){
				$samelvallowance = 0;
			}
			else{
				$samelvallowance = $samelevelbonus['total'];
			}
			$data['addtxt'] = "";
			$data['addmoney'] = 0;
			$data['lesstxt'] = "";
			$data['lessmoney'] = 0;
			$emextrabonuslist = $this->users_bonus_extra_model->getEmExtraBonus($emid['employee_id']);
			foreach($emextrabonuslist as $row){
				if(substr($row['users_bonus_extra_created_date'],0,7) == $currentmonth && $row['users_bonus_extra_type'] == 1){
					$data['addtxt'] = $row['users_bonus_extra_add_txt'];
					$data['addmoney'] = $row['users_bonus_extra_add_money'];
					$data['lesstxt'] = $row['users_bonus_extra_less_txt'];
					$data['lessmoney'] = $row['users_bonus_extra_less_money'];
				}
			}

		    //查詢各商品銷售單位
			$totalpdnum = 0;
			foreach($product as $productlist){
				${'productnum'.$productlist['product_id']} = 0;
			}
			$selection['emid'] = $emid['employee_id'];
			$emorder = $this->order_model->getselectorder($selection);
			foreach($emorder as $orderlist){
				if($orderlist['order_is_check'] == 1){
					$orderinfo = $this->order_info_model->getcertaininfo($orderlist['order_id']);
					foreach($orderinfo as $infolist){
						foreach($product as $productlist){
							if($infolist['product_id'] == $productlist['product_id']){
								${'productnum'.$productlist['product_id']} = ${'productnum'.$productlist['product_id']} + $infolist['order_info_count'];
								$totalpdnum = $totalpdnum + $infolist['order_info_count'];
							}
						}
					}
				}
			}

			$acsnum = 67;
			foreach($product as $key => $productlist){

				$product_info[$key][$index]['product_num'] = ${'productnum'.$productlist['product_id']};
			}

			$data_arr[$index]['orgname'] = $orgname;
			$data_arr[$index]['employeename'] = $employeename;
			$data_arr[$index]['totalpdnum'] = $totalpdnum;
			$data_arr[$index]['count'] = $count;
			$data_arr[$index]['allowance'] = $allowance;
			$data_arr[$index]['samelvallowance'] = $samelvallowance;
			$data_arr[$index]['totalallowance'] = $totalallowance;
			$data_arr[$index]['addmoney'] = $data['addmoney'];
			$data_arr[$index]['addtxt'] = $data['addtxt'];
			$data_arr[$index]['lessmoney'] = $data['lessmoney'];
			$data_arr[$index]['lesstxt'] = $data['lesstxt'];
			$data_arr[$index]['total'] = $count + $allowance + $samelvallowance + $totalallowance + $data['addmoney'] - $data['lessmoney'];
		}

		$this->exportexcel_model->employeeBouns($data_arr,$product_info);
	}

	//利得發放報表輸出(改寫)
	public function interestexport2(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 5)
			{
				if($r['actions_export']==0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有匯出的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('bonussummary','refresh');
				}
			}
		}
		$data = [];
		$data_arr = [];
		$product_info = [];
		$selection['status'] = 1;
		$data['organization'] = $this->organization_model->getList($selection);
		$product = $this->goods_model->getList($selection);
		$keyword['orgid'] = $this->session->userdata('orgid');
		$keyword['year'] = $this->session->userdata('year');
		$keyword['month'] = $this->session->userdata('month');
		$keyword['customid'] = $this->session->userdata('customid');
		$keyword['contract_id'] = $this->session->userdata('contract_id');
		$keyword['day'] = $this->session->userdata('day');
		if($this->session->userdata('year') == false && $this->session->userdata('month') == false && $this->session->userdata('customid') == false && $this->session->userdata('orgid') == false && $this->session->userdata('day') == false && $this->session->userdata('contract_id') == false){
			$keyword['year'] = date("Y",time());
			$keyword['month'] = date("m",time());
		}

		if($this->competence_id == 4){
			$keyword['employee_id'] = $this->session->userdata('employee_id');
		}

		if($this->competence_id == 3){
			$keyword['orgid'] = $this->session->userdata('manager_id');
		}
		else{
			$data['organization'] = $this->organization_model->getList($selection);
		}

		$profitlist = $this->profit_model->geteveryprofit($keyword);
		$data['occupation'] = $this->occupation_model->getList($selection);
		$date = date("Y-m-01",time());
		$time = date("Y-m-01",strtotime("-3month",strtotime($date)));

		$row = 2;
		$acsnum = 69;
		$productrow = 0;
		foreach($product as $productlist){
			$productrow++;
		}
		$lastrow = $acsnum + $productrow + 9;

		foreach($product as $key => $productlist){
			$product_info[$key]['product_name'] = $productlist['product_name'];
		}

		$rowcount = 3;
		$num = 1;
		foreach($profitlist as $index => $profit){
			$profitdate = date("Y-m-01",strtotime($profit['profit_created_date']));

			$orderinfo = $this->order_model->getidData($profit['order_id']);
			if($orderinfo['order_is_check'] != 1){
				continue;
			}
			//訂單客戶資料
			$customer = $this->customers_model->getiddata($orderinfo['customer_id']);
			//訂單業務承攬人資料
			$employee = $this->employee_model->getidData($orderinfo['employee_id']);
			//業務承攬人直屬經理
			$supervisor = $this->employee_model->getidData($employee['employee_id']);
			//訂單營業處資料
			$organization = $this->organization_model->getData($orderinfo['organization_id']);
			$interest = 0;
			$qinterest = 0;
			if($profit['profit_type'] == 0){
				$interest = $profit['profit_bouns_momey'];
			}
			else{
				$qinterest = $profit['profit_bouns_momey'];
			}

			$orderinfolist = $this->order_info_model->getcertaininfo($profit['order_id']);
			//季利得開始發放的時間

			//查詢各商品銷售單位
			$total = 0;
			foreach($product as $productlist){
				${'productnum'.$productlist['product_id']} = 0;
			}
			foreach($orderinfolist as $infolist){
				foreach($product as $productlist){
					if($infolist['product_id'] == $productlist['product_id']){
						${'productnum'.$productlist['product_id']} = ${'productnum'.$productlist['product_id']} + $infolist['order_info_count'];
						$total = $total + $infolist['order_info_count'];
					}
				}
			}


			if($customer['customer_bank'] == 700){
				$data['bank'] = '中華郵政';
				$branchinfo = $this->post_model->getidData($customer['customer_bank_branch']);
				$data['branch'] = $branchinfo['post_name'];
				$branchcode = '7000021';
			}
			else{
				$bankinfo = $this->bank_model->getidData($customer['customer_bank']);
				$data['bank'] = $bankinfo['bank_name'];
				$branchinfo = $this->branch_model->getidData($customer['customer_bank_branch']);
				$data['branch'] = $branchinfo['bank_branch_name'];
				$branchcode = $branchinfo['bank_branch_code'];
			}

			foreach($product as $key => $productlist){
				$product_info[$key][$index]['product_num'] = ${'productnum'.$productlist['product_id']};
			}
			$data_arr[$index]['contract_id'] = $orderinfo['contract_id'];
			$data_arr[$index]['order_active_date'] = $orderinfo['order_active_date'];
			$data_arr[$index]['customer_name'] = $customer['customer_name'];
			$data_arr[$index]['total'] = $total;
			$data_arr[$index]['interest'] = $interest;
			$data_arr[$index]['qinterest'] = $qinterest;
			$data_arr[$index]['interest_total'] = $interest + $qinterest;
			$data_arr[$index]['employee_name'] = $employee['employee_name'];
			$data_arr[$index]['supervisor_name'] = $supervisor['employee_name'];
			$data_arr[$index]['organization_name'] = $organization['organization_name'];
			$data_arr[$index]['branch'] = $data['branch'];
			$data_arr[$index]['branchcode'] = $branchcode;
			$data_arr[$index]['customer_account'] = $customer['customer_account'];
		}

		$this->exportexcel_model->interest($data_arr,$product_info);
	}

	//訂單報表輸出
	public function orderexport(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 	10)
			{
				if($r['actions_export'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有匯出的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('order/list');
				}
			}
		}
		$data = [];
		$data_arr = [];

		$keyword['check_status'] = $this->session->userdata('check_status');
		$keyword['organization_id'] = $this->session->userdata('orgid');
		$keyword['year'] = $this->session->userdata('year');
		$keyword['month'] = $this->session->userdata('month');
		$keyword['customer_id'] = $this->session->userdata('customid');
		$keyword['employee_id'] = $this->session->userdata('emid');
		$keyword['status'] = 1;
		$orgname = "";
		$year = date("Y",time());
		$selectmonth = date("m",time());
		$day = "";

		if ($this->session->userdata('year')) {
			$year = $this->session->userdata('year');
		}

		if ($this->session->userdata('month')) {
			$selectmonth = $this->session->userdata('month');
		}

		if($selectmonth == date("m",time())){
			$day = date("d",time());
		}

		$data['order'] = $this->order_model->getList($keyword);

		if($this->competence_id == 3){
			$keyword['organization_id'] = $this->session->userdata('manager_id');
			$data['order'] = $this->order_model->getList($keyword);
		}
		$data['occupation'] = $this->occupation_model->getList($keyword);
		$date = date("Y-m-01");
		$time = date("Y-m",strtotime("-3month",strtotime($date)));

		foreach($data['order'] as $key => $order){
			//訂單客戶資料
			$customer = $this->customers_model->getiddata($order['customer_id']);
			//訂單業務承攬人資料
			$employee = $this->employee_model->getidData($order['employee_id']);
			//訂單詳細資料
			$orderinfolist = $this->order_info_model->getcertaininfo($order['order_id']);
			//業務承攬人直屬經理
			$supervisor = $this->employee_model->getidData($employee['employee_supervisor']);
			//營業處資料
			$orginfo = $this->organization_model->getData($order['organization_id']);

			$productnum = 0;
			$productnum1 = 0;
			$productname = '';
			$totalprice = 0;
			foreach($orderinfolist as $orderinfo){
				$productinfo = $this->goods_model->getidData($orderinfo['product_id']);
				switch($orderinfo['product_id']){
					case '4':
					$productnum1 = $orderinfo['order_info_count'];
					$totalprice = $totalprice + $orderinfo['order_info_count'] * $productinfo['product_a_price'];
					break;
					default:
					$productnum = $orderinfo['order_info_count'];
					$totalprice = $totalprice + $orderinfo['order_info_count'] * $productinfo['product_a_price'];
					$productname = $productinfo['product_name'];
					break;
				}
			}

			$orderstatus = "";
			switch ($order['order_is_check']) {
				case '0':
				$orderstatus = "未審核";
				break;
				case '1':
				$orderstatus = "審核完成";
				break;
				case '2':
				$orderstatus = "審核否決";
				break;
				case '3':
				$orderstatus = "作廢";
				break;
			}

			//訂單審核狀態
			$data_arr[$key]['order_is_check'] = $orderstatus;
			//營業處
			$data_arr[$key]['organization_name'] = $orginfo['organization_name'];
			//承購日期
			$data_arr[$key]['order_created_date'] = $order['order_created_date'];
			//生效日
			$data_arr[$key]['order_active_date'] = $order['order_active_date'];
			//合約書編號
			$data_arr[$key]['contract_id'] = $order['contract_id'];
			//承購人姓名
			$data_arr[$key]['customer_name'] = $customer['customer_name'];
			//基金
			$data_arr[$key]['product_name'] = $productname;
			//基金單位
			$data_arr[$key]['order_info_count'] = $productnum;
			//股權單位
			$data_arr[$key]['order_info_count_1'] = $productnum1;
			//合計金額
			$data_arr[$key]['totalprice'] = $totalprice;
			//付款別
			$data_arr[$key]['order_pay_type'] = $order['order_pay_type'];
			//簽收日
			$data_arr[$key]['order_sign_date'] = $order['order_sign_date'];
			//經辦人員
			$data_arr[$key]['employee_name'] = $employee['employee_name'];
			//直屬經理
			$data_arr[$key]['supervisor_name'] = $supervisor['employee_name'];
			//備註1
			$data_arr[$key]['order_note_1'] = $order['order_note_1'];
			//備註2
			$data_arr[$key]['order_note_2'] = $order['order_note_2'];
		}
		$order_date['year'] = $year;
		$order_date['selectmonth'] = $selectmonth;
		$order_date['day'] = $day;
		$this->exportexcel_model->order($data_arr,$order_date);
	}

	//股權待售報表輸出
	public function stocksoldexport(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 	10)
			{
				if($r['actions_export'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有匯出的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('stocksold/list');
				}
			}
		}
		$data = [];
		$data_arr = [];

		$selectid = $this->input->get('id');
		if($selectid != ''){
			$data['order'] = $this->order_info_model->getorgequity($selectid);
		}
		else{
			$data['order'] = $this->order_info_model->getallequity();
		}

		$keyword['orgid'] = $this->session->userdata('orgid');
		$keyword['year'] = $this->session->userdata('year');
		$keyword['month'] = $this->session->userdata('month');
		$keyword['customid'] = $this->session->userdata('customid');
		$keyword['employeeid'] = $this->session->userdata('employeeid');

		$selection['status'] = 1;
		$data['occupation'] = $this->occupation_model->getList($selection);
		$equitylist = $this->order_info_model->getallequity($keyword);
		$date = date("Y-m-01");
		$time = date("Y-m",strtotime("-3month",strtotime($date)));

		foreach($equitylist as $key => $order){
			//訂單客戶資料
			$customer = $this->customers_model->getiddata($order['customer_id']);
			//訂單業務承攬人資料
			$employee = $this->employee_model->getidData($order['employee_id']);
			//訂單詳細資料
			$orderinfolist = $this->order_info_model->getcertaininfo($order['order_id']);
			//訂單營業處資料
			$organization = $this->organization_model->getData($order['organization_id']);
			//最新股價
			$stock = $this->stock_model->getlatestvalue();
			//是否可售出
			if($order['order_active_date'] < date('Y-m-d', strtotime('-1 year'))){
				$status = "可售出";
			}
			else{
				$status = "不可售出";
			};

			$data_arr[$key]['contract_id'] = $order['contract_id'];
			$data_arr[$key]['customer_name'] = $customer['customer_name'];
			$data_arr[$key]['order_active_date'] = $order['order_active_date'];
			$data_arr[$key]['order_stock'] = $order['order_stock'];
			$data_arr[$key]['stock_value'] = $stock['stock_value'];
			$data_arr[$key]['status'] = $status;
			$data_arr[$key]['employee_name'] = $employee['employee_name'];
			$data_arr[$key]['organization_name'] = $organization['organization_name'];
		}
		$this->exportexcel_model->stocksold($data_arr);
	}

	//獎金異動報表輸出
	public function bonus_status_export(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 	47)
			{
				if($r['actions_export'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有匯出的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('bonus_status/list');
				}
			}
		}

		if($this->session->userdata('orgid') == false && $this->session->userdata('emid') == false && $this->session->userdata('bonustype') == false && $this->session->userdata('year') == false && $this->session->userdata('month') == false && $this->competence_id !=3)
		{
			$messagediv = "<div class='alert'>
			<button class='close' data-dismiss='alert'></button>
			<div></div> 請選擇匯出範圍，以免匯出檔案過大。
			</div>";
			$this->session->set_flashdata('messagediv',$messagediv);
			redirect('bonus_status/list');
		}

		$data = [];
		$data_arr = [];

		$selectid = $this->input->get('id');
		$selection['status'] = 1;
		if($selectid != ''){
			$data['order'] = $this->order_info_model->getorgequity($selectid);
		}
		else{
			$data['order'] = $this->order_info_model->getallequity();
		}

		if($this->session->userdata('orgid')){
			$keyword['orgid'] = $this->session->userdata('orgid');
		}
		else{
			$keyword['orgid'] = $this->input->post('orgid');
		}

		if($this->session->userdata('emid')){
			$keyword['emplyee_id'] = $this->session->userdata('emid');
		}
		else{
			$keyword['emplyee_id'] = $this->input->post('employeeid');
		}

		if($this->session->userdata('bonustype')){
			$keyword['bonus_type_id'] = $this->session->userdata('bonustype');
		}
		else{
			$keyword['bonus_type_id'] = $this->input->post('bonustype');
		}

		if($this->session->userdata('year')){
			$keyword['year'] = $this->session->userdata('year');
		}
		else{
			$keyword['year'] = $this->input->post('year');
		}

		if($this->session->userdata('month')){
			$keyword['month'] = $this->session->userdata('month');
		}
		else{
			$keyword['month'] = $this->input->post('month');
		}

		if(($this->session->userdata('year') == null or $this->session->userdata('year') == '') && ($this->session->userdata('month') == null or $this->session->userdata('month') == '')){
			$keyword['year'] = date("Y",time());
			$keyword['month'] = date("m",time());
		}

		if($this->competence_id == 3){
			$keyword['orgid'] = $this->session->userdata('manager_id');
		}

		$data['bonuslist'] = $this->users_bonus_model->geteverybonus($keyword);
		$data['occupation'] = $this->occupation_model->getList($selection);
		$date = date("Y-m-01");
		$time = date("Y-m",strtotime("-3month",strtotime($date)));

		foreach($data['bonuslist'] as $key => $bonus){
			//業務承攬人資料
			$employee = $this->employee_model->getidData($bonus['employee_id']);
			//訂單資料
			$orderinfo = $this->order_model->getidData($bonus['order_id']);
			//訂單詳細資料
			$orderinfolist = $this->order_info_model->getcertaininfo($bonus['order_id']);
			//營業處資料
			$organization = $this->organization_model->getData($bonus['organization_id']);
			//職稱資料
			$occupation = $this->occupation_model->getidData($employee['occupation_id']);
			//獎金種類
			$bonusinfo = $this->bonus_model->getonebonustype($bonus['bonus_type_id']);

			if($orderinfo['order_is_check'] != 1){
				continue;
			}

			//獎金發放狀態
			switch($bonus['users_bonus_is_check']){
				case'0':
				$data_arr[$key]['users_bonus_is_check'] = '未確認';
				break;
				case'1':
				$data_arr[$key]['users_bonus_is_check'] = '已確認';
				break;
				case'2':
				$data_arr[$key]['users_bonus_is_check'] = '發放取消';
				break;
				case'3':
				$data_arr[$key]['users_bonus_is_check'] = '作廢';
				break;
			}

			//合約書編號
			$data_arr[$key]['contract_id'] = $orderinfo['contract_id'];
			//業務承攬人
			$data_arr[$key]['employee_name'] = $employee['employee_name'];
			//所屬單位
			$data_arr[$key]['organization_name'] = $organization['organization_name'];
			//職稱
			$data_arr[$key]['occupation_name'] = $occupation['occupation_name'];
			//獎金名稱
			$data_arr[$key]['bonus_type_name'] = $bonusinfo['bonus_type_name'];
			//獎金金額
			$data_arr[$key]['users_bonus_money'] = $bonus['users_bonus_money'];
			//合約生效日
			$data_arr[$key]['order_active_date'] = $orderinfo['order_active_date'];
		}
		$this->exportexcel_model->bonusChange($data_arr);
	}

	//業務承攬人報表輸出
	public function employee_export(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 1)
			{
				if($r['actions_export'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有匯出的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('employee/list');
				}
			}
		}

		$data = [];
		$data_arr = [];

		if($this->session->userdata('orgid')){
			$keyword['orgid'] = $this->session->userdata('orgid');
		}
		else{
			$keyword['orgid'] = $this->input->post('orgid');
		}

		if($this->session->userdata('emid')){
			$keyword['emplyee_id'] = $this->session->userdata('emid');
		}
		else{
			$keyword['emplyee_id'] = $this->input->post('employeeid');
		}

		if($this->competence_id == 3){
			$keyword['orgid'] = $this->session->userdata('manager_id');
		}

		$employeelist = $this->employee_model->getList($keyword);

		foreach($employeelist as $key => $employee){

			//所有下線當月pv總和(不包含自己)
			$group_total_pv = 0;
			//當月下線實動人數
			$active_downline = 0;
			//培養經理人數
			$downline_manager = 0;
			$all_downline = $this->employee_model->get_all_downline($employee['employee_id']);
			foreach($all_downline as $downline){
				if($downline['employee_id'] != $employee['employee_id']){
					$empv = $this->order_info_model->get_month_employee_pv($downline['employee_id']);
					$group_total_pv = $group_total_pv + $empv['pv'];
					if($empv['pv'] != 0){
						$active_downline = $active_downline + 1;
					}

					if($downline['occupation_id'] == 4){
						$downline_manager = $downline_manager + 1;
					}
				}
			}

			//自己當月pv
			$empv = $this->order_info_model->get_month_employee_pv($employee['employee_id']);
			$personal_pv = $empv['pv'];
			$data['group_total_pv'] = $group_total_pv + $personal_pv;

			switch ($employee['occupation_id']) {
				//總監升副總
				case '3':
				if($group_total_pv >= 50000000 && $active_downline >= 10 && $downline_manager >= 3){
					$data['promote'] = '可';
				}
				else{
					$data['promote'] = '否';
				}
				break;
				//協理升總監
				case '11':
				if($group_total_pv >= 25000000 && $active_downline >= 8){
					$data['promote'] = '可';
				}
				else{
					$data['promote'] = '否';
				}
				break;
				//經理升協理
				case '4':
				if($group_total_pv >= 7500000 && $active_downline >= 5){
					$data['promote'] = '可';
				}
				else{
					$data['promote'] = '否';
				}
				break;
				//副理升經理
				case '5':
				if($group_total_pv >= 2500000 && $active_downline >= 2){
					$data['promote'] = '可';
				}
				else{
					$data['promote'] = '否';
				}
				break;
				//專員升副理
				case '6':
				if($personal_pv >= 250000){
					$data['promote'] = '可';
				}
				else{
					$data['promote'] = '否';
				}
				break;
				default:
				$data['promote'] = '否';
				break;
			}

			if($this->session->userdata('promote') && $this->session->userdata('promote') == 1 && $data['promote'] == '否'){
				continue;
			}

			if($this->session->userdata('promote') && $this->session->userdata('promote') == 2 && $data['promote'] == '是'){
				continue;
			}

			//營業處資料
			$organization = $this->organization_model->getData($employee['organization_id']);
			//職稱資料
			$occupation = $this->occupation_model->getidData($employee['occupation_id']);
			//推薦人資料
			$referrer = $this->employee_model->getidData($employee['users_id']);
			//直屬經理資料
			$supervisor = $this->employee_model->getidData($employee['employee_supervisor']);

			$data['bank'] = '';
			$data['branch'] = '';
			//帳戶資料
			if($employee['employee_bank_name'] == 700){
				$data['bank'] = '中華郵政';
				$branchinfo = $this->post_model->getidData($employee['employee_bank_branch']);
				$data['branch'] = $branchinfo['post_name'];
				$branchcode = '7000021';
			}
			else{
				$bankinfo = $this->bank_model->getidData($employee['employee_bank_name']);
				$data['bank'] = $bankinfo['bank_name'];
				$branchinfo = $this->branch_model->getidData($employee['employee_bank_branch']);
				$data['branch'] = $branchinfo['bank_branch_name'];
				$branchcode = $branchinfo['bank_branch_code'];
			}

			if($employee['employee_status'] == 1){
				$status = '啟用';
			}
			else{
				$status = '停權';
			}

			//姓名
			$data_arr[$key]['employee_name'] = $employee['employee_name'];
			//身分證字號
			$data_arr[$key]['employee_id_card'] = $employee['employee_id_card'];
			//營業處
			$data_arr[$key]['organization_name'] = $organization['organization_name'];
			//職稱
			$data_arr[$key]['occupation_name'] = $occupation['occupation_name'];
			//直屬經理
			$data_arr[$key]['supervisor_name'] = $supervisor['employee_name'];
			//推薦人
			$data_arr[$key]['referrer_name'] = $referrer['employee_name'];
			//戶名
			$data_arr[$key]['employee_account_name'] = $employee['employee_account_name'];
			//銀行名稱
			$data_arr[$key]['bank'] = $data['bank'];
			//分行代號
			$data_arr[$key]['bank_branch_code'] = $branchcode;
			//匯款帳號
			$data_arr[$key]['employee_bank_account'] = $employee['employee_bank_account'];
			//備註
			$data_arr[$key]['employee_note'] = $employee['employee_note'];
			//狀態
			$data_arr[$key]['status'] = $status;
		}

		$this->exportexcel_model->employee($data_arr);
	}

	//客戶報表輸出
	public function customer_export(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 3)
			{
				if($r['actions_export'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有匯出的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('employee/list');
				}
			}
		}

		$data = [];
		$data_arr = [];

		if($this->session->userdata('orgid')){
			$keyword['organization_id'] = $this->session->userdata('orgid');
		}
		else{
			$keyword['organization_id'] = $this->input->post('orgid');
		}

		switch($this->competence_id){
			case '3':
			$keyword['organization_id'] = $this->session->userdata('manager_id');
			break;
			case '4':
			$keyword['emplyee_id'] = $this->session->userdata('emplyee_id');
			break;
			default:
			break;
		}

		$customerlist = $this->customers_model->getList($keyword);

		$rowcount = 3;
		foreach($customerlist as $key => $customer){
			//營業處資料
			$organization = $this->organization_model->getData($customer['organization_id']);
			//經辦人員
			$employee = $this->employee_model->getidData($customer['employee_id']);

			$data['bank'] = '';
			$data['branch'] = '';
			//帳戶資料
			if($customer['customer_bank'] == 700){
				$data['bank'] = '中華郵政';
				$branchinfo = $this->post_model->getidData($customer['customer_bank_branch']);
				$data['branch'] = $branchinfo['post_name'];
				$branchcode = '7000021';
			}
			else{
				$bankinfo = $this->bank_model->getidData($customer['customer_bank']);
				$data['bank'] = $bankinfo['bank_name'];
				$branchinfo = $this->branch_model->getidData($customer['customer_bank_branch']);
				$data['branch'] = $branchinfo['bank_branch_name'];
				$branchcode = $branchinfo['bank_branch_code'];
			}

			if($customer['customer_status'] == 1){
				$status = '啟用';
			}
			else{
				$status = '停權';
			}

			//姓名
			$data_arr[$key]['customer_name'] = $customer['customer_name'];
			//出生年月日
			$data_arr[$key]['customer_birthday'] = $customer['customer_birthday'];
			//身份證字號
			$data_arr[$key]['customer_id_card'] = $customer['customer_id_card'];
			//通訊地址
			$data_arr[$key]['customer_permanent_address'] = $customer['customer_permanent_address'];
			//戶籍地址
			$data_arr[$key]['customer_residential_address'] = $customer['customer_residential_address'];
			//手機
			$data_arr[$key]['customer_phone'] = $customer['customer_phone'];
			//聯絡電話(公)
			$data_arr[$key]['customer_company_tel'] = $customer['customer_company_tel'];
			//聯絡電話(宅)
			$data_arr[$key]['customer_residential_tel'] = $customer['customer_residential_tel'];
			//電子郵件信箱
			$data_arr[$key]['customer_email'] = $customer['customer_email'];
			//戶名
			$data_arr[$key]['customer_account_name'] = $customer['customer_account_name'];
			//匯款銀行
			$data_arr[$key]['branch'] = $data['branch'];
			//匯款帳號
			$data_arr[$key]['customer_account'] = $customer['customer_account'];
			//經辦人員
			$data_arr[$key]['employee_name'] = $employee['employee_name'];
			//營業處
			$data_arr[$key]['organization_name'] = $organization['organization_name'];
			//備註
			$data_arr[$key]['customer_note'] = $customer['customer_note'];
			//狀態
			$data_arr[$key]['status'] = $status;
		}
		$this->exportexcel_model->customers($data_arr);
	}

}
