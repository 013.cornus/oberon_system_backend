<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/ks/group_model');
		$this->load->model('amadis_sys/ks/register_key_model');
		$this->load->model('amadis_sys/group_info_model');
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/pagintion_model');
		$this->unitName = '登入帳號管理';
		$this->competence_id = $this->session->userdata('competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);
		

		$admin_id = $this->session->userdata('users_id');
		$username = $this->session->userdata('users_name');
		$this->session->set_flashdata('sidebarselected','group');
		$this->session->set_flashdata('mainsidebar','excel_create');
		$this->load->library('apiconnection');
		if(!$admin_id){
			redirect('home/login');
		}
	}

    //群組列表
	public function index(){
		redirect('group/list');
		exit;
	}

    //群組列表
	public function list($page=''){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有瀏覽的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('home');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '登入帳號設定列表';
		$data['active'] = 'employee';

		$keyword['fullname'] = $this->input->post('fullname');
		
		$result = $this->group_model->getList($keyword);		
		$pager = $this->pagintion_model->setPager2($result,$page);
		$data['page_list'] = $pager['page'];
		$data['result'] = $this->group_model->getList($keyword,$pager['list']);
		
		//選單
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/group/list',$data);	
	}

	//跳到指定頁面
	public function pagejump(){
		if($this->input->post('pagenum') == null || $this->input->post('pagenum') == 0){
			redirect('group/list');
		}
		else{
			redirect('group/list/'.$this->input->post('pagenum').'');
		}
	}

    // 新增表單
	public function create_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_insert'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有新增的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '新增群組';

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['groupList'] = $this->group_info_model->getList();
		$this->session->set_flashdata('sidebarselected','group');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/group/create',$data);
	}

	// 新增登入帳號執行
	public function create(){
		$post = $this->input->post();
		$datetime = date("Y-m-d H:i:s",time());
		$group = array(
			'group_name'         => $post['group_name'],
			'group_phone'        => $post['group_phone'],
			'group_type'         => $post['group_type'],
			'group_password'     => $post['group_password'],
			'group_created_date' => $datetime,
			'group_created_user' => $this->session->userdata('users_id')
		);
		$this->group_model->add_group($group);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！帳號已新增完成。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('group');
	}

	// 刪除執行
	public function delete(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_delete'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有刪除的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 
		$group = array(
			'group_is_del'       => 1,
			'group_updated_date' => date("Y-m-d H:i:s",time()),
			'group_updated_user' => $this->session->userdata('users_id')
		);

		$this->group_model->update_group($group,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆群組已刪除。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('group');
	}

	// 下架執行
	public function group_invisible(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_enable'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 
		$group = array(
			'group_status'       => 0,
			'group_updated_date' => date("Y-m-d H:i:s",time()),
			'group_updated_user' => $this->session->userdata('users_id')
		);

		$this->group_model->update_group($group,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆群組已停用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('group');
	}

	// 上架執行
	public function group_visible(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有上架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 

		$group = array(
			'group_status'       => 1,
			'group_updated_date' => date("Y-m-d H:i:s",time()),
			'group_updated_user' => $this->session->userdata('users_id')
		);

		$this->group_model->update_group($group,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆群組已啟用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('group');
	}

	// 編輯表單
	public function update_form(){
		$id = $this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_update'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯群組';
		
		$data['result'] = $this->group_model->getidData($id);
		$group_info = $this->group_info_model->getDataByType($data['result']['group_type']);
		$data['group_type'] = $group_info['group_info_name'];
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/group/update',$data);
	}

    // 編輯執行
	public function update(){
		$post = $this->input->post();
		$id = $post['id'];

		$group = array(
			'group_name' => $post['group_name'],
		);
		if(isset($post['password']) && $post['password'] != ''){
			$group['group_password'] = md5($post['password']);
		}

		$this->group_model->update_group($group,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！此筆登入帳號已更新完成。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('group');
	}

	// 編輯表單
	public function check_form(){
		$id = $this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯群組';
		
		$data['result'] = $this->group_model->getidData($id);
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/group/check',$data);
	}
}
