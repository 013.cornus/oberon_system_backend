<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group_template_setting extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/ks/group_model');
		$this->load->model('amadis_sys/ks/register_key_model');
		$this->load->model('amadis_sys/group_info_model');
		$this->load->model('amadis_sys/group_template_setting_model');
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/pagintion_model');
		$this->unitName = '銷售/服務端模板設定';
		$this->competence_id = $this->session->userdata('competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);
		

		$admin_id = $this->session->userdata('users_id');
		$username = $this->session->userdata('users_name');
		$this->session->set_flashdata('sidebarselected','group_template_setting');
		$this->session->set_flashdata('mainsidebar','service_side_manage');
		$this->load->library('apiconnection');
		if(!$admin_id){
			redirect('home/login');
		}
	}

    //群組列表
	public function index(){
		redirect('group_template_setting/list');
		exit;
	}

    //群組列表
	public function list($page=''){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有瀏覽的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('home');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '銷售/服務端模板設定列表';
		$data['active'] = 'employee';

		$keyword['fullname'] = $this->input->post('fullname');

		$result = $this->group_template_setting_model->getList();
		$pager = $this->pagintion_model->setPager2($result,$page);
		$data['page_list'] = $pager['page'];
		$data['result'] = $this->group_template_setting_model->getList($keyword,$pager['list']);
		foreach($data['result'] as $key => $row){
			$group_info = $this->group_info_model->getDataByType($row['group_type']);
			$data['group_name'][$key] = $group_info['group_info_name'];
		}
		
		//選單
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/group_template_setting/list',$data);	
	}

	//跳到指定頁面
	public function pagejump(){
		if($this->input->post('pagenum') == null || $this->input->post('pagenum') == 0){
			redirect('group_template_setting/list');
		}
		else{
			redirect('group_template_setting/list/'.$this->input->post('pagenum').'');
		}
	}

    // 新增表單
	public function create_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_insert'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有新增的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group_template_setting');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '新增群組';

		$url = 'http://34.80.38.39/rgms/api/template';
		$dataArr = [];
		$data['template'] = json_decode($this->apiconnection->crul_connect($dataArr,$url,'GET'),true)['Data'];
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['groupList'] = $this->group_info_model->getList();
		$this->session->set_flashdata('sidebarselected','group_template_setting');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/group_template_setting/create',$data);
	}

	// 新增登入帳號執行
	public function create(){
		$post = $this->input->post();
		$datetime = date("Y-m-d H:i:s",time());
		$group_template_setting = array(
			'group_type'    => $post['group_type'],
			'template_list' => json_encode($post['excel_list'])
		);
		$this->group_template_setting_model->add_group_template_setting($group_template_setting);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！帳號已新增完成。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('group_template_setting');
	}

	// 刪除執行
	public function delete(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_delete'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有刪除的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group_template_setting');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 
		$group_template_setting = array(
			'group_template_setting_is_del'       => 1,
			'group_template_setting_updated_date' => date("Y-m-d H:i:s",time()),
			'group_template_setting_updated_user' => $this->session->userdata('users_id')
		);

		$this->group_template_setting_model->update_group_template_setting($group_template_setting,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆群組已刪除。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('group_template_setting');
	}

	// 下架執行
	public function group_template_setting_invisible(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_enable'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group_template_setting');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 
		$group_template_setting = array(
			'group_template_setting_status'       => 0,
			'group_template_setting_updated_date' => date("Y-m-d H:i:s",time()),
			'group_template_setting_updated_user' => $this->session->userdata('users_id')
		);

		$this->group_template_setting_model->update_group_template_setting($group_template_setting,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆群組已停用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('group_template_setting');
	}

	// 上架執行
	public function group_template_setting_visible(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有上架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group_template_setting');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 

		$group_template_setting = array(
			'group_template_setting_status'       => 1,
			'group_template_setting_updated_date' => date("Y-m-d H:i:s",time()),
			'group_template_setting_updated_user' => $this->session->userdata('users_id')
		);

		$this->group_template_setting_model->update_group_template_setting($group_template_setting,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆群組已啟用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('group_template_setting');
	}

	// 編輯表單
	public function update_form(){
		$id = $this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_update'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group_template_setting');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯群組';
		
		$data['result'] = $this->group_template_setting_model->getidData($id);
		$group_info = $this->group_info_model->getDataByType($data['result']['group_type']);
		$data['group_type'] = $group_info['group_info_name'];
		$template_list = json_decode($data['result']['template_list'],true);

		$url = 'http://34.80.38.39/rgms/api/template';
		$dataArr = [];
		$data['template'] = json_decode($this->apiconnection->crul_connect($dataArr,$url,'GET'),true)['Data'];
		foreach($data['template'] as $key => $row){
			$data['selected'][$key] = '';
			if(in_array($row['excel_template_id'], $template_list)){
				$data['selected'][$key] = 'selected';
			}
		}
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/group_template_setting/update',$data);
	}

    // 編輯執行
	public function update(){
		$post = $this->input->post();
		$id = $post['id'];

		$group_template_setting = array(
			'template_list' => json_decode($post['excel_list']),
		);

		$this->group_template_setting_model->update_group_template_setting($group_template_setting,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！已更新完成。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('group_template_setting');
	}

	// 編輯表單
	public function check_form(){
		$id = $this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('group_template_setting');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯群組';
		
		$data['result'] = $this->group_template_setting_model->getidData($id);
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/group_template_setting/check',$data);
	}
}
