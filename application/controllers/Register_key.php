<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_key extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/ks/group_model');
		$this->load->model('amadis_sys/ks/register_key_model');
		$this->load->model('amadis_sys/group_info_model');
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/pagintion_model');
		$this->unitName = '登入帳號管理';
		$this->competence_id = $this->session->userdata('competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);
		

		$admin_id = $this->session->userdata('users_id');
		$username = $this->session->userdata('users_name');
		$this->session->set_flashdata('sidebarselected','register_key');
		$this->session->set_flashdata('mainsidebar','excel_create');
		$this->load->library('apiconnection');
		if(!$admin_id){
			redirect('home/login');
		}
	}

    //群組列表
	public function index(){
		redirect('register_key/list');
		exit;
	}

    //群組列表
	public function list($page=''){
		// $para_arr = array(
		// 	'version' => '1.0',
		// 	'merchant_id' => '153328840000001',
		// 	'terminal_id' => '53328840',
		// 	'order_no' => 'test'.time(),
		// 	'currency' => 'TWD',
		// 	'order_amount' => '10000',
		// 	'order_desc' => 'test',
		// 	'expiry_time' => '20200902121200',
		// 	'timestamp' => time() * 1000,
		// );
		// ksort($para_arr);
		// $stringA = http_build_query($para_arr);
		// $stringB = $stringA.'&key=IBSGx2QNZ1G3cD7mzC76sAzlZVVwIc8Ru1nHYZmAAQWLX8Dkox1xwRKYHy8vhqap';
		// $hashValue = hash('sha256', $stringB,true);
		// $signValue = base64_encode($hashValue);
		// $para_arr['sign'] = $signValue;

		// $ch = curl_init('https://ta.digiflowtech.com/universal/order');
		// $post = http_build_query($para_arr);
		// curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/x-www-form-urlencoded;charset=utf-8')); 
		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		// $result = curl_exec($ch);
		// print_r($result);
		// exit();
		// curl_close($ch);

		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有瀏覽的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('home');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '登入帳號設定列表';
		$data['active'] = 'employee';

		$keyword['fullname'] = $this->input->post('fullname');
		$keyword['group_type'] = $this->session->userdata('group_type');
		$result = $this->register_key_model->getList($keyword);		
		$pager = $this->pagintion_model->setPager2($result,$page);
		$data['page_list'] = $pager['page'];
		$data['result'] = $this->register_key_model->getList($keyword,$pager['list']);
		foreach($data['result'] as $key => $row){
			$group_info = $this->group_info_model->getDataByType($row['group_type']);
			$data['group_name'][$key] = $group_info['group_info_name'];
			if($row['register_key_use'] == 1){
				$data['used'][$key] = '已啟動';
			}
			else{
				$data['used'][$key] = '未啟動';
			}

			$account_info = $this->group_model->getidData($row['group_id']);
			if($account_info){
				$data['account'][$key] = $account_info['group_phone'];
			}
			else{
				$data['account'][$key] = '無';
			}
		}
		
		//選單
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/register_key/list',$data);	
	}

	//跳到指定頁面
	public function pagejump(){
		if($this->input->post('pagenum') == null || $this->input->post('pagenum') == 0){
			redirect('register_key/list');
		}
		else{
			redirect('register_key/list/'.$this->input->post('pagenum').'');
		}
	}

    // 新增表單
	public function create_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_insert'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有新增的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('register_key');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '新增激活序號';

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['groupList'] = $this->group_info_model->getList();
		$this->session->set_flashdata('sidebarselected','group');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/register_key/create',$data);
	}

	// 新增群組執行
	public function create(){
		$post = $this->input->post();
		$datetime = date("Y-m-d H:i:s",time());
		$number = $post['number'];
		for($i = 1;$i <= $number;$i++){
			$key = $this->random_keys(30);
			$keyword['register_key_name'] = $key;
			$check_key = $this->register_key_model->getList($keyword);
			while ($check_key) {
				$key = $this->random_keys(30);
				$keyword['register_key_name'] = $key;
				$check_key = $this->register_key_model->getList($keyword);
			}

			$register_key = array(
				'register_key_name'         => $key,
				'group_type'                => $post['group_type'],
				'register_key_created_date' => $datetime
			);
			$this->register_key_model->add_register_key($register_key);
		}

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！激活序號已新增完成。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('register_key');
	}

	// 刪除執行
	public function delete(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_delete'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有刪除的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('register_key');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 
		$group = array(
			'group_is_del'       => 1,
			'group_updated_date' => date("Y-m-d H:i:s",time()),
			'group_updated_user' => $this->session->userdata('users_id')
		);

		$this->group_model->update_group($group,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆群組已刪除。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('register_key');
	}

	// 下架執行
	public function group_invisible(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_enable'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('register_key');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 
		$group = array(
			'group_status'       => 0,
			'group_updated_date' => date("Y-m-d H:i:s",time()),
			'group_updated_user' => $this->session->userdata('users_id')
		);

		$this->group_model->update_group($group,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆群組已停用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('register_key');
	}

	// 上架執行
	public function group_visible(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有上架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('register_key');
				}
			}
		}
		
		$datetime = date("Y-m-d H:i:s"); 

		$group = array(
			'group_status'       => 1,
			'group_updated_date' => date("Y-m-d H:i:s",time()),
			'group_updated_user' => $this->session->userdata('users_id')
		);

		$this->group_model->update_group($group,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆群組已啟用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('register_key');
	}

	// 編輯表單
	public function update_form(){
		$id = $this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_update'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('register_key');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯群組';
		
		$data['result'] = $this->group_model->getidData($id);
		$data['template'] = json_decode($this->apiconnection->apiConnect('','http://34.80.38.39/api/report_view/getTemplateList','post'),true);
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/register_key/update',$data);
	}

    // 編輯執行
	public function update(){
		$id = $this->input->post('id');

		$group = array(
			'group_name'         => $this->input->post('group_name'),
			'excel_template'      => $this->input->post('template'),
			'group_updated_date' => date("Y-m-d H:i:s",time()),
			'group_updated_user' => $this->session->userdata('users_id')
		);

		$this->group_model->update_group($group,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！此筆群組已更新完成。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('register_key');
	}

	// 編輯表單
	public function check_form(){
		$id = $this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('register_key');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯群組';
		
		$data['result'] = $this->group_model->getidData($id);
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/register_key/check',$data);
	}

	//生成亂碼
	function random_keys($length){
		$output='';
		$string_array = [];
		for($i = 0;$i <= 9;$i++){
			array_push($string_array,$i);
		}  

		for($i = 65;$i <= 90;$i++){
			array_push($string_array,chr($i));
		}  

		for ($a = 0; $a<$length; $a++) {
			$index = rand(0,count($string_array)-1);
			$output .= $string_array[$index]; 
		}   
		return $output;   
	}   
}
