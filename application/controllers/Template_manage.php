<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template_manage extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/excel_template_model');
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/pagintion_model');
		$this->unitName = '新增/建立可用模板';
		$this->competence_id = $this->session->userdata('competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);
		$this->load->library('apiconnection');

		$admin_id = $this->session->userdata('users_id');
		$username = $this->session->userdata('users_name');
		$this->session->set_flashdata('sidebarselected','template_manage');
		$this->session->set_flashdata('mainsidebar','service_side_manage');

		if(!$admin_id){
			redirect('home/login');
		}
	}

    //Excel模板管理列表
	public function index(){
		redirect('template_manage/list');
		exit();
	}
    //Excel模板管理列表
	public function list($page=''){
		$organization_id = $this->session->userdata('organization_id');
		$manager_id = $this->session->userdata('manager_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有瀏覽的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('home');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = 'Excel模板管理列表';
		$data['active'] = 'employee';

		$dataArr = [];
		$url = 'http://34.80.38.39/rgms/api/template';
		$keyword['fullname'] = $this->input->post('fullname');
		
		$result = json_decode($this->apiconnection->crul_connect($dataArr,$url,'GET'),true)['Data'];		
		$pager = $this->pagintion_model->setPager($result,$page);
		$data['page_list'] = $pager['page'];
		$data['result'] = json_decode($this->apiconnection->crul_connect($dataArr,$url,'GET'),true)['Data'];
		
		//選單
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/template_manage/list',$data);	
	}

	//跳到指定頁面
	public function pagejump(){
		if($this->input->post('pagenum') == null || $this->input->post('pagenum') == 0){
			redirect('template_manage/list');
		}
		else{
			redirect('template_manage/list/'.$this->input->post('pagenum').'');
		}
	}

    // 新增表單
	public function create_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_insert'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有新增的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('excel_template');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '新增Excel模板管理';

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$this->session->set_flashdata('sidebarselected','excel_template');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/template_manage/create',$data);
	}

	// 新增Excel模板管理執行
	public function create(){
		$local_file = $_FILES['template']['tmp_name'];
		$filename = basename($_FILES['template']['name']);
        // $fields['file'] = new CURLFile($local_file);
		$fields = array(
			'uploadfile' => curl_file_create($local_file, $_FILES['template']['type'], $filename)
		);
		$url = 'http://34.80.38.39/rgms/api/template';
		$ch = curl_init();
		$opt = array(
			CURLOPT_URL            => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $fields,
		);
		curl_setopt_array($ch, $opt);
		$response = curl_exec($ch);
		$info = curl_getinfo($ch);
		echo "執行時間".$info['total_time'].PHP_EOL;
		curl_close($ch);
		// print_r($response);   
		redirect('template_manage');
	}

	// 刪除執行
	public function delete(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_delete'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有刪除的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('template_manage');
				}
			}
		}

		$dataArr = array(
			'excel_template_id'           => $id,
			'excel_template_is_del'       => 1,
			'excel_template_updated_user' => $user_id,
			'excel_template_updated_date' => date("Y-m-d H:i:s", time()),
		);
		$url = 'http://34.80.38.39/rgms/api/template';

		$this->apiconnection->crul_connect($dataArr, $url, 'PUT');

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆Excel模板管理已刪除。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('template_manage');
	}

	// 下架執行
	public function excel_template_invisible(){
		$id = $this->input->get('id');  //selected excel_template_id
		$user_id = $this->session->userdata('users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_enable'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv', $messagediv);
					redirect('template_manage');
				}
			}
		}

		$dataArr = array(
			'excel_template_id'           => $id,
			'excel_template_status'       => 0,
			'excel_template_updated_user' => $user_id,
			'excel_template_updated_date' => date("Y-m-d H:i:s", time()),
		);
		$url = 'http://34.80.38.39/rgms/api/template';

		$this->apiconnection->crul_connect($dataArr, $url, 'PUT');

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆Excel模板管理已下架。
		</div>";
		$this->session->set_flashdata('messagediv', $messagediv);
		redirect('template_manage');
	}

	// 上架執行
	public function excel_template_visible(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有上架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('template_manage');
				}
			}
		}

		$dataArr = array(
			'excel_template_id'           => $id,
			'excel_template_status'       => 1,
			'excel_template_updated_user' => $user_id,
			'excel_template_updated_date' => date("Y-m-d H:i:s", time()),
		);
		$url = 'http://34.80.38.39/rgms/api/template';

		$this->apiconnection->crul_connect($dataArr, $url, 'PUT');

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆Excel模板管理已啟用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('template_manage');
	}

	// 編輯表單
	public function update_form(){
		$id = $this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_update'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('excel_template');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯Excel模板管理';
		
		$data['result'] = $this->excel_template_model->getidData($id);
		$excel_template_rate_info = $this->bonus_rate_model->getonedata($id);
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/template_manage/update',$data);
	}

    // 編輯執行
	public function update(){
		$id = $this->input->post('id');
		$excel_template_rate_info = $this->bonus_rate_model->getonedata($id);

		$excel_template = array(
			'excel_template_name'         => $this->input->post('excel_templatename'),
			'excel_template_a_price'      => $this->input->post('texcel_templateprice'),
			'excel_template_updated_date' => date("Y-m-d H:i:s",time()),
			'excel_template_updated_user' => $this->session->userdata('users_id')
		);

		$this->excel_template_model->update_excel_template($excel_template,$id);

		$bonus_rate = array(
			'bonus_rate_interest_allowance' => $this->input->post('interest'),
			'bonus_rate_build_period'       => $this->input->post('build_period'),
			'bonus_rate_quarterly'          => $this->input->post('qinterest'),
			'bonus_rate_give'               => $this->input->post('times'),
			'bonus_rate_updated_date'       => date("Y-m-d H:i:s",time()),
			'bonus_rate_updated_user'       => $this->session->userdata('users_id')
		);

		$this->bonus_rate_model->update_bonus_rate($bonus_rate,$excel_template_rate_info['bonus_rate_id']);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！此筆Excel模板管理已更新完成。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('excel_template');
	}

	// 編輯表單
	public function check_form(){
		$id = $this->input->get('id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('excel_template');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '編輯Excel模板管理';
		
		$data['result'] = $this->excel_template_model->getidData($id);
		
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$data['upid'] = $this->input->get('id');
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/template_manage/check',$data);
	}
}
