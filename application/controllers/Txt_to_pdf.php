<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Txt_to_pdf extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/pagintion_model');
		$this->unitName = '產品管理';
		$this->competence_id = $this->session->userdata('competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);
		

		$admin_id = $this->session->userdata('users_id');
		$username = $this->session->userdata('users_name');
		$this->session->set_flashdata('sidebarselected','txt_to_pdf');
		$this->session->set_flashdata('mainsidebar','excel_create');
		$this->load->library('excel');

		if(!$admin_id){
			redirect('home/login');
		}
	}

    //產品列表
	public function index(){
		redirect('txt_to_pdf/list');
		exit;
	}
    //產品列表
	public function list($page=''){
		$organization_id = $this->session->userdata('organization_id');
		$manager_id = $this->session->userdata('manager_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有瀏覽的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('home');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '匯出報表列表';
		$data['active'] = 'employee';

		$keyword['fullname'] = $this->input->post('fullname');

		//選單
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/txt_to_pdf/create',$data);
	}

	//跳到指定頁面
	public function pagejump(){
		if($this->input->post('pagenum') == null || $this->input->post('pagenum') == 0){
			redirect('txt_to_pdf/list');
		}
		else{
			redirect('txt_to_pdf/list/'.$this->input->post('pagenum').'');
		}
	}

    // 新增表單
	public function create_form(){
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_insert'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有新增的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('txt_to_pdf');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '新增產品';

		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		$this->session->set_flashdata('sidebarselected','new_txt_to_pdf');

		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/txt_to_pdf/create',$data);
	}

	// 新增產品執行
	public function create(){
		$post = $this->input->post();
		//讀取來源檔txt
		$txt = fopen('C:\Users\user\Desktop\txt\19906D51_0000544461.txt','r');
		$txtcontent = fread($txt,filesize('C:\Users\user\Desktop\txt\19906D51_0000544461.txt'));
		$col = str_replace("\n","\t",$txtcontent);
		$col = explode("\t",$col);
		//讀取來源檔excel
		$objPHPExcel = PHPExcel_IOFactory::load('C:/xampp/htdocs/bms_model2/public/uploads/excel_model/SAP.xls');

		//設定excel樣板
		$srcfilename = 'C:\Users\user\Desktop\544461_52432993.xls';
		//設定匯出路徑與名稱
		$destfilename = 'C:\Users\user\Desktop\pdf_save\ssss.pdf';

		try {

			if(!file_exists($srcfilename)){

				return;

			}

			$excel = new \COM("excel.application") or die("Unable to instantiate excel");
			$excel->DisplayAlerts = 0;
			$workbook = $excel->Workbooks->Open($srcfilename, null, false, null, "1", "1", true);

			//txt寫入模板
			foreach($post['destPage'] as $key => $row){
				$selectedString = array_keys($col,$post['origString'][$key])[0];
				$value = $col[$selectedString+$post['origCol'][$key]];
				$sheets = $workbook->Worksheets((int)$row);
				$sheets->activate;
				$sheets->Cells($post['destRow'][$key],$post['destCol'][$key])->value = $value;
			}

			//excel寫入模板
			foreach($post['destPageExcel'] as $key => $row){
				$objPHPExcel->setActiveSheetIndex($row-1);
				$sheets = $workbook->Worksheets((int)$post['destPageExcel'][$key]);
				$sheets->activate;
				$result = iconv('UTF-8', 'BIG5', $objPHPExcel->getActiveSheet()->getCell($post['origColExcel'][$key].$post['origRowExcel'][$key])->getValue());
				$sheets->Cells($post['destRowExcel'][$key],$post['destColExcel'][$key])->value = $result;
			}
			$workbook->ExportAsFixedFormat(0, $destfilename,0,0,0,1);
			$workbook->SaveAs('C:\Users\user\Desktop\pdf_save\ssss.csv',6);
			$workbook->SaveAs('C:\Users\user\Desktop\pdf_save\ssss.xml',46);
			$workbook->SaveAs('C:\Users\user\Desktop\pdf_save\ssss.xls',56);
			$workbook->Close();
			$excel->Quit();
			unset($excel);

		} catch (\Exception $e) {

			echo ("src:$srcfilename catch exception:" . $e->__toString());

			if (method_exists($excel, "Quit")){

				$excel->Quit();

			}

			return;

		}
	}

	// 刪除執行
	public function delete(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_delete'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有刪除的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('txt_to_pdf');
				}
			}
		}

		$datetime = date("Y-m-d H:i:s"); 
		$txt_to_pdf = array(
			'txt_to_pdf_is_del'       => 1,
			'txt_to_pdf_updated_date' => date("Y-m-d H:i:s",time()),
			'txt_to_pdf_updated_user' => $this->session->userdata('users_id')
		);

		$this->txt_to_pdf_model->update_txt_to_pdf($txt_to_pdf,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆產品已刪除。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('txt_to_pdf');
	}

	// 下架執行
	public function txt_to_pdf_invisible(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7) {
				if($r['actions_enable'] == 0) {
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有下架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('txt_to_pdf');
				}
			}
		}

		$datetime = date("Y-m-d H:i:s"); 
		$txt_to_pdf = array(
			'txt_to_pdf_status'       => 0,
			'txt_to_pdf_updated_date' => date("Y-m-d H:i:s",time()),
			'txt_to_pdf_updated_user' => $this->session->userdata('users_id')
		);

		$this->txt_to_pdf_model->update_txt_to_pdf($txt_to_pdf,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆產品已停用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('txt_to_pdf');
	}

	// 上架執行
	public function txt_to_pdf_visible(){
		$id = $this->input->get('id');
		$user_id = $this->session->userdata('users_id');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 7)
			{
				if($r['actions_enable'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有上架的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('txt_to_pdf');
				}
			}
		}

		$datetime = date("Y-m-d H:i:s"); 

		$txt_to_pdf = array(
			'txt_to_pdf_status'       => 1,
			'txt_to_pdf_updated_date' => date("Y-m-d H:i:s",time()),
			'txt_to_pdf_updated_user' => $this->session->userdata('users_id')
		);

		$this->txt_to_pdf_model->update_txt_to_pdf($txt_to_pdf,$id);

		$messagediv = "<div class='alert alert-success'>
		<button class='close' data-dismiss='alert'></button>
		<div class='icon-ok'></div> 成功！該筆產品已啟用。
		</div>";
		$this->session->set_flashdata('messagediv',$messagediv);
		redirect('txt_to_pdf');
	}
}
