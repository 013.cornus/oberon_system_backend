<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_view extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('amadis_sys/competence_model');
		$this->load->model('amadis_sys/sidebarmenu_model');
		$this->load->model('amadis_sys/pagintion_model');
		$this->load->model('amadis_sys/group_info_model');
		$this->load->model('amadis_sys/ks/ks_users_model');
		$this->load->model('amadis_sys/ks/group_model');
		$this->unitName = '評量結果';
		$this->competence_id = $this->session->userdata('competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);
		

		$admin_id = $this->session->userdata('users_id');
		$username = $this->session->userdata('users_name');
		$this->session->set_flashdata('sidebarselected','report_view_2');
		$this->session->set_flashdata('mainsidebar','report_view_2');
		$this->load->library('excel');
		$this->load->library('apiconnection');

		if(!$admin_id){
			redirect('home/login');
		}
	}

    //報告列表
	public function index(){
		redirect('rgms_report/report_view/list');
		exit;
	}
    //報告列表
	public function list($page=''){
		$organization_id = $this->session->userdata('organization_id');
		$manager_id = $this->session->userdata('manager_id');
		$user_group_type = $this->session->userdata('group_type');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 60)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div> 您沒有瀏覽的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('home');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '報告列表';
		$data['active'] = 'employee';

		// $keyword['fullname'] = $this->input->post('fullname');

		$data['result'] = json_decode($this->apiconnection->apiConnect('','http://47.93.32.82/web/tw_api/api/detection/selectedData','post'),true)['Data'];
		$templateList = json_decode($this->apiconnection->apiConnect('','http://34.80.38.39/api/report_view/getTemplateList','post'),true);

		$templateName = [];
		if(isset($templateList)){
			foreach($templateList as $key => $row){
			$templateName[$row['excel_template_id']] = $row['excel_template_name'];
		}
		}

		$data['template'] = [];
		$data['templateShow'] = "style ='display:none'";
		if($user_group_type == 0){
			$data['template'] = json_decode($this->apiconnection->apiConnect('','http://34.80.38.39/api/report_view/getTemplateList','post'),true);
			$data['templateShow'] = '';
		}

		$groupInfo = $this->group_info_model->getDataByType($user_group_type);

		if(empty($groupInfo['excel_template'])){
			$messagediv = "<div class='alert'>
			<button class='close' data-dismiss='alert'></button>
			<div></div> 尚未設置模板,無法瀏覽報告。
			</div>";
			$this->session->set_flashdata('messagediv',$messagediv);
		}

		if($this->input->get('group_type')){
			if($this->session->userdata('group_type') != 0 && $this->input->get('group_type') != $this->session->userdata('group_type')){
				$messagediv = "<div class='alert'>
				<button class='close' data-dismiss='alert'></button>
				<div></div> 您沒有瀏覽的權限。
				</div>";
				$this->session->set_flashdata('messagediv',$messagediv);
				redirect('home');
			}
			$group_type = $this->input->get('group_type');
		}
		else{
			$group_type = $this->session->userdata('group_type');
		}

		foreach($data['result'] as $key => $row){
			$caseUserInfo = $this->ks_users_model->getIdData($row['id']);
			$groupInfo = $this->group_model->getIdData($caseUserInfo['group_id']);
			$groupData = $this->group_info_model->getDataByType($groupInfo['group_type']);
			if($user_group_type == 0){
				$data['link'][$key] = 'http://34.80.38.39/api/report_view/check_form?caseId='.$row['case'].'&date='.date('YmdHis',$row["date"]).'&userId='.$row['id'];
			}
			else{
				if(empty($groupData['excel_template'])){
					$data['link'][$key] = '';
				}
				else{
					$data['link'][$key] = 'http://34.80.38.39/api/report_view/check_form?caseId='.$row['case'].'&date='.date('YmdHis',$row["date"]).'&userId='.$row['id'].'&model='.$templateName[$groupData['excel_template']];
				}
			}

			if(($group_type != 0 && $group_type != $groupInfo['group_type']) || !isset($groupInfo['group_type'])){
				$data['viewRight'][$key] = 0;
			}
			else{
				$data['viewRight'][$key] = 1;
			}
		}
		
		//藉由分頁model分頁
		// $dataArr = array(
		// 	'limit1' => 0,
		// 	'limit2' => ''
		// );
		// $result = json_decode($this->apiconnection->apiConnectOuter($dataArr,'http://47.93.32.82/web/tw_api/api/detection/selectedData'),true)['Data'];
		// $pager = $this->pagintion_model->setPager($result,$page);

		// $data['page_list'] = $pager['page'];
		// $dataArr = array(
		// 	'limit1' => $pager['limit'][0],
		// 	'limit2' => $pager['limit'][1]
		// );
		// $data['result'] = json_decode($this->apiconnection->apiConnectOuter($dataArr,'http://47.93.32.82/web/tw_api/api/detection/selectedData'),true)['Data'];

		//選單
		$data['mainbar'] = $this->sidebarmenu_model->getsidebarmain();
		$data['subbar'] = $this->sidebarmenu_model->getsidebarsub();
		$data['view'] = $this->competence_model->getaction($this->competence_id);
		
		$data['footer'] = $this->load->view('amadis_sys/common/footer','', TRUE);
		$data['script'] = $this->load->view('amadis_sys/common/script','', TRUE);
		$this->load->view('amadis_sys/common/head');
		$this->load->view('amadis_sys/common/header');
		$this->load->view('amadis_sys/common/sidebar',$data);
		$this->load->view('amadis_sys/rgms_report/report_view/list',$data);	
	}

	//跳到指定頁面
	public function pagejump(){
		if($this->input->post('pagenum') == null || $this->input->post('pagenum') == 0){
			redirect('report_view/list');
		}
		else{
			redirect('report_view/list/'.$this->input->post('pagenum').'');
		}
	}

    //瀏覽報告
	public function check_form(){
		$id = $this->input->get('caseId');
		$date = $this->input->get('date');
		foreach($this->rightid as $r){
			if($r['sidebar_sub_id'] == 60)
			{
				if($r['actions_view'] == 0)
				{
					$messagediv = "<div class='alert'>
					<button class='close' data-dismiss='alert'></button>
					<div></div><div class=' icon-warning-sign'></div> 您沒有編輯的權限。
					</div>";
					$this->session->set_flashdata('messagediv',$messagediv);
					redirect('rgms_report/report_view');
				}
			}
		}
		$data = [];
		$data['title'] = $this->unitName;
		$data['title_small'] = '瀏覽報告';

		//excel表更新時間
		$updateTime = filemtime('C:/xampp/htdocs/bms_model/public/uploads/excel_model/JS_001.xlsx');

		//檔案命名規則:檢測id_檢測日期_模板修改時間
		$pdfLocation = 'C:/xampp/htdocs/bms_model/public/report/34.80.127.28/'.$id.'/'.$id.'_'.$date.'_'.$updateTime.'.pdf';
		if(!file_exists($pdfLocation)){
			$file_path = 'C:\xampp\htdocs\bms_model\public\report\/34.80.127.28/'.$id.'/';
			if(!file_exists($file_path)){
				mkdir($file_path);
			}
		    //查詢模板資料
			$template = $this->excel_template_model->getidData(4);

		    //設定excel樣板
			$srcfilename = 'C:/xampp/htdocs/bms_model/public/uploads/excel_model/JS_001.xlsx';
		    //設定匯出路徑與名稱
			// $destfilename = $file_path.$id.'/'.$id.'_'.$date.'.pdf';
			$destfilename = 'C:\xampp\htdocs\bms_model\public\report/'.$id.'/'.'test.pdf';

			try {

				if(!file_exists($srcfilename)){

					return;

				}

				$excel = new \COM("excel.application") or die("Unable to instantiate excel");
				$excel->DisplayAlerts = 0;
				$excel->ScreenUpdating = 0;
				$excel->DisplayStatusBar = 0;
				$excel->EnableEvents = 0;
				$workbook = $excel->Workbooks->Open($srcfilename, null, false, null, "1", "1", true);
				//查詢模板設定
				$seletion['excel_template_id'] = 4;
				$templateSetting = json_decode($this->report_template_setting_model->getDataBySelection($seletion)['report_template_setting_content'],true);

				//查詢受測者資料
				$value = $this->sourceDataToArray($id)['value'];

				foreach($templateSetting as $key => $row){
					$target = explode(',',$row);
					$sheets = $workbook->Worksheets((int)$target[0]);
					// $result = iconv('UTF-8', 'BIG5', $dataValue);

					$result = mb_convert_encoding($value[$key], 'BIG5');
					//判斷是否為圖片
					if(substr($result,-3) == 'jpg' || substr($result,-3) == 'png'){
						//判斷圖片網址是否失效
						if(fopen($result,'r')){
							$sheets->Shapes->AddPicture($result,False,True,$sheets->Range($target[1])->MergeArea->left,$sheets->Range($target[1])->MergeArea->top,$sheets->Range($target[1])->MergeArea->Width,$sheets->Range($target[1])->MergeArea->Height);
						}
					}
					else{
						// $col = substr($target[1],0,1);
						// $row = substr($target[1],1);
						// $sheets->cells($row,$col)->value = $result;
						$sheets->Range($target[1])->value = $result;
					}
				}

				$excel->ScreenUpdating = 1;
				$excel->DisplayStatusBar = 1;
				$excel->EnableEvents = 1;
				$workbook->SaveAs('C:\xampp\htdocs\bms_model\public\report\test.xlsx',51);
				$workbook->Close();
				$excel->Quit();
				unset($excel);

			} catch (\Exception $e) {

				echo ("src:$srcfilename catch exception:" . $e->__toString());
				if (method_exists($excel, "Quit")){

					$excel->Quit();

				}

				return;

			}

		    //設定excel樣板
			$srcfilename = 'C:\xampp\htdocs\bms_model\public\report\test.xlsx';
		    //設定匯出路徑與名稱
			$destfilename = $pdfLocation;

			try {

				if(!file_exists($srcfilename)){

					return;

				}

				$excel = new \COM("excel.application") or die("Unable to instantiate excel");
				$excel->DisplayAlerts = 0;
				$workbook2 = $excel->Workbooks->Open($srcfilename, null, false, null, "1", "1", true);
				$workbook2->ExportAsFixedFormat(0, $destfilename,0,0,0,1);
				$workbook2->Close();
				$excel->Quit();
				unset($excel);

			} catch (\Exception $e) {

				echo ("src:$srcfilename catch exception:" . $e->__toString());
				if (method_exists($excel, "Quit")){

					$excel->Quit();

				}

				return;

			}

			header("Content-type:application/pdf");
			readfile($pdfLocation);
		}
		else{
			header("Content-type:application/pdf");
			readfile($pdfLocation);
		}
	}

	//將來源json轉為陣列
	public function sourceDataToArray($case='980'){
		$returnData = json_decode($this->apiconnection->apiConnect('','http://47.93.32.82/web/tw_api/api/report?item='.$case,'get'),true)['Data'];
		// $groupName = array();

		$keyName = array();
		$value = array();

		foreach($returnData as $key => $row){
			if(is_array($row)){
				foreach($row as $key2 => $row2){
					if(is_array($row2)){
						foreach($row2 as $key3 => $row3){
							if(is_array($row3)){
								foreach($row3 as $key4 => $row4){
									if(is_array($row4)){
										foreach($row4 as $key5 => $row5){
											if(is_array($row5)){
												foreach($row5 as $key6 => $row6){
													array_push($keyName,$key6);
													array_push($value,$row6);
												}
											}
											else{
												array_push($keyName,$key5);
												array_push($value,$row5);
											}
										}
									}
									else{
										array_push($keyName,$key4);
										array_push($value,$row4);
									}
								}
							}
							else{
								array_push($keyName,$key3);
								array_push($value,$row3);
							}
						}
					}
					else{
						array_push($keyName,$key2);
						array_push($value,$row2);
					}
				}
			}
			else{
				array_push($keyName,$key);
				array_push($value,$row);
			}
		}

		$result = array(
			'keyName' => $keyName,
			'value'    => $value,
		);
		
		return $result;
	}

	//建立主帳號
	public function add_group(){
		for($i = 65 ; $i <= 79; $i++){
			$group = array(
				'group_name' => chr($i),
				'group_type' => 8,
				'group_password' => md5('123456')
			);

			$this->group_model->add_group($group);

			// $group_info = $this->group_model->getKeyData('group_name',chr($i));

			// for($j = 1;$j <= 20; $j++){
			// 	$users = array(
			// 		'group_id' => $group_info['group_id'],
			// 	);

			// 	$users_info = array(
			// 		'users_info_name' => chr($i).str_pad($j,2,'0',STR_PAD_LEFT)
			// 	);
			// }

		}
	}
}
