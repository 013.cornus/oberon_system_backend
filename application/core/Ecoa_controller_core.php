<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ecoa_controller_core extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->unitName = '客戶管理';
		$this->competence_id = $this->session->userdata('competence_id');
		$this->rightid = $this->competence_model->getaction($this->competence_id);
		
		$admin_id = $this->session->userdata('users_id');
		$username = $this->session->userdata('users_name');

		$this->session->set_flashdata('sidebarselected','customers');
		$this->session->set_flashdata('mainsidebar','customer');

		if(!$admin_id){
			redirect('home/login');
		}
	}
}
