<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_model extends CI_model{

    //查詢所有帳號
    public function getdata($keyword){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('users_is_del',0);

        if (isset($keyword) && $keyword != '') {
            $this->db->like('users_name',$keyword);
            // $this->db->or_like('users_account',$keyword);
        }
        
        $query = $this->db->get();
        return $query->result_array();
    }

    // 查詢所有帳號
    public function getList($keyword,$limit){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('users_is_del',0);
        // $this->db->where('users_status',1);
        if (!empty($this->session->userdata('competence_id')) && $this->session->userdata('competence_id') != 1) {
            $this->db->where_not_in('users_id',1);
            $this->db->where_not_in('users_id',2);
            $this->db->where_not_in('users_id',3);
            $this->db->where_not_in('users_id',4);
            $this->db->where_not_in('users_id',5);
            // $this->db->where_not_in('users_id',6);
        }
        
        if (isset($keyword['users_name']) && $keyword['users_name'] != '') {
            $this->db->like('users_name',$keyword['users_name']);
        }

        if (isset($keyword['organization_id']) && $keyword['organization_id'] != '') {
            $this->db->group_start();
            $this->db->where('organization_id',$keyword['organization_id']);
            $this->db->or_where('organization_id',0);
            $this->db->group_end();
        }

        if (isset($keyword['competence_id']) && $keyword['competence_id'] != '') {
            $this->db->where('competence_id >=',$keyword['competence_id']);
        }

        if (isset($keyword['competence']) && $keyword['competence'] != '') {
            $this->db->where('competence_id',$keyword['competence']);
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        $result = $this->db->get();
        $result = $result->result_array();
        return $result;
    }

    //查詢單筆帳號
    public function getonedata($id){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('users_id',$id);
        $this->db->where('users_is_del',0);
        $this->db->where('users_status',1);

        $query = $this->db->get();
        return $query->row_array();
    }

    //新增帳號
    public function add_account($account){

        $this->db->insert('users',$account);

    }

    //修改帳號
    public function update_account($account,$id){
        $this->db->update('users',$account,array('users_id' => $id));
    }

    //變更密碼
    public function changepassword($newpassword,$id){
        $this->db->update('users',$newpassword,array('users_id' => $id));
    }
}
