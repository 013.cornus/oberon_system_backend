<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Address_model extends CI_model{

    //所有地址
	public function getList(){
		$this->db->select('*');
		$this->db->from('address');
		$this->db->where('address_is_del',0);
		$this->db->where('address_status',1);

		$query = $this->db->get();
		return $query->result_array();
	}
	

	//撈出縣市
    public function getcity(){
        $this->db->select('*');
        $this->db->from('address');

        $query = $this->db->get();
        return $query->result_array();
    }
}
