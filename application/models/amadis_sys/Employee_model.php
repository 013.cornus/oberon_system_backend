<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_model extends CI_model{
    //查詢所有會員資料
    public function getidData($id){
        $this->db->select('*');
        $this->db->from('employee');
        $this->db->where('employee_id',$id);
        $this->db->where('employee_is_del',0);

        $query = $this->db->get();
        return $query->row_array();
    }
    
    public function getList($keyword='',$limit=''){
        $this->db->select('*');
        $this->db->from('employee');
        $this->db->where('employee_is_del',0);
        $this->db->order_by('employee_id');
        // $this->db->where('employee_status',1);
        
        if (isset($keyword['orgid']) && $keyword['orgid'] != '') {
            $this->db->where('organization_id',$keyword['orgid']);
        }
         if (isset($keyword['status']) && $keyword['status'] != '') {
            $this->db->where('employee_status',$keyword['status']);
        }
        if (isset($keyword['emplyee_id']) && $keyword['emplyee_id'] != '') {
            $this->db->where('employee_id',$keyword['emplyee_id']);
        }
        if (isset($keyword['employee_name']) && $keyword['employee_name'] != '') {
            $this->db->like('employee_name',$keyword['employee_name']);
        }
        if (isset($keyword['organization_id']) && $keyword['organization_id'] != '') {
            $this->db->where('organization_id',$keyword['organization_id']);
        }

        //篩選晉升條件
        if (isset($keyword['promoteem']) && $keyword['promoteem'] != null) {
            foreach($keyword['promoteem'] as $row){
                $this->db->where('employee_id !=',$row);
            }
        }

        //篩選達標條件
        if (isset($keyword['goalem']) && $keyword['goalem'] != null) {
            $this->db->group_start();
            foreach($keyword['goalem'] as $key => $value){
                if($key == 0){
                    $this->db->where('employee_id',$value);
                }
                else{
                    $this->db->or_where('employee_id',$value);
                }
            }
            $this->db->group_end();
        }

        //篩選適用考核人員
        if (isset($keyword['except_occupation']) && $keyword['except_occupation'] != null) {
            $this->db->group_start();
            foreach($keyword['except_occupation'] as $key => $row){
                if($key == 0){
                    $this->db->where('occupation_id',$row);
                }
                else{
                    $this->db->or_where('occupation_id',$row);
                }
            }
            $this->db->group_end();
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    //新增會員
    public function add_employee($employee){

        $this->db->insert('employee',$employee);

    }

    //修改會員
    public function update_employee($employeeupdate,$id){
        $this->db->update('employee',$employeeupdate,array('employee_id' => $id));
    }

    public function get_certain_col_goal($keyword =""){
        $this->db->select('employee_id,occupation_id');
        $this->db->from('employee');
        $this->db->where('employee_is_del',0);
        $this->db->where('occupation_id !=',1);
        $this->db->where('occupation_id !=',2);
        $this->db->where('occupation_id !=',6);
        // $this->db->where('employee_status',1);
        
        if (isset($keyword['orgid']) && $keyword['orgid'] != '') {
            $this->db->where('organization_id',$keyword['orgid']);
        }
        if (isset($keyword['emplyee_id']) && $keyword['emplyee_id'] != '') {
            $this->db->where('employee_id',$keyword['emplyee_id']);
        }
        if (isset($keyword['employee_name']) && $keyword['employee_name'] != '') {
            $this->db->where('employee_name',$keyword['employee_name']);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_evaluation_orgidem($id){
        $this->db->select('*');
        $this->db->from('employee');
        $this->db->where('organization_id',$id);
        $this->db->where('employee_is_del',0);
        $this->db->where('employee_status',1);
        $this->db->group_start();
        $this->db->where('occupation_id',3);
        $this->db->or_where('occupation_id',4);
        $this->db->or_where('occupation_id',5);
        $this->db->or_where('occupation_id',11);
        $this->db->group_end();

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getdownline($id){
        $this->db->select('*');
        $this->db->from('employee');
        $this->db->where('users_id',$id);
        $this->db->where('employee_is_del',0);
        $this->db->where('employee_status',1);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_last_id(){
        $this->db->select_max('employee_id','maxid');
        $this->db->from('employee');
        $this->db->where('employee_is_del',0);
        $this->db->where('employee_status',1);

        $query = $this->db->get();
        return $query->row_array();
    }
    
    public function get_evaluation_employee(){
        $this->db->select('*');
        $this->db->from('employee');
        $this->db->where('employee_is_del',0);
        $this->db->where('employee_status',1);
        $this->db->group_start();
        $this->db->where('occupation_id',3);
        $this->db->or_where('occupation_id',4);
        $this->db->or_where('occupation_id',5);
        $this->db->or_where('occupation_id',11);
        $this->db->group_end();

        $query = $this->db->get();
        return $query->result_array();
    }

    //撈出單一會員整條下線
    public function get_all_downline($id){
        $query = $this->db->query('select employee_id,
            employee_name,
            employee_is_del,
            occupation_id,
            users_id
            from  (select * from employee where employee_is_del = 0
            order by users_id, employee_id) products_sorted,
            (select @pv := '.$id.') initialisation
            where   find_in_set(users_id, @pv)
            and     length(@pv := concat(@pv,",", employee_id));');
        return $query->result_array();
    }
}
