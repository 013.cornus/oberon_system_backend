<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Excel_template_model extends CI_model{

    public function getidData($id){
        $this->db->select('*');
        $this->db->from('excel_template');
        $this->db->where('excel_template_id',$id);
        $this->db->where('excel_template_is_del',0);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getList($keyword='',$limit=''){
        $this->db->select('*');
        $this->db->from('excel_template');
        $this->db->where('excel_template_is_del',0);
        if (isset($keyword['fullname']) && $keyword['fullname'] != '') {
            $this->db->like('excel_template_name',$keyword['fullname']);
        }
        if (isset($keyword['status']) && $keyword['status'] != '') {
            $this->db->where('excel_template_status',$keyword['status']);
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    //新增產品
    public function add_excel_template($excel_template){

        $this->db->insert('excel_template',$excel_template);

    }

    //修改產品
    public function update_excel_template($excel_template,$id){
        $this->db->update('excel_template',$excel_template,array('excel_template_id' => $id));
    }

    //撈出最新商品id
    public function get_latest_id(){
        $this->db->select_max('excel_template_id','maxid');
        $this->db->from('excel_template');

        $query = $this->db->get();
        return $query->row_array();
    }
}
