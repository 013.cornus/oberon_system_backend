<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group_info_model extends CI_model{

    public function getidData($id){
        $this->db->select('*');
        $this->db->from('group_info');
        $this->db->where('group_info_id',$id);
        $this->db->where('group_info_is_del',0);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getDataByType($type){
        $this->db->select('*');
        $this->db->from('group_info');
        $this->db->where('group_info_type',$type);
        $this->db->where('group_info_is_del',0);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getList($keyword='',$limit=''){
        $this->db->select('*');
        $this->db->from('group_info');
        $this->db->where('group_info_is_del',0);
        if (isset($keyword['fullname']) && $keyword['fullname'] != '') {
            $this->db->like('group_info_name',$keyword['fullname']);
        }
        if (isset($keyword['status']) && $keyword['status'] != '') {
            $this->db->where('group_info_status',$keyword['status']);
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    //新增產品
    public function add_group_info($group_info){

        $this->db->insert('group_info',$group_info);

    }

    //111以下最大銷售/服務端號碼
    public function max_group_type(){
        $this->db->select_max('group_info_type','group_info_type');
        $this->db->from('group_info');
        $this->db->where('group_info_type <',111);

        $query = $this->db->get();
        return $query->row_array()['group_info_type'];
    }

    //修改產品
    public function update_group_info($group_info,$id){
        $this->db->update('group_info',$group_info,array('group_info_id' => $id));
    }

    //撈出最新商品id
    public function get_latest_id(){
        $this->db->select_max('group_info_id','maxid');
        $this->db->from('group_info');

        $query = $this->db->get();
        return $query->row_array();
    }
}
