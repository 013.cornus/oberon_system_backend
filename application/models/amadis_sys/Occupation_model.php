<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Occupation_model extends CI_model{
    // 查詢所有職級
    public function getList($keyword = '',$limit = ''){
        $this->db->select('*');
        $this->db->from('occupation');
        $this->db->where('occupation_is_del',0);
        // $this->db->where('organization_status',1);

        if (isset($keyword['fullname']) && $keyword['fullname'] != '') {
            $this->db->like('occupation_name',$keyword['fullname']);
        }
        if (isset($keyword['status']) && $keyword['status'] != '') {
            $this->db->where('occupation_status',$keyword['status']);
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        $result = $this->db->get();
        $result = $result->result_array();
        return $result;
    }

    //查詢單個職級資料
    public function getidData($id){
        $this->db->select('*');
        $this->db->from('occupation');
        $this->db->where('occupation_id',$id);
        $this->db->where('occupation_status',1);
        $this->db->where('occupation_is_del',0);

        $query = $this->db->get();
        return $query->row_array();
    }

    //查詢最後一筆職級ID
    public function getlatestid(){
        $this->db->select_Max('occupation_id','maxid');  
        
        $query = $this->db->get('occupation');
        return $query->result_array();
    }

    //新增職級
    public function add_occupation($occupation){
        $this->db->insert('occupation',$occupation);
    }

    //修改職級
    public function update_occupation($occupation,$id){
        $this->db->update('occupation',$occupation,array('occupation_id' => $id));
    }
}
