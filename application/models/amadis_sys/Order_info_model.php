<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_info_model extends CI_model{

    //搜尋所有資料
    public function getList($keyword='',$limit=''){
        $this->db->select('*');
        $this->db->from('order_info');
        $this->db->where('order_info_is_del',0);
        $this->db->where('order_info_status',1);

        if (isset($keyword) && $keyword != '') {
           $this->db->where('order_id',$keyword);
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getidData($id){
        $this->db->select('*');
        $this->db->from('order_info');
        $this->db->where('order_info_id',$id);
        $this->db->where('order_info_is_del',0);
        $this->db->where('order_info_status',1);
      
        $query = $this->db->get();
        return $query->row_array();
    }

    //新增訂單資訊
    public function add_order_info($order_info){

        $this->db->insert('order_info',$order_info);

    }

    //搜尋所有股權
    public function getequity($keyword='',$limit=''){
        $this->db->select('*');
        $this->db->from('order_info');
        $this->db->join('order','order.order_id=order_info.order_id');
        $this->db->where('goods_id',4);
        $this->db->where('order_is_check',1);
        $this->db->where('order_info_is_del',0);
        $this->db->where('order_info_status',1);
        $this->db->order_by('order_info_created_date','asc');

        if (isset($keyword['contract_id']) && $keyword['contract_id'] != '') {
           $this->db->like('contract_id',$keyword['contract_id']);
        }

        if (isset($keyword['employeeid']) && $keyword['employeeid'] != '') {
           $this->db->where('employee_id',$keyword['employeeid']);
        }

        if (isset($keyword['customid']) && $keyword['customid'] != '') {
           $this->db->where('customer_id',$keyword['customid']);
        }


        if (isset($keyword['orgid']) && $keyword['orgid'] != '') {
           $this->db->where('organization_id',$keyword['orgid']);
        }

        if (isset($keyword['year']) && $keyword['year'] != '') {
            $this->db->where('year(order_info_created_date)',$keyword['year']);
        }

        if (isset($keyword['month']) && $keyword['month'] != '') {
            $this->db->where('month(order_info_created_date)',$keyword['month']);
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    //搜尋所有股權
    public function getallequity($keyword = ''){
        $this->db->select('*');
        $this->db->from('order_info');
        $this->db->join('order','order.order_id=order_info.order_id');
        $this->db->where('goods_id',4);
        $this->db->where('order_is_del',0);
        $this->db->where('order_status',1);
        $this->db->where('order_info_is_del',0);
        $this->db->where('order_info_status',1);

        if (isset($keyword['contract_id']) && $keyword['contract_id'] != '') {
           $this->db->where('contract_id',$keyword['contract_id']);
        }

        if (isset($keyword['employeeid']) && $keyword['employeeid'] != '') {
           $this->db->where('employee_id',$keyword['employeeid']);
        }

        if (isset($keyword['customid']) && $keyword['customid'] != '') {
           $this->db->where('customer_id',$keyword['customid']);
        }


        if (isset($keyword['orgid']) && $keyword['orgid'] != '') {
           $this->db->where('organization_id',$keyword['orgid']);
        }

        if (isset($keyword['year']) && $keyword['year'] != '') {
            $this->db->where('year(order_info_created_date)',$keyword['year']);
        }

        if (isset($keyword['month']) && $keyword['month'] != '') {
            $this->db->where('month(order_info_created_date)',$keyword['month']);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    //搜尋所有股權
    public function getorgequity($id){
        $this->db->select('*');
        $this->db->from('order_info');
        $this->db->join('order','order.order_id=order_info.order_id');
        $this->db->where('goods_id',4);
        $this->db->where('organization_id',$id);
        $this->db->where('order_is_del',0);
        $this->db->where('order_status',1);
        $this->db->where('order_info_is_del',0);
        $this->db->where('order_info_status',1);

        $query = $this->db->get();
        return $query->result_array();
    }

    //搜尋營業處總單位
    public function get_org_totalcount($id,$time =''){
        $this->db->select_sum('order_info_count','total');
        $this->db->from('order_info');
        $this->db->join('order','order.order_id=order_info.order_id');
        if($time != ''){
            $this->db->where('year(order_info_created_date)',date("Y",strtotime($time)));
            $this->db->where('month(order_info_created_date)',date("m",strtotime($time)));
        }
        $this->db->where('organization_id',$id);
        $this->db->where('order_is_del',0);
        $this->db->where('order_is_check',1);
        $this->db->where('order_status',1);
        $this->db->where('order_info_is_del',0);

        $query = $this->db->get();
        return $query->row_array();
    }

    //搜尋營業處總pv
    public function get_org_totalpv($id,$time =''){
        $this->db->select('sum(`order_info_count`*`goods_a_pv`) as monthpv', FAlSE);
        $this->db->from('order_info');
        $this->db->join('order','order.order_id=order_info.order_id');
        $this->db->join('goods','goods.goods_id=order_info.product_id');
        if($time != ''){
            $this->db->where('year(order_info_created_date)',date("Y",strtotime($time)));
            $this->db->where('month(order_info_created_date)',date("m",strtotime($time)));
        }
        $this->db->where('organization_id',$id);
        $this->db->where('order_is_del',0);
        $this->db->where('order_is_check',1);
        $this->db->where('order_status',1);
        $this->db->where('order_info_is_del',0);
        $this->db->where('order_info_status',1);

        $query = $this->db->get();
        return $query->row_array();
    }

    //查詢單個合約詳細資訊
    public function getcertaininfo($id){
        $this->db->select('*');
        $this->db->from('order_info');
        $this->db->where('order_id',$id);
        $this->db->where('order_info_is_del',0);
        $this->db->where('order_info_status',1);
      
        $query = $this->db->get();
        return $query->result_array();
    }

    //查詢單筆合約詳細資訊
    public function getOrderIdInfo($id){
        $this->db->select('*');
        $this->db->from('order_info');
        $this->db->where('order_id',$id);
        $this->db->where('order_info_is_del',0);
        $this->db->where('order_info_status',1);
      
        $query = $this->db->get();
        return $query->row_array();
    }

    //刪除orderinfo
    public function delete_orderinfo($id,$datetime,$user_id){
        $this->db->where('order_info_id',$id);
        $this->db->set('order_info_is_del',1); 
        $this->db->set('order_info_updated_date',$datetime);
        $this->db->set('order_info_updated_user',$user_id);
        $this->db->update('order_info');
    }

    //修改orderinfo
    public function update_orderinfo($info,$id){
        $this->db->update('order_info',$info,array('order_info_id' => $id));
    }

    //查詢單個合約股權
    public function getcertainequity($id){
        $this->db->select('*');
        $this->db->from('order_info');
        $this->db->where('goods_id',4);
        $this->db->where('order_id',$id);
        $this->db->where('order_info_is_del',0);
        $this->db->where('order_info_status',1);
      
        $query = $this->db->get();
        return $query->row_array();
    }

    //搜尋當月已審核銷售總額(用到join)
    public function get_Current_List($selection = ''){
        $this->db->select('sum(`order_info_count`*`goods_a_price`) as monthsale', FAlSE);
        $this->db->from('order_info');
        $this->db->join('order','order.order_id=order_info.order_id');
        $this->db->join('goods','goods.goods_id=order_info.product_id');
        if (isset($selection['employeeid']) && $selection['employeeid'] != '') {
           $this->db->where('employee_id',$keyword['employeeid']);
        }

        if (isset($selection['organization_id']) && $selection['organization_id'] != '') {
           $this->db->where('organization_id',$selection['organization_id']);
        }
        $this->db->where('year(order_active_date)',date("Y",time()));
        $this->db->where('month(order_active_date)',date("m",time()));
        $this->db->where('order_is_check',1);
        $this->db->where('order_info_is_del',0);
        $this->db->where('order_info_status',1);
        
        $query = $this->db->get();
        return $query->row_array();
    }

    //搜尋當月未審核銷售總額(用到join)
    public function get_Current_Unchecked_List($selection = ''){
        $this->db->select('sum(`order_info_count`*`goods_a_price`) as unchecked_monthsale', FAlSE);
        $this->db->from('order_info');
        $this->db->join('order','order.order_id=order_info.order_id');
        $this->db->join('goods','goods.goods_id=order_info.product_id');
        if (isset($selection['employeeid']) && $selection['employeeid'] != '') {
           $this->db->where('employee_id',$keyword['employeeid']);
        }

        if (isset($selection['organization_id']) && $selection['organization_id'] != '') {
           $this->db->where('organization_id',$selection['organization_id']);
        }
        $this->db->where('year(order_active_date)',date("Y",time()));
        $this->db->where('month(order_active_date)',date("m",time()));
        $this->db->group_start();
        $this->db->where('order_is_check',0);
        $this->db->or_where('order_is_check',2);
        $this->db->group_end();
        $this->db->where('order_info_is_del',0);
        $this->db->where('order_info_status',1);
        
        $query = $this->db->get();
        return $query->row_array();
    }

    //搜尋上月已審核銷售總額(用到join)
    public function get_last_List($selection = '',$lastmonth){
        $this->db->select('sum(`order_info_count`*`goods_a_price`) as lastsale', FAlSE);
        $this->db->from('order_info');
        $this->db->join('order','order.order_id=order_info.order_id');
        $this->db->join('goods','goods.goods_id=order_info.product_id');
        if (isset($selection['employeeid']) && $selection['employeeid'] != '') {
           $this->db->where('employee_id',$keyword['employeeid']);
        }

        if (isset($selection['organization_id']) && $selection['organization_id'] != '') {
           $this->db->where('organization_id',$selection['organization_id']);
        }
        $this->db->where('year(order_active_date)',date("Y",strtotime($lastmonth)));
        $this->db->where('month(order_active_date)',date("m",strtotime($lastmonth)));
        $this->db->where('order_is_check',1);
        $this->db->where('order_info_is_del',0);
        $this->db->where('order_info_status',1);
        
        $query = $this->db->get();
        return $query->row_array();
    }

    //搜尋員工pv
    public function get_employee_pv($id,$time =''){
        $this->db->select('sum(`order_info_count`*`goods_a_pv`) as pv', FAlSE);
        $this->db->from('order_info');
        $this->db->join('order','order.order_id=order_info.order_id');
        $this->db->join('goods','goods.goods_id=order_info.product_id');
        if($time != ''){
            $this->db->where('year(order_info_created_date)',date("Y",strtotime($time)));
            $this->db->where('month(order_info_created_date)',date("m",strtotime($time)));
        }
        $this->db->where('order_is_check',1);
        $this->db->where('employee_id',$id);
        $this->db->where('order_info_is_del',0);
        $this->db->where('order_info_status',1);
        
        $query = $this->db->get();
        return $query->row_array();
    }

    //搜尋員工季pv
    public function get_employee_qpv($id,$date1,$date2){
        $this->db->select('sum(`order_info_count`*`goods_a_pv`) as pv', FAlSE);
        $this->db->from('order_info');
        $this->db->join('order','order.order_id=order_info.order_id');
        $this->db->join('goods','goods.goods_id=order_info.product_id');
        $this->db->where('order_info_created_date >=',$date1);
        $this->db->where('order_info_created_date <=',$date2);
        $this->db->where('order_is_check',1);
        $this->db->where('employee_id',$id);
        $this->db->where('order_info_is_del',0);
        $this->db->where('order_info_status',1);
        
        $query = $this->db->get();
        return $query->row_array();
    }

    //搜尋當月員工pv
    public function get_month_employee_pv($id,$time =''){
        $this->db->select('sum(`order_info_count`*`goods_a_pv`) as pv', FAlSE);
        $this->db->from('order_info');
        $this->db->join('order','order.order_id=order_info.order_id');
        $this->db->join('goods','goods.goods_id=order_info.product_id');
        $this->db->where('year(order_info_created_date)',date("Y",time()));
        $this->db->where('month(order_info_created_date)',date("m",time()));
        $this->db->where('employee_id',$id);
        $this->db->where('order_is_check',1);
        $this->db->where('order_info_is_del',0);
        $this->db->where('order_info_status',1);
        
        $query = $this->db->get();
        return $query->row_array();
    }
}
