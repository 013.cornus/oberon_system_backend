<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_model extends CI_model{
    
    public function getList($keyword='',$limit=''){
        $this->db->select('*');
        $this->db->from('order');
        $this->db->where('order_is_del',0);
        // $this->db->where('order_status',1);

        if (isset($keyword['customerid']) && $keyword['customerid'] != '') {
            $this->db->where('customer_id',$keyword['customerid']);
        }

        if (isset($keyword['year']) && $keyword['year'] != '') {
            $this->db->where('year(order_active_date)',$keyword['year']);
        }

        if (isset($keyword['month']) && $keyword['month'] != '') {
            $this->db->where('month(order_active_date)',$keyword['month']);
        }

        if (isset($keyword['employeeid']) && $keyword['employeeid'] != '') {
            $this->db->where('employee_id',$keyword['employeeid']);
        }

        if (isset($keyword['contract_id']) && $keyword['contract_id'] != '') {
            $this->db->like('contract_id',$keyword['contract_id']);
        }

        if (isset($keyword['employee_id']) && $keyword['employee_id'] != '') {
            $this->db->where('employee_id',$keyword['employee_id']);
        }
        if (isset($keyword['customer_id']) && $keyword['customer_id'] != '') {
            $this->db->where('customer_id',$keyword['customer_id']);
        }
        if (isset($keyword['organization_id']) && $keyword['organization_id'] != '') {
            $this->db->where('organization_id',$keyword['organization_id']);
        }
        if (isset($keyword['orgid']) && $keyword['orgid'] != '') {
            $this->db->where('organization_id',$keyword['orgid']);
        }
        if (isset($keyword['status']) && $keyword['status'] != '') {
            $this->db->where('order_status',$keyword['status']);
        }

         if (isset($keyword['check_status']) && $keyword['check_status'] != '') {
            $this->db->where('order_is_check',$keyword['check_status']);
        }

        if($this->session->userdata('competence_id') == 4){
            $this->db->group_start();
            foreach($keyword['employee_list'] as $key => $employeelist){
                if($key == 0){
                    $this->db->where('employee_id',$employeelist);
                }
                else{
                    $this->db->or_where('employee_id',$employeelist);
                }
            }
            $this->db->group_end();
        }
        
        if ($limit) {
            $this->db->limit($limit);
        }

        $this->db->order_by("order_active_date", "desc");
        $this->db->order_by("order_created_date", "desc");

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getidData($id){
        $this->db->select('*');
        $this->db->from('order');
        $this->db->where('order_id',$id);
        $this->db->where('order_is_del',0);
        $this->db->where('order_status',1);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getselectorder($keyword = ''){
        $this->db->select('*');
        $this->db->from('order');
        $this->db->where('order_is_check',1);
        $this->db->where('order_status',1);
        $this->db->where('order_is_del',0);

        if (isset($keyword['year']) && $keyword['year'] != '') {
            $this->db->where('year(order_active_date)',$keyword['year']);
        }

        if (isset($keyword['month']) && $keyword['month'] != '') {
            $this->db->where('month(order_active_date)',$keyword['month']);
        }

        if (isset($keyword['emid']) && $keyword['emid'] != '') {
            $this->db->where('employee_id',$keyword['emid']);
        }

        if (isset($keyword['organization_id']) && $keyword['organization_id'] != '') {
            $this->db->where('organization_id',$keyword['organization_id']);
        }

        if (isset($keyword['customid']) && $keyword['customid'] != '') {
            $this->db->where('customer_id',$keyword['customid']);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    //查詢最後一筆資料ID
    public function getlatestid(){
        $this->db->select_Max('order_id','maxid');  

        $query = $this->db->get('order');
        return $query->row_array();
    }

    //查詢所有不同客戶
    public function getordercustomer(){
        $this->db->select('customer_id');
        $this->db->distinct('customer_id');
        $this->db->from('order');
        $this->db->where('order_status',1);
        $this->db->where('order_is_del',0);

        $query = $this->db->get();
        return $query->result_array();
    }

    //查詢所有不同客戶
    public function getorgcustomer($id){
        $this->db->select('customer_id');
        $this->db->distinct('customer_id');
        $this->db->from('order');
        $this->db->where('organization_id',$id);
        $this->db->where('order_status',1);
        $this->db->where('order_is_del',0);

        $query = $this->db->get();
        return $query->result_array();
    }

    //查詢最新資料
    public function getlatest(){
        $this->db->select_Max('order_number','order_number');
        $this->db->from('order');

        $query = $this->db->get();
        return $query->result_array();
    }

    //新增訂單
    public function add_order($order){

        $this->db->insert('order',$order);

    }

    //撈出完整資訊
    public function allorderinfo(){
        $this->db->select('*');
        $this->db->from('order_info');
        $this->db->join('order','order.order_id=order_info.order_id');
        $this->db->join('product','product.product_id=order_info.product_id');
        // $this->db->where('order_is_check',1);
        $this->db->where('order_is_del',0);

        $query = $this->db->get();
        return $query->result_array();
    }

    //修改訂單
    public function update_order($orderupdate,$id){
        $this->db->update('order',$orderupdate,array('order_id' => $id));
    }
}
