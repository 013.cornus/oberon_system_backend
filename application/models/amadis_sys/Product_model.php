<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_model{

    public function getidData($id){
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('product_id',$id);
        $this->db->where('product_is_del',0);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getList($keyword='',$limit=''){
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('product_is_del',0);
        if (isset($keyword['fullname']) && $keyword['fullname'] != '') {
            $this->db->like('product_name',$keyword['fullname']);
        }
        if (isset($keyword['status']) && $keyword['status'] != '') {
            $this->db->where('product_status',$keyword['status']);
        }

        if ($limit) {
            $this->db->limit($limit);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    //新增產品
    public function add_product($product){

        $this->db->insert('product',$product);

    }

    //修改產品
    public function update_product($product,$id){
        $this->db->update('product',$product,array('product_id' => $id));
    }

    //撈出最新商品id
    public function get_latest_id(){
        $this->db->select_max('product_id','maxid');
        $this->db->from('product');

        $query = $this->db->get();
        return $query->row_array();
    }
}
