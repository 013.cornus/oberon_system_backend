<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sidebarmenu_model extends CI_model{
    //查詢所有主選單資料
    public function getdata($keyword = ''){
        $this->db->select('*');
        $this->db->from('sidebar_main');
        $this->db->like('sidebar_main_name',$keyword);
        // $this->db->where('sidebar_main_status','1');
        $this->db->where('sidebar_main_is_del','0');
        $this->db->order_by("sidebar_main_sort", "ASC"); 

        $query = $this->db->get();
        return $query->result_array();
    }

    //查詢單個主選單資料
    public function getidData($id){
        $this->db->select('*');
        $this->db->from('sidebar_main');
        $this->db->where('sidebar_main_status','1');
        $this->db->where('sidebar_main_is_del','0');
        $this->db->where('sidebar_main_id',$id);

        $query = $this->db->get();
        return $query->row_array();
    }

    //查詢單個主選單資料
    public function getonedata($id){
        $this->db->select('*');
        $this->db->from('sidebar_main');
        $this->db->where('sidebar_main_status','1');
        $this->db->where('sidebar_main_is_del','0');
        $this->db->where('sidebar_main_id',$id);

        $query = $this->db->get();
        return $query->result_array();
    }

  //查詢單個主選單的子選單資料
    public function getsubdata($id,$keyword){
        $this->db->select('*');
        $this->db->from('sidebar_sub');
        $this->db->like('sidebar_sub_name',$keyword);
        $this->db->where('sidebar_main_id',$id);
        $this->db->where('sidebar_sub_is_del','0');
        // $this->db->where('sidebar_sub_status','1');
        $this->db->order_by("sidebar_sub_sort", "ASC"); 

        $query = $this->db->get();
        return $query->result_array();
    }

  //查詢單個子選單資料
    public function getsubidData($id){
        $this->db->select('*');
        $this->db->from('sidebar_sub');
        $this->db->where('sidebar_sub_status','1');
        $this->db->where('sidebar_sub_is_del','0');
        $this->db->where('sidebar_sub_id',$id);

        $query = $this->db->get();
        return $query->row_array();
    }

  //撈出所有主選單資料
    public function getsidebarmain(){
        $this->db->select('*');
        $this->db->from('sidebar_main');
        $this->db->where('sidebar_main_is_del','0');
        $this->db->where('sidebar_main_status','1');
        $this->db->order_by("sidebar_main_sort", "ASC"); 
        $query = $this->db->get();
        return $query->result_array();
    }

  //撈出所有副選單資料
    public function getsidebarsub(){
        $this->db->select('*');
        $this->db->from('sidebar_sub');
        $this->db->where('sidebar_sub_is_del','0');
        $this->db->where('sidebar_sub_status','1');
        $this->db->order_by("sidebar_sub_sort", "ASC");

        $query = $this->db->get();
        return $query->result_array();
    }

  //新增主選單
    public function add_sidebarmain($sidebarmain){

        $this->db->insert('sidebar_main',$sidebarmain);

    }

  //新增副選單
    public function add_sidebarsub($sidebarsub){

        $this->db->insert('sidebar_sub',$sidebarsub);

    }

  //修改主選單
    public function update_sidebarmain($sidebarmain,$id){
        $this->db->update('sidebar_main',$sidebarmain,array('sidebar_main_id' => $id));
    }

  //修改副選單
    public function update_sidebarsub($sidebarsub,$id){
        $this->db->update('sidebar_sub',$sidebarsub,array('sidebar_sub_id' => $id));
    }

  //刪除主選單
    public function delete_sidebar_main($id,$datetime,$user_id){
        $this->db->where('sidebar_main_id',$id);
        $this->db->set('sidebar_main_is_del','1'); 
        $this->db->set('sidebar_main_updated_date',$datetime);
        $this->db->set('sidebar_main_updated_user',$user_id);
        $this->db->update('sidebar_main');
    }

  //刪除副選單
    public function delete_sidebar_sub($id,$datetime,$user_id){
        $this->db->where('sidebar_sub_id',$id);
        $this->db->set('sidebar_sub_is_del','1'); 
        $this->db->set('sidebar_sub_updated_date',$datetime);
        $this->db->set('sidebar_sub_updated_user',$user_id);
        $this->db->update('sidebar_sub');
    }

    //查詢最新副選單id
    public function getlatestid(){
        $this->db->select_Max('sidebar_sub_id','maxid');  

        $query = $this->db->get('sidebar_sub');
        return $query->result_array();
    }


  //主選單下架
    public function invisible_sidebar_main($id,$datetime,$user_id){
        $this->db->where('sidebar_main_id',$id);
        $this->db->set('sidebar_main_status','0'); 
        $this->db->set('sidebar_main_updated_date',$datetime);
        $this->db->set('sidebar_main_updated_user',$user_id);
        $this->db->update('sidebar_main');
    }

  //主選單上架
    public function visible_sidebar_main($id,$datetime,$user_id){
        $this->db->where('sidebar_main_id',$id);
        $this->db->set('sidebar_main_status','1'); 
        $this->db->set('sidebar_main_updated_date',$datetime);
        $this->db->set('sidebar_main_updated_user',$user_id);
        $this->db->update('sidebar_main');
    }

  //副選單下架
    public function invisible_sidebar_sub($id,$datetime,$user_id){
        $this->db->where('sidebar_sub_id',$id);
        $this->db->set('sidebar_sub_status','0'); 
        $this->db->set('sidebar_sub_updated_date',$datetime);
        $this->db->set('sidebar_sub_updated_user',$user_id);
        $this->db->update('sidebar_sub');
    }

  //副選單上架
    public function visible_sidebar_sub($id,$datetime,$user_id){
        $this->db->where('sidebar_sub_id',$id);
        $this->db->set('sidebar_sub_status','1'); 
        $this->db->set('sidebar_sub_updated_date',$datetime);
        $this->db->set('sidebar_sub_updated_user',$user_id);
        $this->db->update('sidebar_sub');
    }
}
