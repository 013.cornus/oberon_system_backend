<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calculate_model extends CI_model{

    //  報表模板設置
	public function __construct(){
		parent::__construct();
        // $this->load->library('excel');
	}

	//	業績百萬單位轉換
	public function pv_million_convert($value){
		$unit = 1000000;
		$result = 0;

		if ( $value > $unit ) {
			$result = $value / $unit;
			$result = number_format($result,2,'.',',').'M';
		} else {
    		$result = number_format($value,0,'.',',');
		}

		return $result;
	}

	// 
	public function abc($value){
		$unit = 1000000;
		$result = 0;

		$result = $value / $unit;
		$result = round($result,2).'M';

		return $result;
	}

}