<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_key_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->from = 'register_key';
		$this->db_connect2 = $this->load->database('ks_db',true); 
	}

	public function getIdData($id){
		$this->db_connect2->select('*')
				 ->from($this->from)
				 ->where($this->from.'_status',1,false)
				 ->where($this->from.'_is_del',0,false)
				 ->where($this->from.'_id',$id,false);
		$result = $this->db_connect2->get();
		$result = $result->row_array();
		return $result;
	}

	public function getKeyData($key,$value){
		$this->db_connect2->select('*')
				 ->from($this->from)
				 ->where($key,$value);
		$result = $this->db_connect2->get();
		$result = $result->row_array();
		return $result;
	}

	public function getList($queryData,$limit = ''){
		$this->db_connect2->select('*')
				 ->from($this->from)
				 ->where($this->from.'_status',1,false)
				 ->where($this->from.'_is_del',0,false);

		if ( isset($queryData['type']) && $queryData['type'] ) {
		 	$this->db_connect2->where($this->from.'_type',$queryData['type']);
		} 
		if ( isset($queryData['id']) && $queryData['id'] ) {
		 	$this->db_connect2->where($this->from.'_id',$queryData['id']);
		}
		if ( isset($queryData['register_key_name']) && $queryData['register_key_name'] ) {
		 	$this->db_connect2->where($this->from.'_name', $queryData['register_key_name']);
		}

		if ( isset($queryData['group_type']) && $queryData['group_type'] ) {
		 	$this->db_connect2->where('group_type', $queryData['group_type']);
		}

        if ($limit) {
           $this->db_connect2->limit($limit[0],$limit[1]);
        }

		$result = $this->db_connect2->get();
		$result = $result->result_array();
		return $result;
	}

	//獲取所有帳號群
	public function getregister_keyType(){
		$this->db_connect2->select('register_key_type')
				 ->from($this->from)
				 ->where($this->from.'_status',1,false)
				 ->where($this->from.'_is_del',0,false);
		$this->db_connect2->distinct();
		$result = $this->db_connect2->get();
		$result = $result->result_array();
		return $result;
	}

	//新增帳號群
    public function add_register_key($register_key){

        $this->db_connect2->insert('register_key',$register_key);

    }

    //修改帳號群
    public function update_register_key($register_key,$id){
        $this->db_connect2->update('register_key',$register_key,array('register_key_id' => $id));
    }
	
	public function checkUsersPhone($phone){
		$this->db_connect2->from($this->from)
				 ->where($this->from.'_phone', $phone)
				 ->where($this->from.'_is_del', 0, false)
				 ->where($this->from.'_status', 1, false);

		$result = $this->db_connect2->count_all_results();
		return $result;
	}

}