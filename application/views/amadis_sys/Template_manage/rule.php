<script type="text/javascript">
	var rule1=/./;
	var rule2=/.@./;
	var rule3=/^[A-Z]{1}[0-9]{9}$/;
	var rule4=/^09\d{8}/;
	var rule5=/^0\d{1,2}-?(\d{6,8})(#\d{1,5}){0,1}/;
	var rule6=/[\u4e00-\u9fa5]{0,2}縣|市[\u4e00-\u9fa5]{2,4}鄉|鎮|市|區[\u4e00-\u9fa5]{2,4}路|街|大道.*\d*號./;
	//商品名稱
	$("#productname").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			productnameMsg.innerHTML="OK";
			productnameMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			productnameMsg.innerHTML="商品名稱不可為空";
			productnameMsg.style.color="red";
		}
	})
	//台單單價
	$("#tproductprice").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			tproductpriceMsg.innerHTML="OK";
			tproductpriceMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			tproductpriceMsg.innerHTML="台單單價不可為空";
			tproductpriceMsg.style.color="red";
		}
	})
	//台單PV
	$("#tpv").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			tpvMsg.innerHTML="OK";
			tpvMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			tpvMsg.innerHTML="台單PV不可為空";
			tpvMsg.style.color="red";
		}
	})

	//陸單單價
	$("#cproductprice").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			cproductpriceMsg.innerHTML="OK";
			cproductpriceMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			cproductpriceMsg.innerHTML="陸單單價不可為空";
			cproductpriceMsg.style.color="red";
		}
	})

	//陸單PV
	$("#cpv").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			cpvMsg.innerHTML="OK";
			cpvMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			cpvMsg.innerHTML="陸單PV不可為空";
			cpvMsg.style.color="red";
		}
	})
	function check() {
		if (rule1.test($("#productname").val()) && rule1.test($("#tproductprice").val()) && rule1.test($("#tpv").val()) && rule1.test($("#cproductprice").val()) && rule1.test($("#cpv").val())){
			document.getElementById("submitform").submit();	
		} else {
			alert("請輸入完整資訊");
			
			if(rule1.test($("#productname").val()) == false){
				$("#productname").css("border-color","red")
				productnameMsg.innerHTML="商品名稱不可為空";
				productnameMsg.style.color="red";
			}
			if(rule1.test($("#tproductprice").val()) == false){
				$("#tproductprice").css("border-color","red")
				tproductpriceMsg.innerHTML="台單單價不可為空";
				tproductpriceMsg.style.color="red";
			}
			if(rule1.test($("#tpv").val()) == false){
				$("#tpv").css("border-color","red")
				tpvMsg.innerHTML="台單PV不可為空";
				tpvMsg.style.color="red";
			}
			if(rule1.test($("#cproductprice").val()) == false){
				$("#cproductprice").css("border-color","red")
				cproductpriceMsg.innerHTML="陸單單價不可為空";
				cproductpriceMsg.style.color="red";
			}
			if(rule1.test($("#cpv").val()) == false){
				$("#cpv").css("border-color","red")
				cpvMsg.innerHTML="陸單PV不可為空";
				cpvMsg.style.color="red";
			}
		}
	}
</script>
</script>