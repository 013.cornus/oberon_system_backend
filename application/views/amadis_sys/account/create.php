		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">    
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home');?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url($this->router->fetch_class()); ?>"><?php echo $title; ?></a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?php echo $title_small; ?></a></li>
							<li class="pull-right no-text-shadow" style="display: none;">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-reorder"></i><?php echo $title_small; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<form action="<?php echo base_url('account/create');?>" class="form-horizontal"  method="post" id="createForm">
									<div class="alert alert-error hide">
										<button class="close" data-dismiss="alert"></button>
										錯誤！請完成所有必填項目
									</div>
									<div class="alert alert-success hide">
										<button class="close" data-dismiss="alert"></button>
										Your form validation is successful!
									</div>
									<div class="alert alert-info">
										<i class="icon-warning-sign"></i> 注意！新創建帳號的密碼一律是：<span style="color: red">000000</span>
									</div>

									<div class="control-group">
										<label class="control-label">用戶名稱<span class="required">*</span></label>
										<div class="controls chzn-controls">
											<select name="employee" id="form_2_chosen" class="span6 select2_category" data-with-diselect="1" name="options1" data-placeholder="請選擇用戶" tabindex="1">
												<option value=""></option>
												<?php foreach ($employee as $val) { ?>
												<option value="<?php echo $val["employee_id"];?>"><?php echo $val["employee_name"];?></option>
												<?php }?>
											</select>
										</div>
									</div>
									
									<div class="control-group">
										<label class="control-label">權限<span class="required">*</span></label>
										<div class="controls chzn-controls">
											<select  class="span6 select2_category" name="competence" data-placeholder="請選擇權限" tabindex="1">
												<option value=""></option>
												<?php foreach ($competence as $val) { ?>
												<option value="<?php echo $val["competence_id"];?>"><?php echo $val["competence_name"];?></option>
												<?php }?>
											</select>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label">用戶帳號<span class="required">*</span></label>
										<div class="controls">
											<input name="account" id="account" type="text" class="span6 m-wrap" "/><span id="errorMsg"></span>
										</div>
									</div>

									<div class="form-actions">
										<button type="submit" class="btn green" onclick="check()">
											新增 <div class="icon-save"></div>
										</button>
										<a href="<?php echo base_url('account');?>">
											<button type="button" class="btn">取消 <div class="icon-undo"></div></button>
										</a>
									</div>

								</form>
								<!-- END FORM-->
							</div>

						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END DASHBOARD STATS -->
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->    
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
<div class="footer">
	<?php require_once(dirname(__FILE__).'/../common/footer.php'); ?>
</div>
<!-- END FOOTER -->
<?php require_once(dirname(__FILE__).'/../common/script.php'); ?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script> -->
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url('public/globel/js/form-samples.js')?>" type="text/javascript"></script> 
<script src="<?php echo base_url('public/globel/js/select2.min.js')?>" type="text/javascript"></script> 
<!-- <script type="text/javascript" src="<?php echo base_url('public/globel/js/additional-methods.min.js');?>"></script>
<!-- <script type="text/javascript" src="<?php echo base_url('public/globel/js/chosen.jquery.min.js');?>"></script> -->
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL STYLES -->
<!-- <script type="text/javascript" src="http://static.runoob.com/assets/jquery-validation-1.14.0/lib/jquery.js"></script>
<script type="text/javascript" src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script> -->

<!-- <script type="text/javascript" src="<?php echo base_url('public/plugin/jquery-validation-1.14.0/jquery.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/plugin/jquery-validation-1.14.0/jquery.validate.min.js');?>"></script> -->
<!-- <script src="<?php echo base_url('public/plugin/main/account.js');?>" type="text/javascript"></script> -->
<!-- END PAGE LEVEL STYLES -->    
<script src="<?php echo base_url('public/globel/js/jquery.validate.min.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/plugin/main/user-validation.js')?>" type="text/javascript"></script>

<script>
	jQuery(document).ready(function() {   
		// initiate layout and plugins
		FormValidation.init();
	});
</script>
</body>
<!-- END BODY -->
</html>