<script type="text/javascript">
	var rule1=/./;
	var rule2=/.@./;
	var rule3=/^[A-Z]{1}[0-9]{9}$/;
	var rule4=/^09\d{8}/;
	var rule5=/^0\d{1,2}-?(\d{6,8})(#\d{1,5}){0,1}/;
	var rule6=/[\u4e00-\u9fa5]{0,2}縣|市[\u4e00-\u9fa5]{2,4}鄉|鎮|市|區[\u4e00-\u9fa5]{2,4}路|街|大道.*\d*號./;
	//用戶名稱
	$("#accountname").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			accountnameMsg.innerHTML="OK";
			accountnameMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			accountnameMsg.innerHTML="用戶名稱不可為空";
			accountnameMsg.style.color="red";
		}
	})
	//用戶帳號
	$("#account").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			accountMsg.innerHTML="OK";
			accountMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			accountMsg.innerHTML="用戶帳號不可為空";
			accountMsg.style.color="red";
		}
	})
	//用戶密碼
	$("#password").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			passwordMsg.innerHTML="OK";
			passwordMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			passwordMsg.innerHTML="用戶密碼不可為空";
			passwordMsg.style.color="red";
		}
	})
	//再次確認密碼
	$("#passwordcheck").blur(function(){
		if(rule1.test($(this).val()) && $(this).val()==$("#password").val()){	
			$(this).css("border-color","green")
			passwordcheckMsg.innerHTML="OK";
			passwordcheckMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			passwordcheckMsg.innerHTML="確認密碼為空或與用戶密碼不同";
			passwordcheckMsg.style.color="red";
		}
	})
	function check() {
		if (rule1.test($("#accountname").val()) && rule1.test($("#account").val()) && rule1.test($("#password").val()) && $("#passwordcheck").val()==$("#password").val()){
			document.getElementById("submitform").submit();	
		} else {
			alert("請輸入完整資訊");
		}
	}
</script>
</script>