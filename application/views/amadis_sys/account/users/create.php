		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">    
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home');?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url($this->router->fetch_class()); ?>"><?php echo $title; ?></a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?php echo $title_small; ?></a></li>
							<li class="pull-right no-text-shadow" style="display: none;">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-reorder"></i><?php echo $title_small; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<form action="<?php echo base_url('users/create');?>" class="form-horizontal"  method="post" id="createForm">
									<div class="alert alert-error hide">
										<button class="close" data-dismiss="alert"></button>
										錯誤！請完成所有必填項目
									</div>
									<div class="alert alert-success hide">
										<button class="close" data-dismiss="alert"></button>
										Your form validation is successful!
									</div>
									<div class="alert alert-info">
										<i class="icon-warning-sign"></i> 注意！新創建帳號的密碼一律是：<span style="color: red">000000</span>
									</div>

									<!-- <div class="control-group" id='account_type' style="display: none;">
										<label class="control-label">帳號種類<span class="required">*</span></label>
										<div class="controls chzn-controls">
											<select name="employee" id="type" class="span6 select2_category " data-with-diselect="1" name="type" data-placeholder="請選擇用戶" tabindex="1">
												<option value=""></option>
												<option value="1">總部</option>
												<option value="2">業務</option>
												<option value="3">客戶</option>
												<option value="4">業務+客戶</option>
											</select>
											<span id="errorMsg"></span>
										</div>
									</div> -->

									<div class="control-group">
										<label class="control-label">權限<span class="required">*</span></label>
										<div class="controls chzn-controls">
											<select name="competence" id="form_3_chosen" class="span6 select2_category" data-with-diselect="1" name="options1" data-placeholder="請選擇權限" tabindex="1">
												<option value=""></option>
												<?php foreach ($competence as $val) { ?>
													<?php if($this->session->userdata('competence_id') == 3){?>
														<?php if($val['competence_id'] >= 3){?>
															<option value="<?php echo $val["competence_id"];?>"></option>
														<?php }?>
													<?php }else{?>
														<option value="<?php echo $val["competence_id"];?>"><?php echo $val["competence_name"];?></option>
													<?php }?>
												<?php }?>
											</select>
											<span id="errorMsg"></span>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label">群組類別<span class="required">*</span></label>
										<div class="controls chzn-controls">
											<select name="group_type" id="form_3_chosen" class="span6 select2_category" data-with-diselect="1" name="options1" data-placeholder="請選擇權限" tabindex="1">
												<?php foreach($groupList as $key => $row){?>
													<option value="<?php echo $row['group_info_type']?>"><?php echo $row['group_info_name']?></option>
												<?php }?>
											</select>
											<span id="errorMsg"></span>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label">用戶名稱<span class="required">*</span></label>
										<div class="controls chzn-controls">
											<input name="users_name" id="users_name" type="text" class="span6 m-wrap" "/>
											<span id="errorMsg"></span>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label">用戶帳號<span class="required">*</span></label>
										<div class="controls">
											<input name="account" id="account" type="text" class="span6 m-wrap" "/>
											<span id="errorMsg"></span>
										</div>
									</div>

									<div class="form-actions">
										<button type="submit" class="btn green">
											新增 <div class="icon-save"></div>
										</button>
										<a href="<?php echo base_url('users');?>">
											<button type="button" class="btn">取消 <div class="icon-undo"></div></button>
										</a>
									</div>

								</form>
								<!-- END FORM-->
							</div>

						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END DASHBOARD STATS -->
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->    
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
<div class="footer">
	<?php echo $footer; ?>
</div>
<!-- END FOOTER -->
<?php echo $script; ?>

<script src="<?php echo base_url('public/globel/plugin/main/user-validation.js')?>" type="text/javascript"></script>
<script type="text/javascript">
	$(':radio').click(function(){
		if($('input[type=radio]:checked').val() == 2){
			// $("#account_type").css('display','none');
			// document.getElementById("type").options[3].disabled = false;
			// document.getElementById("type").options[3].selected = true;
			document.getElementById("form_3_chosen").options.length=0;
			document.getElementById("form_3_chosen").options.add(new Option('客戶',5));
			$("#form_3_chosen").val(5).trigger('change');
			document.getElementById("form_1_chosen").options.length=0;
			document.getElementById("form_1_chosen").options.add(new Option('不需填寫',0));
			$("#form_1_chosen").val(0).trigger('change');
					//撈出客戶
					$.ajax({
						url:"<?php echo base_url('users/getcustomer');?>",				
						method:"POST",
						dataType:"json",				
						success:function(res){
							document.getElementById("form_2_chosen").options.length=0;
							var NumOfData = res.length;
							for (var i = 0; i < NumOfData; i++) {
								document.getElementById("form_2_chosen").options.add(new Option(res[i]["customer_name"],res[i]["customer_id"]));    
							}
							$("#form_2_chosen							").val(1).trigger('change');  
						},
						error : function(xhr, ajaxOptions, thrownError){
							$("portlet-body").append(xhr.status);
							$("body").append(xhr.responseText);

							alert(thrownError);
						}
					})
				}
				else{
					// $("#account_type").css('display', '');
					// document.getElementById("type").options[3].disabled = true;
					//撈出業務承攬人
					$.ajax({
						url:"<?php echo base_url('users/getemployee');?>",				
						method:"POST",
						dataType:"json",
						async: false,				
						success:function(res){
							document.getElementById("form_2_chosen").options.length=0;
							var NumOfData = res.length;
							for (var i = 0; i < NumOfData; i++) {
								document.getElementById("form_2_chosen").options.add(new Option(res[i]["employee_name"],res[i]["employee_id"]));
								$("#form_2_chosen").val(1).trigger('change');    
							}  
						},
						error : function(xhr, ajaxOptions, thrownError){
							$("portlet-body").append(xhr.status);
							$("body").append(xhr.responseText);

							alert(thrownError);
						}
					})

					//撈出營業處
					$.ajax({
						url:"<?php echo base_url('users/getorganization');?>",				
						method:"POST",
						dataType:"json",
						async: false,				
						success:function(res){
							document.getElementById("form_1_chosen").options.length=0;
							var NumOfData = res.length;
							for (var i = 0; i < NumOfData; i++) {
								document.getElementById("form_1_chosen").options.add(new Option(res[i]["organization_name"],res[i]["organization_id"]));    
							}  
							$("#form_1_chosen").val(1).trigger('change');
						},
						error : function(xhr, ajaxOptions, thrownError){
							$("portlet-body").append(xhr.status);
							$("body").append(xhr.responseText);

							alert(thrownError);
						}
					})

					//撈出權限
					$.ajax({
						url:"<?php echo base_url('users/getcompetence');?>",				
						method:"POST",
						dataType:"json",				
						success:function(res){
							document.getElementById("form_3_chosen").options.length=0;
							var NumOfData = res.length;
							for (var i = 0; i < NumOfData; i++) {
								if(res[i]["competence_id"] != 5){
									document.getElementById("form_3_chosen").options.add(new Option(res[i]["competence_name"],res[i]["competence_id"]));
								}    
							}
							$("#form_3_chosen").val(4).trigger('change');  
						},
						error : function(xhr, ajaxOptions, thrownError){
							$("portlet-body").append(xhr.status);
							$("body").append(xhr.responseText);

							alert(thrownError);
						}
					})
				}
			})
		</script>

		<script>
			jQuery(document).ready(function() {   
		// initiate layout and plugins
		FormValidation.init();
	});
</script>
</body>
<!-- END BODY -->
</html>