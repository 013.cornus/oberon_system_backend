<div class="footer">

	<div class="container">

		<div class="footer-inner">
			<?php echo FOOTER_YEAR.FOOTER_COPY.FOOTER_COMPANY.FOOTER_COPY_TEXT; ?>
			<!-- <?php echo date('Y'); ?> &copy; Metronic by keenthemes.Collect from <a href="#" title="" target="_blank">Metronic</a> -->

		</div>

		<div class="footer-tools">

			<span class="go-top">

				<i class="icon-angle-up"></i>

			</span>

		</div>

	</div>

</div>