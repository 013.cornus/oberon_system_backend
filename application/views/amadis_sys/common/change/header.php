<div class="header navbar navbar-inverse navbar-fixed-top">
	<!-- BEGIN TOP NAVIGATION BAR -->
	<div class="navbar-inner">
		<div class="container">
			<!-- BEGIN LOGO -->
			<a class="brand" href="#">
				<div style="width: 170px;margin-top: -23px;">
					<img src="<?php echo base_url('public/globel/image/logo/system-logo-h.png');?>" alt="logo"/>
				</div>
			</a>
			<!-- END LOGO -->
			<!-- BEGIN HORIZANTAL MENU -->
			<div class="navbar hor-menu hidden-phone hidden-tablet">
				<div class="navbar-inner hide">
					<ul class="nav">
						<li class="visible-phone visible-tablet">
							<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
							<form class="sidebar-search">
								<div class="input-box">
									<a href="javascript:;" class="remove"></a>
									<input type="text" placeholder="Search..." />            
									<input type="button" class="submit" value=" " />
								</div>
							</form>
							<!-- END RESPONSIVE QUICK SEARCH FORM -->
						</li>
						</ul>
					</div>
				</div>
				<!-- END HORIZANTAL MENU -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse" style="display: none;">
					<img src="<?php echo base_url('public/globel/image/menu-toggler.png');?>" alt="" />
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->            
				<!-- BEGIN TOP NAVIGATION MENU -->              
				<ul class="nav pull-right">
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<li class="dropdown user">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<!-- <img alt="" src="<?php echo base_url('public/globel/image/avatar1_small.jpg');?>" /> -->
							<div style="padding:4px; ">
								<i class="icon-user"></i>
								<!-- <i class="fa fa-camera-retro" style="font-size:24px"></i> -->
								<span class="username"><?php echo $this->session->userdata('users_name');?></span>
								<i class="icon-angle-down"></i>
							</div>
						</a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url('home/logout'); ?>"><i class="icon-key"></i> 登出</a></li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
				<!-- END TOP NAVIGATION MENU --> 
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>