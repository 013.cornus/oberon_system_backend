		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">    
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home');?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url($this->router->fetch_class()); ?>"><?php echo $title; ?></a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?php echo $title_small; ?></a></li>
							<li class="pull-right no-text-shadow" style="display: none;">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-reorder"></i><?php echo $title_small; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<?php foreach($maxid as $r){$id = $r['maxid'] + 1;};?>
								<form action="<?php echo base_url('competence/create?id=').$id;?>" class="form-horizontal"  method="post" name="submitform" id="submitform">
									<div class="alert alert-error hide">
										<button class="close" data-dismiss="alert"></button>
										You have some form errors. Please check below.
									</div>
									<div class="alert alert-success hide">
										<button class="close" data-dismiss="alert"></button>
										Your form validation is successful!
									</div>
									<div class="control-group">
										<label class="control-label">權限名稱<span class="required">*</span></label>
										<div class="controls">
											<input name="competencename" id="competencename" type="text" class="span6 m-wrap" "/><span id="competencenameMsg"></span><p id="test"></p>
										</div>
									</div>
									<?php $a=1;?>
									<?php foreach($mainbar as $m){?>
										<div class="accordion-group">

											<div class="accordion-heading">

												<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="<?php echo "#tag".$m['sidebar_main_id'];?>">

													<i class="icon-th"></i>

													<?php echo $m['sidebar_main_name']?>

												</a>

											</div>

											<div id="<?php echo "tag".$m['sidebar_main_id'];?>" class="accordion-body collapse">

												<?php foreach($subbar as $r){ if($m['sidebar_main_id'] == $r['sidebar_main_id']){?>
													<div class="accordion-inner">
														<div class="control-group">

															<label class="control-label"><?php echo $r['sidebar_sub_name'];?></label>

															<div class="controls">
																<label class="checkbox">

																	<input type="checkbox" name="<?php echo "allright".$a?>" id="<?php echo "allright".$a?>" onclick="<?php echo "allrightcheck".$a?>()" value="0"/> 全選

																</label>

																<label class="checkbox">

																	<input type="checkbox" name="<?php echo "view".$a?>" id="<?php echo "view".$a?>" onclick="<?php echo "viewcheck".$a?>()" value="1"/> 檢視

																</label>

																<label class="checkbox">

																	<input type="checkbox" name="<?php echo "insert".$a?>" id="<?php echo "insert".$a?>" onclick="<?php echo "insertcheck".$a?>()" value="1"/> 新增

																</label>

																<label class="checkbox">

																	<input type="checkbox" name="<?php echo "update".$a?>" id="<?php echo "update".$a?>" onclick="<?php echo "updatecheck".$a?>()" value="1"/> 修改

																</label>

																<label class="checkbox">

																	<input type="checkbox" name="<?php echo "enable".$a?>" id="<?php echo "enable".$a?>" onclick="<?php echo "enablecheck".$a?>()" value="1"/> 上下架

																</label>

																<label class="checkbox">

																	<input type="checkbox" name="<?php echo "delete".$a?>" id="<?php echo "delete".$a?>" onclick="<?php echo "deletecheck".$a?>()" value="1"/> 刪除

																</label>

																<label class="checkbox">

																	<input type="checkbox" name="<?php echo "export".$a?>" id="<?php echo "export".$a?>" onclick="<?php echo "exportcheck".$a?>()" value="1"/> 匯出

																</label>
																<input style="display: none" name="<?php echo "mainid".$a;?>" id="<?php echo "mainid".$a;?>" value="<?php echo $r['sidebar_main_id'];?>"/>

															</div>
														</div>
													</div>
													<?php $a=$a+1;}}?>

												</div>
											</div>
										<?php }?>							
										<div class="form-actions">
											<button type="submit" class="btn green">
												新增 <div class="icon-save"></div>
											</button>
											<a href="<?php echo base_url('competence');?>">
												<button type="button" class="btn">取消 <div class="icon-undo"></div></button>
											</a>
										</div>

									</form>
									<!-- END FORM-->
								</div>

							</div>
							<!-- END VALIDATION STATES-->
						</div>
					</div>
					<!-- END DASHBOARD STATS -->
					<div class="clearfix"></div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php require_once(dirname(__FILE__).'/../common/footer.php'); ?>
	</div>
	<!-- END FOOTER -->
	<?php require_once(dirname(__FILE__).'/../common/script.php'); ?>

	<!-- BEGIN RULE -->
	<?php require_once(dirname(__FILE__).'/../account/rule.php'); ?>
	<!-- END RULE -->
	</body>
	<!-- END BODY -->
	</html>