<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home');?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url($this->router->fetch_class()); ?>"><?php echo $title; ?></a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?php echo $title_small; ?></a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box yellow">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i><?php echo $title_small; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<form action="<?php echo base_url('customers/update');?>" class="form-horizontal"  method="post" name="submitform" id="submitform">
									<div class="alert alert-error hide">
										<button class="close" data-dismiss="alert"></button>
										You have some form errors. Please check below.
									</div>
									<div class="alert alert-success hide">
										<button class="close" data-dismiss="alert"></button>
										Your form validation is successful!
									</div>
									<div class="tabbable tabbable-custom">

										<ul class="nav nav-tabs">

											<li class="active"><a href="#tab_1_1" data-toggle="tab">客戶基本資料</a></li>

											<li><a href="#tab_1_2" data-toggle="tab">客戶帳戶資料</a></li>

											<li><a href="#tab_1_3" data-toggle="tab">營業處資料</a></li>

										</ul>

										<div class="tab-content">

											<div class="tab-pane active" id="tab_1_1">

												<div data-always-visible="1" data-rail-visible="0">
													<div class="control-group">
														<label class="control-label">姓名</label>
														<div class="controls">
															<input type="text" id="name" name="name"  data-required="1" class="span6 m-wrap" value="<?php echo $result['customer_name'];?>" readonly/>
															<span id="namesMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">經辦人</label>
														<div class="controls">
															<input type="text" id="name" name="name"  data-required="1" class="span6 m-wrap" value="<?php echo $employeename;?>" readonly/>
															<span id="namesMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">性別</label>
														<div class="controls">
															<input name="line" id="line" type="text" class="span1 m-wrap" value="<?php if($result['customer_sex'] == 1){echo "男";} else{echo "女";}?>" readonly/>
															<span id="sex"></span>
														</div>
													</div>
													<div class="control-group">

														<label class="control-label">出生年月日</label>

														<div class="controls">

															<input name="line" id="line" type="text" class="span3 m-wrap" value="<?php echo $result['customer_birthday'];?>" readonly/></span><span id="birthdayMsg"></span>

														</div>
													</div>
													<div class="control-group">
														<label class="control-label">身分證字號</label>
														<div class="controls">
															<input name="idcard" id="idcard" type="text" class="span6 m-wrap" maxlength="10" value="<?php echo $result['customer_id_card'];?>" readonly/><span id="idMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">戶籍地址</label>
														<div class="controls">
															<input name="email" id="email" type="text" class="span2 m-wrap" value="<?php foreach($address as $a){if($a["address_city"].$a["address_area"] == mb_substr($result['customer_permanent_address'],5,6,'utf8')){echo $a['address_city'].$a['address_area'];}}?>" readonly/>
															<input name="paddressdetail" id="paddressdetail" type="text" class="span6 m-wrap" style="width: 30%;" value="<?php echo $result['customer_permanent_address'];?>" readonly/>
															<span id="paddressMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">通訊地址</label>
														<div class="controls">
															<input name="email" id="email" type="text" class="span2 m-wrap" value="<?php foreach($address as $a){if($a["address_city"].$a["address_area"] == mb_substr($result['customer_residential_address'],5,6,'utf8')){echo $a['address_city'].$a['address_area'];}}?>" readonly/>
															<input name="raddressdetail" id="raddressdetail" type="text" class="span6 m-wrap" style="width: 30%;" value="<?php echo $result['customer_residential_address'];?>" readonly/>
															<span id="caddressMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">電子信箱</label>
														<div class="controls">
															<input name="email" id="email" type="text" class="span6 m-wrap" value="<?php echo $result['customer_email'];?>" readonly/>
															<span id="emailMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">Line ID</label>
														<div class="controls">
															<input name="line" id="line" type="text" class="span6 m-wrap" value="<?php echo $result['customer_line_id'];?>" readonly/>
															<span id="lineMsg"></span>
														</div>
													</div>
												</div>

											</div>

											<div class="tab-pane" id="tab_1_2">

												<div data-always-visible="1" data-rail-visible1="1">
													<div class="control-group">
														<label class="control-label">公司電話</label>
														<div class="controls">
															<input name="companytel" id="companytel" type="text" class="span5 m-wrap" value="<?php echo $result['customer_company_tel'];?>" readonly/>
															<span id="ctelephoneMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">住宅電話</label>
														<div class="controls">
															<input name="residentialtel" id="residentialtel" type="text" class="span5 m-wrap" value="<?php echo $result['customer_residential_tel'];?>" readonly/>
															<span id="rtelephoneMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">手機</label>
														<div class="controls">
															<input name="cellphone" id="cellphone" type="text" class="span5 m-wrap" maxlength="10" value="<?php echo $result['customer_phone'];?>" readonly/>
															<span id="cellphoneMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">匯款銀行</label>
														<div class="controls">
															<input name="bank" id="bank" type="text" class="span6 m-wrap" value="<?php echo $bankname;?>" readonly/>
															<span id="cellphoneMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">分行名稱</label>
														<div class="controls">
															<input name="branch" id="branch" type="text" class="span6 m-wrap" value="<?php echo $branchname;?>" readonly/>
															<span id="cellphoneMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">匯款帳號</label>
														<div class="controls">
															<input name="account" id="account" type="text" class="span6 m-wrap" value="<?php echo $result['customer_account'];?>" readonly/>
															<span id="cellphoneMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">戶名</label>
														<div class="controls">
															<input name="accountname" id="accountname" type="text" class="span6 m-wrap" value="<?php echo $result['customer_account_name'];?>" readonly/>
															<span id="cellphoneMsg"></span>
														</div>
													</div>	
													<div class="control-group">
														<label class="control-label">備註</label>
														<div class="controls">
															<input name="note" id="note" type="text" class="span6 m-wrap" value="<?php echo $result['customer_note'];?>" readonly/>
															<span id="noteMsg"></span>
														</div>
													</div>
												</div>

											</div>

											<div class="tab-pane" id="tab_1_3">

												<div data-always-visible="1" data-rail-visible1="1">
													<div class="control-group">
														<label class="control-label">營業處</label>
														<div class="controls">
															<input name="bank" id="bank" type="text" class="span6 m-wrap" value="<?php echo $orgname;?>" readonly/>
															<span id="cellphoneMsg"></span>
														</div>
													</div>
												</div>

											</div>

										</div>

									</div>

									<div class="form-actions">
										<a href="<?php echo base_url('customers');?>">
											<button type="button" class="btn">取消 <div class="icon-undo"></div></button>
										</a>
									</div>
								</form>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>

				<!-- END DASHBOARD STATS -->
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->    
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
	<?php echo $footer; ?>
</div>
<!-- END FOOTER -->
<?php echo $script; ?>
<!-- BEGIN PAGE LEVEL PLUGINS -->
	<!-- BEGIN RULE -->
	<!-- END RULE -->
</body>
<!-- END BODY -->
</html>