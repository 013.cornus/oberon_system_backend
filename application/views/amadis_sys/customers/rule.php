<script type="text/javascript">
	var rule1=/./;
	var rule2=/.@./;
	var rule3=/^[A-Z]{1}[0-9]{9}$/;
	var rule4=/^09\d{8}/;
	var rule5=/^0\d{1,2}-?(\d{6,8})(#\d{1,5}){0,1}/;
	var rule6=/[\u4e00-\u9fa5]{0,2}縣|市[\u4e00-\u9fa5]{2,4}鄉|鎮|市|區[\u4e00-\u9fa5]{2,4}路|街|大道.*\d*號./;
	//姓名
	$("#name").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			namesMsg.innerHTML="OK";
			namesMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			namesMsg.innerHTML="請輸入客戶姓名";
			namesMsg.style.color="red";
		}
	})
	// //出生年月日
	// $("#birthday").blur(function(){
	// 	if(rule1.test($(this).val())){	
	// 		$(this).css("border-color","green")
	// 		birthdayMsg.innerHTML="OK";
	// 		birthdayMsg.style.color="green";
	// 	}else{
	// 		$(this).css("border-color","red")
	// 		birthdayMsg.innerHTML="您的輸入不符合規則";
	// 		birthdayMsg.style.color="red";
	// 	}
	// })
	//身分證字號
	$("#idcard").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			idMsg.innerHTML="OK";
			idMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			idMsg.innerHTML="請輸入身分證字號";
			idMsg.style.color="red";

		}
	})
	//戶籍地址
	$("#paddressdetail").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			paddressMsg.innerHTML="OK";
			paddressMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			paddressMsg.innerHTML="請輸入戶籍地址";
			paddressMsg.style.color="red";
		}
	})
	//通訊地址
	$("#raddressdetail").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			raddressMsg.innerHTML="OK";
			raddressMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			raddressMsg.innerHTML="請輸入通訊地址";
			raddressMsg.style.color="red";
		}
	})

	//手機
	$("#cellphone").blur(function(){
		if(rule1.test($(this).val()) && rule1.test($("#cellarea").val())){	
			$(this).css("border-color","green")
			cellphoneMsg.innerHTML="OK";
			cellphoneMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			$("#cellarea").css("border-color","red")
			cellphoneMsg.innerHTML="請輸入手機";
			cellphoneMsg.style.color="red";
		}
	})

	//匯款銀行
	$("#bank").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			bankMsg.innerHTML="OK";
			bankMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			bankMsg.innerHTML="請輸入匯款銀行";
			bankMsg.style.color="red";
		}
	})

	//分行代號
	$("#branch").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			branchMsg.innerHTML="OK";
			branchMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			branchMsg.innerHTML="請輸入分行";
			branchMsg.style.color="red";
		}
	})

	//匯款帳號
	$("#account").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			accountMsg.innerHTML="OK";
			accountMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			accountMsg.innerHTML="請輸入匯款帳號";
			accountMsg.style.color="red";
		}
	})

	//戶名
	$("#accountname").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			accountnameMsg.innerHTML="OK";
			accountnameMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			accountnameMsg.innerHTML="請輸入戶名";
			accountnameMsg.style.color="red";
		}
	})
	function check() {
		if (rule1.test($("#name").val()) && rule1.test($("#idcard").val()) && rule1.test($("#paddressdetail").val()) && rule1.test($("#raddressdetail").val()) && rule1.test($("#cellphone").val()) && rule1.test($("#bank").val()) && rule1.test($("#branch").val()) && rule1.test($("#account").val()) && rule1.test($("#accountname").val())){
			document.getElementById("submitform").submit();	
		} else {
			alert("請輸入完整資訊");
			if(rule1.test($("#name").val()) == false){
				$("#name").css("border-color","red")
				namesMsg.innerHTML="請輸入客戶姓名";
				namesMsg.style.color="red";
			}

			if(rule1.test($("#idcard").val()) == false){
				$("#idcard").css("border-color","red")
				idMsg.innerHTML="請輸入身分證字號";
				idMsg.style.color="red";
			}

			if(rule1.test($("#paddressdetail").val()) == false){
				$("#paddressdetail").css("border-color","red")
				paddressMsg.innerHTML="請輸入戶籍地址";
				paddressMsg.style.color="red";
			}

			if(rule1.test($("#raddressdetail").val()) == false){
				$("#raddressdetail").css("border-color","red")
				raddressMsg.innerHTML="請輸入通訊地址";
				raddressMsg.style.color="red";
			}

			if(rule1.test($("#cellphone").val()) == false || rule1.test($("#cellarea").val())){
				$("#cellphone").css("border-color","red")
				$("#cellarea").css("border-color","red")
				cellphoneMsg.innerHTML="請輸入手機";
				cellphoneMsg.style.color="red";
			}

			if(rule1.test($("#bank").val()) == false){
				$("#bank").css("border-color","red")
				bankMsg.innerHTML="請輸入匯款銀行";
				bankMsg.style.color="red";
			}

			if(rule1.test($("#branch").val()) == false){
				$("#branch").css("border-color","red")
				branchMsg.innerHTML="請輸入分行";
				branchMsg.style.color="red";
			}

			if(rule1.test($("#account").val()) == false){
				$("#account").css("border-color","red")
				accountMsg.innerHTML="請輸入匯款帳號";
				accountMsg.style.color="red";
			}

			if(rule1.test($("#accountname").val()) == false){
				$("#accountname").css("border-color","red")
				accountnameMsg.innerHTML="請輸入戶名";
				accountnameMsg.style.color="red";
			}
		}
	}
</script>