		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home');?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url($this->router->fetch_class()); ?>"><?php echo $title; ?></a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?php echo $title_small; ?></a></li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-user"></i><?php echo $title_small; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<form action="<?php echo base_url('customers/update');?>" class="form-horizontal"  method="post" name="customerform" id="customerform">
									<div class="alert alert-error hide">
										<button class="close" data-dismiss="alert"></button>
										<i class="icon-warning-sign"></i>	
										錯誤！請完成所有必填項目(客戶帳戶資料亦有必填欄位)
									</div>
									<div class="alert alert-success hide">
										<button class="close" data-dismiss="alert"></button>
										Your form validation is successful!
									</div>
																						<div class="control-group">
														<label class="control-label">姓名<span class="required">*</span></label>
														<div class="controls">
															<input type="text" id="name" name="name"  data-required="1" class="span6 m-wrap" value="<?php echo $result['customer_name'];?>"/>
															<span id="namesMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">手機</label>
														<div class="controls">
															<input name="cellphone" id="cellphone" type="text" class="span5 m-wrap" value="<?php echo $result['customer_phone'];?>"/>
															<span id="cellphoneMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">推薦人<span class="required">*</span></label>
														<div class="controls">
															<select class="span6 select2_category" name="employeename" id="employeename" data-placeholder="請選擇">
																<option value=""></option>
																<?php foreach($employee as $d){?>
																	<option value="<?php echo $d["employee_id"];?>" <?php if($result['employee_id'] == $d['employee_id']){echo 'selected';}?>><?php echo $d["employee_name"];?></option>
																<?php }?>
															</select>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">性別<span class="required">*</span></label>
														<div class="controls">
															<label class="radio">
																<input type="radio" name="optionsRadios1" value="1" <?php if($result['customer_sex'] == 1){echo "checked";}?>/>男
															</label>
															<label class="radio">
																<input type="radio" name="optionsRadios1" value="2" <?php if($result['customer_sex'] == 2){echo "checked";} ?>/>女
															</label>  
															<span id="sex"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">身分證字號</label>
														<div class="controls">
															<input name="idcard" id="idcard" type="text" class="span6 m-wrap" maxlength="10" value="<?php echo $result['customer_id_card'];?>"/><span id="idMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">地址</label>
														<div class="controls">
															<select class="span3 select2_category" name="residentialaddress" data-placeholder="請選擇"  style="width: auto;">
																<option value=""></option>
																<?php foreach($address as $r){?>
																	<option value="<?php echo $r["address_id"];?>" <?php if($result['address_id_2'] == $r["address_id"]){echo 'selected';}?> ><?php echo $r["address_zip"].' '.$r["address_city"].$r["address_area"];?></option>
																<?php }?>
															</select>
															<input name="raddressdetail" id="raddressdetail" type="text" class="span6 m-wrap" value="<?php echo $result['customer_residential_address'];?>"/>
															<span id="caddressMsg"></span>
														</div>
													</div>
									<input type="text" name="id" value="<?php echo $upid;?>" style='display: none;'>
									<div class="form-actions">
										<button type="submit" class="btn green">
											儲存 <div class="icon-save"></div>
										</button>
										<a href="<?php echo base_url('customers');?>">
											<button type="button" class="btn">取消 <div class="icon-undo"></div></button>
										</a>
									</div>
								</form>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>

				<!-- END DASHBOARD STATS -->
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->    
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
	<?php echo $footer; ?>
</div>
<!-- END FOOTER -->

</body>
<!-- END BODY -->
<?php echo $script; ?>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url('public/globel/js/form-samples.js')?>" type="text/javascript"></script> 
<script src="<?php echo base_url('public/globel/js/select2.min.js')?>" type="text/javascript"></script> 
<script src="<?php echo base_url('public/globel/js/jquery.validate.min.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/plugin/main/customer-validation.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/globel/js/bootstrap-datepicker.js')?>" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script type="text/javascript">
	var selectedid = 0;
	if($('input[name=bankoption]:checked').val() == 1){
		$.ajax({
			url:"<?php echo base_url('bank/getbank');?>",				
			method:"POST",
			dataType:"json",
			async: false,				
			success:function(res){
				document.getElementById("bank").options.length=0;
				var NumOfData = res.length;
				document.getElementById("bank").options.add(new Option("請選擇",0));
				for (var i = 0; i < NumOfData; i++) {
					if(<?php echo $result['customer_bank'];?> == res[i]["bank_id"]){
						document.getElementById("bank").options.add(new Option(res[i]["bank_name"],res[i]["bank_id"],true,true));
						selectedid = res[i]["bank_id"];
					}
					else{
						document.getElementById("bank").options.add(new Option(res[i]["bank_name"],res[i]["bank_id"]));
					}
				}  
			},
			error : function(xhr, ajaxOptions, thrownError){
				$("portlet-body").append(xhr.status);
				$("body").append(xhr.responseText );

				alert(thrownError);
			}
		})

		var mainselected = selectedid;

		$.ajax({
			url:"<?php echo base_url('bank/combo');?>",				
			method:"POST",
			dataType:"json",
			async: false,
			data:{
				mainselected:mainselected
			},					
			success:function(res){
				document.getElementById("branch").options.length=0;
				document.getElementById("branch").options.add(new Option("請選擇",0));
				var NumOfData = res.length;
				for (var i = 0; i < NumOfData; i++) {
					if(<?php echo $result['customer_bank_branch'];?> == res[i]["bank_branch_id"]){
						document.getElementById("branch").options.add(new Option(res[i]["bank_branch_name"],res[i]["bank_branch_id"],true,true));
					}
					else{
						document.getElementById("branch").options.add(new Option(res[i]["bank_branch_name"],res[i]["bank_branch_id"]));
					}    
				}  
			},
			error : function(xhr, ajaxOptions, thrownError){
				$("portlet-body").append(xhr.status);
				$("body").append(xhr.responseText );

				alert(thrownError);
			}
		})
	}

	else{
		document.getElementById("bank").options.length=0;
		document.getElementById("bank").options.add(new Option("請選擇",0));
		document.getElementById("bank").options.add(new Option('中華郵政',700,true,true));

		mainselected = 700;
		$.ajax({
			url:"<?php echo base_url('bank/combo');?>",				
			method:"POST",
			dataType:"json",
			async: false,
			data:{
				mainselected:mainselected
			},					
			success:function(res){
				document.getElementById("branch").options.length=0;
				var NumOfData = res.length;
				for (var i = 0; i < NumOfData; i++) {
					if(<?php echo $result['customer_bank_branch'];?> == res[i]["post_id"]){
						document.getElementById("branch").options.add(new Option(res[i]["post_name"],res[i]["post_id"],true,true));
					}
					else{
						document.getElementById("branch").options.add(new Option(res[i]["post_name"],res[i]["post_id"]));
					}
				}  
			},
			error : function(xhr, ajaxOptions, thrownError){
				$("portlet-body").append(xhr.status);
				$("body").append(xhr.responseText );

				alert(thrownError);
			}
		})
	}

	$(':radio').click(function(){
		if($('input[name=bankoption]:checked').val() == 1){
			$.ajax({
				url:"<?php echo base_url('bank/getbank');?>",				
				method:"POST",
				dataType:"json",				
				success:function(res){
					document.getElementById("bank").options.length=0;
					var NumOfData = res.length;
					document.getElementById("bank").options.add(new Option("請選擇",0));
					for (var i = 0; i < NumOfData; i++) {
						document.getElementById("bank").options.add(new Option(res[i]["bank_name"],res[i]["bank_id"]));
					}
				},
				error : function(xhr, ajaxOptions, thrownError){
					$("portlet-body").append(xhr.status);
					$("body").append(xhr.responseText );

					alert(thrownError);
				}
			})
		}

		else{
			document.getElementById("bank").options.length=0;
			document.getElementById("bank").options.add(new Option("請選擇",0));
			document.getElementById("bank").options.add(new Option('中華郵政',700));
		}

	})

	function getoption(){
		var mainselected = $("#bank").val();
		if(mainselected == 700){
			$.ajax({
				url:"<?php echo base_url('bank/combo');?>",				
				method:"POST",
				dataType:"json",
				data:{
					mainselected:mainselected
				},					
				success:function(res){
					document.getElementById("branch").options.length=0;
					var NumOfData = res.length;
					for (var i = 0; i < NumOfData; i++) {
						document.getElementById("branch").options.add(new Option(res[i]["post_name"],res[i]["post_id"]));
					}  
				},
				error : function(xhr, ajaxOptions, thrownError){
					$("portlet-body").append(xhr.status);
					$("body").append(xhr.responseText );

					alert(thrownError);
				}
			})
		}
		else{
			$.ajax({
				url:"<?php echo base_url('bank/combo');?>",				
				method:"POST",
				dataType:"json",
				data:{
					mainselected:mainselected
				},					
				success:function(res){
					document.getElementById("branch").options.length=0;
					document.getElementById("branch").options.add(new Option("請選擇",0));
					var NumOfData = res.length;
					for (var i = 0; i < NumOfData; i++) {
						document.getElementById("branch").options.add(new Option(res[i]["bank_branch_name"],res[i]["bank_branch_id"]));    
					}  
				},
				error : function(xhr, ajaxOptions, thrownError){
					$("portlet-body").append(xhr.status);
					$("body").append(xhr.responseText );

					alert(thrownError);
				}
			})
		}
	}
</script>
<script>
	jQuery(document).ready(function() {   
		// initiate layout and plugins
		FormValidation.init();
	});

	$(".date-picker").datepicker({
		format: 'yyyy-mm-dd',
		todayBtn: 'true',
		todayHighlight : true, 
		// pickerPosition: "bottom-left",
	});
</script> 

</html>