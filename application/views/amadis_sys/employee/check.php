			<!-- BEGIN PAGE -->
			<div class="page-content">
				<!-- BEGIN PAGE CONTAINER-->
				<div class="container-fluid">
					<!-- BEGIN PAGE HEADER-->
					<div class="row-fluid">
						<div class="span12">
							<!-- BEGIN PAGE TITLE & BREADCRUMB-->
							<h3 class="page-title">
								<?php echo $title; ?> <small><?php echo $title_small; ?></small>
							</h3>
							<ul class="breadcrumb">
								<li>
									<i class="icon-home"></i>
									<a href="<?php echo base_url('home');?>">首頁</a> 
									<i class="icon-angle-right"></i>
								</li>
								<li>
									<a href="<?php echo base_url($this->router->fetch_class()); ?>"><?php echo $title; ?></a>
									<i class="icon-angle-right"></i>
								</li>
								<li><a href="#"><?php echo $title_small; ?></a></li>
							</ul>
							<!-- END PAGE TITLE & BREADCRUMB-->
						</div>
					</div>
					<!-- END PAGE HEADER-->
					<div class="row-fluid">
						<div class="span12">
							<!-- BEGIN VALIDATION STATES-->
							<div class="portlet box yellow">
								<div class="portlet-title">
									<div class="caption"><i class="icon-user"></i><?php echo $title_small; ?></div>
									<div class="tools">
										<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<form action="<?php echo base_url('employee/update');?>" name="submitform" id="submitform" class="form-horizontal"  method="post" onsubmit="return check()">
									<div class="alert alert-error hide">
										<button class="close" data-dismiss="alert"></button>
										You have some form errors. Please check below.
									</div>
									<div class="alert alert-success hide">
										<button class="close" data-dismiss="alert"></button>
										Your form validation is successful!
									</div>
									<div class="tabbable tabbable-custom">

										<ul class="nav nav-tabs">

											<li class="active"><a href="#tab_1_1" data-toggle="tab">基本資料</a></li>

											<li><a href="#tab_1_2" data-toggle="tab">帳戶資料</a></li>
											<li><a href="#tab_1_3" data-toggle="tab">公司資料</a></li>

										</ul>

										<div class="tab-content">

											<div class="tab-pane active" id="tab_1_1">

												<div data-always-visible="1" data-rail-visible="0">
													<div class="control-group">
														<label class="control-label">姓名</label>
														<div class="controls">
															<input type="text" name="name" id="name" data-required="1" class="span6 m-wrap" value="<?php echo $result['employee_name'];?>" readonly/>
															<span id="namesMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">身分證字號</label>
														<div class="controls">
															<input name="idcard" id="idcard" type="text" class="span6 m-wrap" value="<?php echo $result['employee_id_card'];?>" readonly/><span id="idMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">所屬單位</label>
														<div class="controls">
															<input name="organization" id="organization" type="text" class="span6 m-wrap" value="<?php echo $organization_name;?>" readonly/>
															<span id="organizationMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">聯絡手機</label>
														<div class="controls">
															<input name="cellphone" id="cellphone" type="text" class="span6 m-wrap" value="<?php echo $result['employee_phone'];?>" readonly/>
															<span id="cellphoneMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">聯絡電話</label>
														<div class="controls">
															<input name="telephone" id="telephone" type="text" class="span6 m-wrap" value="<?php echo $result['employee_tel_1'];?>" readonly/>
															<span id="telephoneMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">聯絡地址</label>
														<div class="controls">
															<input name="addressdetail" type="text" class="span2 m-wrap" value="<?php foreach($address as $a){if($result['address_id'] == $a['address_id']){echo $a['address_city'].$a['address_area'];}}?>" readonly/>
															<input name="addressdetail" type="text" class="span4 m-wrap" value="<?php echo $result['employee_address'];?>" readonly/>
															<span id="addressMsg"></span>
														</div>
													</div>

												</div>

											</div>

											<div class="tab-pane" id="tab_1_2">

												<div data-always-visible="1" data-rail-visible1="1">
													<div class="control-group">
														<label class="control-label">匯款銀行</label>
														<div class="controls">
															<input name="bank" id="bank" type="text" class="span6 m-wrap" value="<?php echo $bankname;?>" readonly/>
															<span id="bankMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">分行名稱</label>
														<div class="controls">
															<input name="branch" id="branch" type="text" class="span6 m-wrap" value="<?php echo $branchname;?>" readonly/>
															<span id="branchMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">戶名</label>
														<div class="controls">
															<input name="account_name" id="account_name" type="text" class="span6 m-wrap" value="<?php echo $result['employee_account_name'];?>" readonly/>
															<span id="accountMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">匯款帳號</label>
														<div class="controls">
															<input name="account" id="account" type="text" class="span6 m-wrap" value="<?php echo $result['employee_bank_account'];?>" readonly/>
															<span id="accountMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">備註</label>
														<div class="controls">
															<input name="note" id="note" type="text" class="span6 m-wrap" value="<?php echo $result['employee_note'];?>" readonly/>
															<span id="noteMsg"></span>
														</div>
													</div>	
												</div>

											</div>

											<div class="tab-pane" id="tab_1_3">

												<div data-always-visible="1" data-rail-visible1="1">
													<div class="control-group">
														<label class="control-label">職稱</label>
														<div class="controls">
															<input name="addressdetail" type="text" class="span4 m-wrap" value="<?php echo $occupation_name;?>" readonly/>
															<span id="occupationMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">推薦人</label>
														<div class="controls">
															<input name="addressdetail" type="text" class="span4 m-wrap" value="<?php echo $referrer['employee_name'];?>" readonly/>
															<span id="referrerMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">直屬經理</label>
														<div class="controls">
															<input name="addressdetail" type="text" class="span4 m-wrap" value="<?php echo $supervisor['employee_name'];?>" readonly/>
															<span id="supervisorMsg"></span>
														</div>
													</div>
												</div>

											</div>

										</div>

									</div>
									<div class="form-actions">
										<a href="<?php echo base_url('employee/list');?>">
											<button type="button" class="btn">返回 <div class="icon-undo"></div></button>
										</a>
									</div>
								</form>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END DASHBOARD STATS -->
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->    
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
	<?php echo $footer; ?>
</div>
<!-- END FOOTER -->
<?php echo $script; ?>
<!-- BEGIN PAGE LEVEL PLUGINS -->

<!-- END PAGE LEVEL PLUGINS -->
</body>
<!-- END BODY -->
</html>