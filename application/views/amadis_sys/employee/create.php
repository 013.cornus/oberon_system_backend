		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">    
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home'); ?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url($this->router->fetch_class()); ?>"><?php echo $title; ?></a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?php echo $title_small; ?></a></li>
							<li class="pull-right no-text-shadow" style="display: none;">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-reorder"></i><?php echo $title_small; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<form action="<?php echo base_url('employee/create');?>" class="form-horizontal" name="employeeform" id="employeeform" method="post">
									<div class="alert alert-error hide">
										<button class="close" data-dismiss="alert"></button>
										<div class="icon-warning-sign"></div>	
										錯誤！請完成所有必填項目
									</div>
									<div class="alert alert-success hide">
										<button class="close" data-dismiss="alert"></button>
										Your form validation is successful!
									</div>
									<div class="tabbable tabbable-custom">
										<ul class="nav nav-tabs">
											<li class="active"><a href="#tab_1_1" data-toggle="tab">基本資料</a></li>
											<li><a href="#tab_1_2" data-toggle="tab">帳戶資料</a></li>
											<li><a href="#tab_1_3" data-toggle="tab">公司資料</a></li>
										</ul>
										<div class="tab-content">
											<div class="tab-pane active" id="tab_1_1">
												<div data-always-visible="1" data-rail-visible="0">

													<div class="control-group">
														<label class="control-label">姓名<span class="required">*</span></label>
														<div class="controls">
															<input type="text" id="name" name="name"  data-required="1" class="span6 m-wrap"/>
															<span id="namesMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">身分證字號<span class="required">*</span></label>
														<div class="controls">
															<input name="idcard" id="idcard" type="text" class="span6 m-wrap" maxlength="10"/><span id="idMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">所屬單位<span class="required">*</span></label>
														<div class="controls">
															<select class="span6 select2_category" name="organization" id="organization" data-placeholder="請選擇">
																<option value=""></option>
																<?php foreach($organization as $d){?>
																	<option value="<?php echo $d["organization_id"];?>"><?php echo $d["organization_name"];?></option>
																<?php }?>
															</select>
															<span id="organizationMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">手機<span class="required">*</span></label>
														<div class="controls">
															<input name="cellphone" id="cellphone" type="text" class="span6 m-wrap" maxlength="10" />
															<span id="cellphoneMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">聯絡電話</label>
														<div class="controls">
															<input name="telephone" id="telephone" type="text" class="span6 m-wrap"/>
															<span id="telephoneMsg"></span>
														</div>
													</div>

													<div class="control-group">
														<label class="control-label">聯絡地址</label>
														<div class="controls">
															<select class="span3 select2_category" name="address" style="width: auto;" data-placeholder="請選擇" tabindex="1">
																<option value=""></option>
																<?php foreach ($result as $r) { ?>
																	<option value="<?php echo $r["address_id"];?>"><?php echo $r["address_zip"].' '.$r["address_city"].$r["address_area"];?></option>
																<?php }?>
															</select>
															<input name="addressdetail" id="addressdetail" type="text" class="span5 m-wrap"/>
															<span id="addressMsg"></span>
														</div>
													</div>							
												</div>

											</div>

											<div class="tab-pane" id="tab_1_2">

												<div data-always-visible="1" data-rail-visible1="1">
													<div class="control-group">
														<label class="control-label">銀行/郵局</label>
														<div class="controls">
															<label class="radio">
																<input type="radio" name="optionsRadios1"value="1"/>銀行</label>
																<label class="radio">
																	<input type="radio" name="optionsRadios1" value="2"/>郵局</label>							
																	<span id="sex"></span>
																</div>
															</div>

															<div class="control-group">
																<label class="control-label">匯款銀行</label>
																<div class="controls">
																	<select class="span6 select2_category"  data-placeholder="請選擇" name="bank" id="bank" onchange="getoption()">
																	<option value=""></option>																
																	</select>
																</div>
															</div>
															<div class="control-group">
																<label class="control-label">分行名稱</label>
																<div class="controls">
																	<select class="span6 select2_category"  data-placeholder="請選擇" name="branch" id="branch">
																		<option value=""></option>

																	</select>
																</div>
															</div>
															<div class="control-group">
																<label class="control-label">匯款帳號</label>
																<div class="controls">
																	<input name="account" id="account" type="text" class="span6 m-wrap"/>
																	<span id="accountMsg"></span>
																</div>
															</div>
															<div class="control-group">
																<label class="control-label">戶名</label>
																<div class="controls">
																	<input name="account_name" id="account_name" type="text" class="span6 m-wrap"/>
																	<span id="accountMsg"></span>
																</div>
															</div>
															<div class="control-group">
																<label class="control-label">備註</label>
																<div class="controls">
																	<input name="note" id="note" type="text" class="span6 m-wrap"/>
																	<span id="telephoneMsg"></span>
																</div>
															</div>
														</div>

													</div>

													<div class="tab-pane" id="tab_1_3">

														<div data-always-visible="1" data-rail-visible1="1">
															<div class="control-group">
																<label class="control-label">職稱<span class="required">*</span></label>
																<div class="controls">
																	<select class="span6 select2_category"  data-placeholder="請選擇" name="occupation" id="occupation">
																		<option value=""></option>
																		<?php foreach ($occupation as $r) { ?>
																			<option value="<?php echo $r["occupation_id"];?>"><?php echo $r["occupation_name"];?></option>
																		<?php }?>
																	</select>
																	<span id="occupationMsg"></span>
																</div>
															</div>
															<div class="control-group">
																<label class="control-label" >推薦人<?php if($right != 1){?><span class="required">*</span><?php }?></label>
																<div class="controls">
																	<select class="span6 select2_category" data-placeholder="請選擇" tabindex="1" name="referrer" id="referrer">
																		<option value=""></option>
																		<?php foreach ($employee as $r) { ?>
																			<option value="<?php echo $r["employee_id"];?>"><?php echo $r["employee_name"];?></option>
																		<?php }?>

																	</select>
																	<span id="referrerMsg"></span>
																</div>
															</div>

															<div class="control-group">
																<label class="control-label" >直屬經理<?php if($right != 1){?><span class="required">*</span><?php }?></label>
																<div class="controls">
																	<select class="span6 select2_category" data-placeholder="請選擇" tabindex="1" name="supervisor" id="supervisor">
																		<option value=""></option>
																		<?php foreach ($employee as $r) { ?>
																			<option value="<?php echo $r["employee_id"];?>"><?php echo $r["employee_name"];?></option>
																		<?php }?>

																	</select>
																	<span id="supervisorMsg"></span>
																</div>
															</div>
														</div>

													</div>

												</div>

											</div>
											<div class="form-actions">
												<button type="submit" class="btn green">
													新增 <div class="icon-save"></div>
												</button>
												<a href="<?php echo base_url('employee');?>">
													<button type="button" class="btn">取消 <div class="icon-undo"></div></button>
												</a>
											</div>
										</form>

										<!-- END FORM-->
									</div>
								</div>
								<!-- END VALIDATION STATES-->
							</div>
						</div>
						<!-- END DASHBOARD STATS -->
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- END PAGE CONTAINER-->    
			</div>
			<!-- END PAGE -->
		</div>
		<!-- END CONTAINER -->
		<!-- BEGIN FOOTER -->
		<div class="footer">
			<?php echo $footer; ?>
		</div>
		<!-- END FOOTER -->

<?php echo $script; ?>
<script src="<?php echo base_url('public/globel/js/form-samples.js')?>" type="text/javascript"></script> 
<script src="<?php echo base_url('public/globel/js/select2.min.js')?>" type="text/javascript"></script> 
<script src="<?php echo base_url('public/globel/js/jquery.validate.min.js')?>" type="text/javascript"></script> 
<?php if($right == 0){?>
	<script src="<?php echo base_url('public/globel/plugin/main/employee-validation.js')?>" type="text/javascript"></script>
<?php } else {?>
	<script src="<?php echo base_url('public/globel/plugin/main/employee-for-topmanager-validation.js')?>" type="text/javascript"></script>
<?php }?>

<script type="text/javascript">
	$(':radio').click(function(){
		if($('input[type=radio]:checked').val() == 1){
			$.ajax({
				url:"<?php echo base_url('bank/getbank');?>",				
				method:"POST",
				dataType:"json",				
				success:function(res){
					document.getElementById("bank").options.length=0;
					var NumOfData = res.length;
					document.getElementById("bank").options.add(new Option("請選擇",0));
					for (var i = 0; i < NumOfData; i++) {
						document.getElementById("bank").options.add(new Option(res[i]["bank_name"],res[i]["bank_id"]));
					}  
				},
				error : function(xhr, ajaxOptions, thrownError){
					$("portlet-body").append(xhr.status);
					$("body").append(xhr.responseText );

					alert(thrownError);
				}
			})
		}

		else{
			document.getElementById("bank").options.length=0;
			document.getElementById("bank").options.add(new Option("請選擇",0));
			document.getElementById("bank").options.add(new Option('中華郵政',700));
		}

	})

	function getoption(){
		var mainselected = $("#bank").val();
		if(mainselected == 700){
			$.ajax({
				url:"<?php echo base_url('bank/combo');?>",				
				method:"POST",
				dataType:"json",
				data:{
					mainselected:mainselected
				},					
				success:function(res){
					document.getElementById("branch").options.length=0;
					var NumOfData = res.length;
					for (var i = 0; i < NumOfData; i++) {
						document.getElementById("branch").options.add(new Option(res[i]["post_name"],res[i]["post_id"]));    
					}  
				},
				error : function(xhr, ajaxOptions, thrownError){
					$("portlet-body").append(xhr.status);
					$("body").append(xhr.responseText );

					alert(thrownError);
				}
			})
		}
		else{
			$.ajax({
				url:"<?php echo base_url('bank/combo');?>",				
				method:"POST",
				dataType:"json",
				data:{
					mainselected:mainselected
				},					
				success:function(res){
					document.getElementById("branch").options.length=0;
					var NumOfData = res.length;
					for (var i = 0; i < NumOfData; i++) {
						document.getElementById("branch").options.add(new Option(res[i]["bank_branch_name"],res[i]["bank_branch_id"]));    
					}  
				},
				error : function(xhr, ajaxOptions, thrownError){
					$("portlet-body").append(xhr.status);
					$("body").append(xhr.responseText );

					alert(thrownError);
				}
			})
		}
	}
</script>


<script>
	jQuery(document).ready(function() {   
		FormValidation.init();
	});
</script>

</body>
<!-- END BODY -->
</html>