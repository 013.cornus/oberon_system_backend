			<!-- BEGIN PAGE -->
			<div class="page-content">
				<!-- BEGIN PAGE CONTAINER-->
				<div class="container-fluid">
					<!-- BEGIN PAGE HEADER-->
					<div class="row-fluid">
						<div class="span12">
							<!-- BEGIN PAGE TITLE & BREADCRUMB-->
							<h3 class="page-title">
								<?php echo $title; ?> <small><?php echo $title_small; ?></small>
							</h3>
							<ul class="breadcrumb">
								<li>
									<i class="icon-home"></i>
									<a href="<?php echo base_url('home');?>">首頁</a> 
									<i class="icon-angle-right"></i>
								</li>
								<li>
									<a href="<?php echo base_url($this->router->fetch_class()); ?>"><?php echo $title; ?></a>
									<i class="icon-angle-right"></i>
								</li>
								<li><a href="#"><?php echo $title_small; ?></a></li>
							</ul>
							<!-- END PAGE TITLE & BREADCRUMB-->
						</div>
					</div>
					<!-- END PAGE HEADER-->
					<div class="row-fluid">
						<div class="span12">
							<!-- BEGIN VALIDATION STATES-->
							<div class="portlet box green">
								<div class="portlet-title">
									<div class="caption"><i class="icon-user"></i><?php echo $title_small; ?></div>
									<div class="tools">
										<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<form action="<?php echo base_url('employee/update');?>" name="employeeform" id="employeeform" class="form-horizontal"  method="post" onsubmit="return check()">
									<div class="alert alert-error hide">
										<button class="close" data-dismiss="alert"></button>
										<div class="icon-warning-sign"></div>	
										錯誤！請完成所有必填項目！
									</div>
									<div class="alert alert-success hide">
										<button class="close" data-dismiss="alert"></button>
										Your form validation is successful!
									</div>
									<div class="tabbable tabbable-custom">
										<ul class="nav nav-tabs">
											<li class="active"><a href="#tab_1_1" data-toggle="tab">基本資料</a></li>
											<li><a href="#tab_1_2" data-toggle="tab">帳戶資料</a></li>
											<li><a href="#tab_1_3" data-toggle="tab">公司資料</a></li>
										</ul>
										<div class="tab-content">
											<div class="tab-pane active" id="tab_1_1">
												<div data-always-visible="1" data-rail-visible="0">
													<div class="control-group">
														<label class="control-label">姓名<span class="required">*</span></label>
														<div class="controls">
															<input type="text" name="name" id="name" data-required="1" class="span6 m-wrap" value="<?php echo $result['employee_name'];?>"/>
															<span id="namesMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">身分證字號<span class="required">*</span></label>
														<div class="controls">
															<input name="idcard" id="idcard" type="text" class="span6 m-wrap" value="<?php echo $result['employee_id_card'];?>"/><span id="idMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">所屬單位<span class="required">*</span></label>
														<div class="controls">
															<select class="span6 select2_category" name="organization" id="organization" data-placeholder="請選擇">
																<option value=""></option>
																<?php foreach ($organization as $r) {?>
																<option value="<?php echo $r["organization_id"];?>" <?php if($r["organization_id"] == $result["organization_id"])
																{echo "selected='selected'";}?>><?php echo $r["organization_name"];?></option>
																<?php }?>
															</select>
															<span id="organizationMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">聯絡手機<span class="required">*</span></label>
														<div class="controls">
															<input name="cellphone" id="cellphone" type="text" class="span6 m-wrap" value="<?php echo $result['employee_phone'];?>"/>
															<span id="cellphoneMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">聯絡電話<span class="required">*</span></label>
														<div class="controls">
															<input name="telephone" id="telephone" type="text" class="span6 m-wrap" value="<?php echo $result['employee_tel_1'];?>"/>
															<span id="telephoneMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">聯絡地址<span class="required">*</span></label>
														<div class="controls">
															<select class="span3 select2_category" name="address" id="address" data-placeholder="請選擇" >
																<option value=""></option>
																<?php foreach ($address as $r) {?>
																<option value="<?php echo $r["address_id"];?>" <?php if($r["address_id"] == $result["address_id"])
																{echo "selected='selected'";}?>><?php echo $r["address_zip"].' '.$r["address_city"].'&nbsp;'.$r["address_area"];?></option>
																<?php }?>
															</select>
															<input name="addressdetail" type="text" class="span4 m-wrap" value="<?php echo $result['employee_address'];?>"/>
															<span id="addressMsg"></span>
														</div>
													</div>
												</div>
											</div>
											<div class="tab-pane" id="tab_1_2">
												<div data-always-visible="1" data-rail-visible1="1">
													<div class="control-group">
														<label class="control-label">銀行/郵局</label>
														<div class="controls">
															<label class="radio">
																<input type="radio" name="optionsRadios1"value="1" <?php if($result['employee_bank_name'] != 700){echo "checked";}?>/>銀行</label>
															<label class="radio">
																<input type="radio" name="optionsRadios1" value="2" <?php if($result['employee_bank_name'] == 700){echo "checked";}?>/>郵局</label>	
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">匯款銀行</label>
														<div class="controls">
															<select class="span6 select2_category"  data-placeholder="請選擇" name="bank" id="bank" onchange="getoption()">
																<option value=""></option>
															</select>
														</div>
													</div>

													<div class="control-group">
														<label class="control-label">分行名稱</label>
														<div class="controls">
															<select class="span6 select2_category"  data-placeholder="請選擇" name="branch" id="branch">
																<option value=""></option>
															</select>
														</div>
													</div>

													<div class="control-group">
														<label class="control-label">戶名</label>
														<div class="controls">
															<input name="account_name" id="account_name" type="text" class="span6 m-wrap" value="<?php echo $result['employee_account_name'];?>" />
															<span id="accountMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">匯款帳號</label>
														<div class="controls">
															<input name="account" id="account" type="text" class="span6 m-wrap" value="<?php echo $result['employee_bank_account'];?>"/>
															<span id="accountMsg"></span>
														</div>
													</div>
													<div class="control-group">
														<label class="control-label">備註</label>
														<div class="controls">
															<input name="note" id="note" type="text" class="span6 m-wrap" value="<?php echo $result['employee_note'];?>"/>
															<span id="noteMsg"></span>
														</div>
													</div>	
												</div>

											</div>
											<div class="tab-pane" id="tab_1_3">
												<div data-always-visible="1" data-rail-visible1="1">
													<div class="control-group">
														<label class="control-label">職稱<span class="required">*</span></label>
														<div class="controls">
														<?php if($disabled == 0){?>
															<select class="span6 select2_category" name="occupation" id="occupation" data-placeholder="請選擇" onclick="showvalue()">
																<option value=""></option>
																<?php foreach ($occupation as $r) { ?>
																<option value="<?php echo $r["occupation_id"];?>" <?php if($r["occupation_id"] == $result["occupation_id"])
																{echo "selected='selected'";}?>><?php echo $r["occupation_name"];?></option>
																<?php }?>
															</select>
														<?php } else{ ?>
															<input type="text" class="span6 m-wrap" value="<?php echo $result['occupation_name'];?>" readonly/>
															<input type="text" name="occupation" id="occupation" value="<?php echo $result['occupation_id'];?>" style="display: none">
															<span id="occupationMsg"></span>
														<?php }?>
														</div>
													</div>

													<div class="control-group">
														<label class="control-label">推薦人<span class="required">*</span></label>
														<div class="controls">
															<select class="span6 select2_category" name="referrer" id="referrer" data-placeholder="請選擇" >
																<option value=""></option>
																<?php foreach ($employee as $r) {?>
																<option value="<?php echo $r["employee_id"];?>" <?php if($r["employee_id"]==$result["users_id"])
																{echo "selected='selected'";}?>><?php echo $r["employee_name"];?></option>
																<?php }?>
															</select>
															<span id="referrerMsg"></span>
														</div>
													</div>
													
													<div class="control-group">
														<label class="control-label">直屬經理<span class="required">*</span></label>
														<div class="controls">
															<select class="span6 select2_category" name="supervisor" id="supervisor" data-placeholder="請選擇">
																<option value=""></option>
																<?php foreach ($employee as $r) {?>
																<option value="<?php echo $r["employee_id"];?>" <?php if($r["employee_id"]==$result["employee_supervisor"])
																{echo "selected='selected'";}?>><?php echo $r["employee_name"];?></option>
																<?php }?>
															</select>
															<span id="supervisorMsg"></span>
														</div>
													</div>
												</div>

											</div>

										</div>

									</div>
									<div class="form-actions">
										<button type="submit" class="btn green" name="id" value="<?php echo $upid;?>">儲存 <div class="icon-save"></div></button>
										<a href="<?php echo base_url('employee/list');?>">
											<button type="button" class="btn">返回 <div class="icon-undo"></div></button>
										</a>
									</div>
								</form>
								<!-- END FORM-->
							</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END DASHBOARD STATS -->
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->    
	</div>
	<!-- END PAGE -->
</div>

<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="footer">
	<?php echo $footer; ?>
</div>
<!-- END FOOTER -->
<?php echo $script; ?>
<script src="<?php echo base_url('public/globel/js/form-samples.js')?>" type="text/javascript"></script> 
<script src="<?php echo base_url('public/globel/js/select2.min.js')?>" type="text/javascript"></script> 
<script src="<?php echo base_url('public/globel/js/jquery.validate.min.js')?>" type="text/javascript"></script> 
<?php if($right == 0){?>
	<script src="<?php echo base_url('public/globel/plugin/main/employee-validation.js')?>" type="text/javascript"></script>
<?php } else {?>
	<script src="<?php echo base_url('public/globel/plugin/main/employee-for-topmanager-validation.js')?>" type="text/javascript"></script>
<?php }?>

<script type="text/javascript">
	var selectedid = 0;
	if($('input[type=radio]:checked').val() == 1){
			$.ajax({
				url:"<?php echo base_url('bank/getbank');?>",				
				method:"POST",
				dataType:"json",
				async: false,				
				success:function(res){
					document.getElementById("bank").options.length=0;
					var NumOfData = res.length;
					document.getElementById("bank").options.add(new Option("請選擇",0));
					for (var i = 0; i < NumOfData; i++) {
						if(<?php echo $result['employee_bank_name'];?> == res[i]["bank_id"]){
							document.getElementById("bank").options.add(new Option(res[i]["bank_name"],res[i]["bank_id"],true,true));
							selectedid = res[i]["bank_id"];
						}
						else{
							document.getElementById("bank").options.add(new Option(res[i]["bank_name"],res[i]["bank_id"]));
						}
					}  
				},
				error : function(xhr, ajaxOptions, thrownError){
					$("portlet-body").append(xhr.status);
					$("body").append(xhr.responseText );

					alert(thrownError);
				}
			})

			var mainselected = selectedid;

			$.ajax({
				url:"<?php echo base_url('bank/combo');?>",				
				method:"POST",
				dataType:"json",
				async: false,
				data:{
					mainselected:mainselected
				},					
				success:function(res){
					document.getElementById("branch").options.length=0;
					var NumOfData = res.length;
					for (var i = 0; i < NumOfData; i++) {
						if(<?php echo $result['employee_bank_branch'];?> == res[i]["bank_branch_id"]){
							document.getElementById("branch").options.add(new Option(res[i]["bank_branch_name"],res[i]["bank_branch_id"],true,true));
						}
						else{
							document.getElementById("branch").options.add(new Option(res[i]["bank_branch_name"],res[i]["bank_branch_id"]));
						}    
					}  
				},
				error : function(xhr, ajaxOptions, thrownError){
					$("portlet-body").append(xhr.status);
					$("body").append(xhr.responseText );

					alert(thrownError);
				}
			})
	}

	else{
		document.getElementById("bank").options.length=0;
		document.getElementById("bank").options.add(new Option("請選擇",0));
		document.getElementById("bank").options.add(new Option('中華郵政',700,true,true));

		mainselected = 700;
			$.ajax({
				url:"<?php echo base_url('bank/combo');?>",				
				method:"POST",
				dataType:"json",
				async: false,
				data:{
					mainselected:mainselected
				},					
				success:function(res){
					document.getElementById("branch").options.length=0;
					var NumOfData = res.length;
					for (var i = 0; i < NumOfData; i++) {
						if(<?php echo $result['employee_bank_branch'];?> == res[i]["post_id"]){
							document.getElementById("branch").options.add(new Option(res[i]["post_name"],res[i]["post_id"],true,true));
						}
						else{
							document.getElementById("branch").options.add(new Option(res[i]["post_name"],res[i]["post_id"]));
						}
					}  
				},
				error : function(xhr, ajaxOptions, thrownError){
					$("portlet-body").append(xhr.status);
					$("body").append(xhr.responseText );

					alert(thrownError);
				}
			})
	}

	$(':radio').click(function(){
		if($('input[type=radio]:checked').val() == 1){
			$.ajax({
				url:"<?php echo base_url('bank/getbank');?>",				
				method:"POST",
				dataType:"json",				
				success:function(res){
					document.getElementById("bank").options.length=0;
					var NumOfData = res.length;
					document.getElementById("bank").options.add(new Option("請選擇",0));
					for (var i = 0; i < NumOfData; i++) {
						document.getElementById("bank").options.add(new Option(res[i]["bank_name"],res[i]["bank_id"]));
					}
				},
				error : function(xhr, ajaxOptions, thrownError){
					$("portlet-body").append(xhr.status);
					$("body").append(xhr.responseText );

					alert(thrownError);
				}
			})
		}

		else{
			document.getElementById("bank").options.length=0;
			document.getElementById("bank").options.add(new Option("請選擇",0));
			document.getElementById("bank").options.add(new Option('中華郵政',700));
		}

	})

	function getoption(){
		var mainselected = $("#bank").val();
		if(mainselected == 700){
			$.ajax({
				url:"<?php echo base_url('bank/combo');?>",				
				method:"POST",
				dataType:"json",
				data:{
					mainselected:mainselected
				},					
				success:function(res){
					document.getElementById("branch").options.length=0;
					var NumOfData = res.length;
					for (var i = 0; i < NumOfData; i++) {
						document.getElementById("branch").options.add(new Option(res[i]["post_name"],res[i]["post_id"]));
					}  
				},
				error : function(xhr, ajaxOptions, thrownError){
					$("portlet-body").append(xhr.status);
					$("body").append(xhr.responseText );

					alert(thrownError);
				}
			})
		}
		else{
			$.ajax({
				url:"<?php echo base_url('bank/combo');?>",				
				method:"POST",
				dataType:"json",
				data:{
					mainselected:mainselected
				},					
				success:function(res){
					document.getElementById("branch").options.length=0;
					var NumOfData = res.length;
					for (var i = 0; i < NumOfData; i++) {
						document.getElementById("branch").options.add(new Option(res[i]["bank_branch_name"],res[i]["bank_branch_id"]));    
					}  
				},
				error : function(xhr, ajaxOptions, thrownError){
					$("portlet-body").append(xhr.status);
					$("body").append(xhr.responseText );

					alert(thrownError);
				}
			})
		}
	}
</script>

<script>
	jQuery(document).ready(function() {   
		// initiate layout and plugins
		FormValidation.init();
	});
</script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- END PAGE LEVEL PLUGINS -->
</body>
<!-- END BODY -->
</html>