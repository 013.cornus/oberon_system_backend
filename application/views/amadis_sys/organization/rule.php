<script type="text/javascript">
	var rule1=/./;
	var rule2=/.@./;
	var rule3=/^[A-Z]{1}[0-9]{9}$/;
	var rule4=/^09\d{8}/;
	var rule5=/^0\d{1,2}-?(\d{6,8})(#\d{1,5}){0,1}/;
	var rule6=/[\u4e00-\u9fa5]{0,2}縣|市[\u4e00-\u9fa5]{2,4}鄉|鎮|市|區[\u4e00-\u9fa5]{2,4}路|街|大道.*\d*號./;
	//營業處名稱
	$("#organization").blur(function(){
		if(rule1.test($(this).val())){	
			$(this).css("border-color","green")
			organizationMsg.innerHTML="OK";
			organizationMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			organizationMsg.innerHTML="營業處名稱不可為空";
			organizationMsg.style.color="red";
		}
	})
	//副總經理
	$("#principal").blur(function(){
		if(rule1.test($(this).val()) && $(this).val() != ""){	
			$(this).css("border-color","green")
			principalMsg.innerHTML="OK";
			principalMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			principalMsg.innerHTML="副總經理不可為空";
			principalMsg.style.color="red";
		}
	})

	//推薦人
	$("#referrer").blur(function(){
		if(rule1.test($(this).val()) && $(this).val() != ""){	
			$(this).css("border-color","green")
			referrerMsg.innerHTML="OK";
			referrerMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			referrerMsg.innerHTML="推薦人不可為空";
			referrerMsg.style.color="red";
		}
	})

	//母單位
	$("#parentunit").blur(function(){
		if(rule1.test($(this).val()) && $(this).val() != ""){	
			$(this).css("border-color","green")
			parentunitMsg.innerHTML="OK";
			parentunitMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			parentunitMsg.innerHTML="母單位不可為空";
			parentunitMsg.style.color="red";
		}
	})

	//地址(區)
	$("#address").blur(function(){
		if(rule1.test($(this).val()) && rule1.test($("#paddressdetail").val())){	
			$(this).css("border-color","green")
			$("#paddressdetail").css("border-color","green")
			paddressdetailMsg.innerHTML="OK";
			paddressdetailMsg.style.color="green";
		}else{
			$(this).css("border-color","red")
			$("#paddressdetail").css("border-color","red")
			paddressdetailMsg.innerHTML="地址不可為空";
			paddressdetailMsg.style.color="red";
		}
	})

	function check() {
		if (rule1.test($("#organization").val()) && rule1.test($("#principal").val()) && rule1.test($("#referrer").val()) && rule1.test($("#parentunit").val()) && rule1.test($("#address").val()) && rule1.test($("#paddressdetail").val())){
			document.getElementById("submitform").submit();	
		} else {
			alert("請輸入完整資訊");

			if(rule1.test($("#organization").val()) == false){
				$("#organization").css("border-color","red")
				organizationMsg.innerHTML="營業處名稱不可為空";
				organizationMsg.style.color="red";
			}

			if(rule1.test($("#principal").val()) == false){
				$("#principal").css("border-color","red")
				principalMsg.innerHTML="副總經理不可為空";
				principalMsg.style.color="red";
			}

			if(rule1.test($("#referrer").val()) == false){
				$("#referrer").css("border-color","red")
				referrerMsg.innerHTML="推薦人不可為空";
				referrerMsg.style.color="red";
			}

			if(rule1.test($("#parentunit").val()) == false){
				$("#parentunit").css("border-color","red")
				parentunitMsg.innerHTML="母單位不可為空";
				parentunitMsg.style.color="red";
			}

			if(rule1.test($("#address").val()) == false || rule1.test($("#paddressdetail").val()) == false){
				$("#address").css("border-color","red")
				$("#paddressdetail").css("border-color","red")
				paddressdetailMsg.innerHTML="地址不可為空";
				paddressdetailMsg.style.color="red";
			}
		}
	}
</script>
</script>