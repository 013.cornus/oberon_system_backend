		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">    
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home');?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url($this->router->fetch_class()); ?>"><?php echo $title; ?></a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?php echo $title_small; ?></a></li>
							<li class="pull-right no-text-shadow" style="display: none;">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-reorder"></i><?php echo $title_small; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<form action="<?php echo base_url('service_users/create');?>" class="form-horizontal"  method="post" id="createForm" onsubmit="return check();">
									<div class="alert alert-error hide">
										<button class="close" data-dismiss="alert"></button>
										錯誤！請完成所有必填項目
									</div>
									<div class="alert alert-success hide">
										<button class="close" data-dismiss="alert"></button>
										Your form validation is successful!
									</div>
<!-- 									<div class="alert alert-info">
										<i class="icon-warning-sign"></i> 注意！新創建帳號的密碼一律是：<span style="color: red">000000</span>
									</div> -->

									<!-- <div class="control-group" id='account_type' style="display: none;">
										<label class="control-label">帳號種類<span class="required">*</span></label>
										<div class="controls chzn-controls">
											<select name="employee" id="type" class="span6 select2_category " data-with-diselect="1" name="type" data-placeholder="請選擇用戶" tabindex="1">
												<option value=""></option>
												<option value="1">總部</option>
												<option value="2">業務</option>
												<option value="3">客戶</option>
												<option value="4">業務+客戶</option>
											</select>
											<span id="errorMsg"></span>
										</div>
									</div> -->

									<div class="control-group">
										<label class="control-label">權限<span class="required">*</span></label>
										<div class="controls chzn-controls">
											<select name="competence" id="form_3_chosen" class="span6 select2_category" data-with-diselect="1" name="options1" data-placeholder="請選擇權限" tabindex="1" required>
												<option value=""></option>
												<?php foreach ($competence as $val) { ?>
													<?php if($this->session->userdata('competence_id') == 3){?>
														<?php if($val['competence_id'] >= 3){?>
															<option value="<?php echo $val["competence_id"];?>"></option>
														<?php }?>
													<?php }else{?>
														<option value="<?php echo $val["competence_id"];?>"><?php echo $val["competence_name"];?></option>
													<?php }?>
												<?php }?>
											</select>
											<span id="errorMsg"></span>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label">群組類別<span class="required">*</span></label>
										<div class="controls chzn-controls">
											<select name="group_type" id="form_3_chosen" class="span6 select2_category" data-with-diselect="1" name="options1" data-placeholder="請選擇權限" tabindex="1">
												<?php foreach($groupList as $key => $row){?>
													<option value="<?php echo $row['group_info_type']?>"><?php echo $row['group_info_name']?></option>
												<?php }?>
											</select>
											<span id="errorMsg"></span>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label">用戶名稱<span class="required">*</span></label>
										<div class="controls chzn-controls">
											<input name="users_name" id="users_name" type="text" class="span6 m-wrap" required/>
											<span id="errorMsg"></span>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label">銷售/服務端帳號<span class="required">*</span></label>
										<div class="controls">
											<input name="account" id="account" type="text" class="span6 m-wrap" required/>
											<span id="errorMsg"></span>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label">銷售/服務端密碼<span class="required">*</span></label>
										<div class="controls">
											<input name="password" id="password" type="password" class="span6 m-wrap" required/>
											<span id="errorMsg"></span>
										</div>
									</div>

									<div class="control-group">
										<label class="control-label">再輸入一次密碼<span class="required">*</span></label>
										<div class="controls">
											<input name="password2" id="password2" type="password" class="span6 m-wrap" required/>
											<span id="errorMsg"></span>
										</div>
									</div>

									<div class="form-actions">
										<button type="submit" class="btn green">
											新增 <div class="icon-save"></div>
										</button>
										<a href="<?php echo base_url('users');?>">
											<button type="button" class="btn">取消 <div class="icon-undo"></div></button>
										</a>
									</div>

								</form>
								<!-- END FORM-->
							</div>

						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<!-- END DASHBOARD STATS -->
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- END PAGE CONTAINER-->    
	</div>
	<!-- END PAGE -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
<div class="footer">
	<?php echo $footer; ?>
</div>
<!-- END FOOTER -->
<?php echo $script; ?>

<script type="text/javascript">
	function check(){
		val1 = $('#password').val();
		val2 = $('#password2').val();
		if(val1 != val2){
			alert('密碼不相同,請確認輸入兩次相同密碼');
			return false;
		}
	}
</script>
</body>
<!-- END BODY -->
</html>