		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">    
						<!-- BEGIN PAGE TITLE & BREADCRUMB-->
						<h3 class="page-title">
							<?php echo $title; ?> <small><?php echo $title_small; ?></small>
						</h3>
						<ul class="breadcrumb">
							<li>
								<i class="icon-home"></i>
								<a href="<?php echo base_url('home');?>">首頁</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li>
								<a href="<?php echo base_url($this->router->fetch_class()); ?>"><?php echo $title; ?></a>
								<i class="icon-angle-right"></i>
							</li>
							<li><a href="#"><?php echo $title_small; ?></a></li>
							<li class="pull-right no-text-shadow" style="display: none;">
								<div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="icon-calendar"></i>
									<span></span>
									<i class="icon-angle-down"></i>
								</div>
							</li>
						</ul>
						<!-- END PAGE TITLE & BREADCRUMB-->
					</div>
				</div>
				<!-- END PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">
						<!-- BEGIN VALIDATION STATES-->
						<div class="portlet box green">
							<div class="portlet-title">
								<div class="caption"><i class="icon-reorder"></i><?php echo $title_small; ?></div>
								<div class="tools">
									<a href="javascript:;" class="collapse"></a>
									<!-- <a href="#portlet-config" data-toggle="modal" class="config"></a>
									<a href="javascript:;" class="reload"></a>
									<a href="javascript:;" class="remove"></a> -->
								</div>
							</div>
							<div class="portlet-body form">
								<!-- BEGIN FORM-->
								<h3></h3>
								<form action="<?php echo base_url('txt_to_pdf/create');?>" class="form-horizontal"  method="post" enctype="multipart/form-data">
									<div class="alert alert-error hide">
										<button class="close" data-dismiss="alert"></button>
										錯誤！請完成所有必填項目
									</div>
									<div class="alert alert-success hide">
										<button class="close" data-dismiss="alert"></button>
										Your form validation is successful!
									</div>
									<!-- <div class="control-group">
										<label class="control-label">excel匯入<span class="required">*</span></label>
										<div class="controls">
											<input name="excel" id="excel" type="file" class="span6 m-wrap"/><span id="productnameMsg"></span>
										</div>
									</div> -->

									<div class="control-group" id="addExcel">
										<label class="control-label">出貨資料</label>
										<div class="controls" >
											<button class="btn green" type="button" name="addgoodsExcel" id="addgoodsExcel" class="btn">增加匯入欄位 <i class="icon-plus"></i></button>
										</div>
										<div class="control-group"></div>
									</div>
									<div class="control-group" id="importExcel" style="display: none">
										<!-- <label class="control-label">來源頁數<span class="required">*</span></label> -->
										<div class="controls">
											<button class="btn red icn-only" type="button" name="delegoodsExcel" id="delegoodsExcel" class="btn" onclick="deltxt(0)"><i class="icon-remove"></i></button>
											&nbsp;&nbsp;來源頁數：
											<input name="" id="origPageExcel" type="text" class="span1 m-wrap"/><span id="productnameMsg"></span>
											&nbsp;&nbsp;來源欄(英文)：
											<input type="text" name="" id="origColExcel" type="text" class="span1 m-wrap" style=""/><span id="countMsg"></span>
											&nbsp;&nbsp;來源列(數字)：
											<input type="text" name="" id="origRowExcel" type="text" class="span1 m-wrap" style=""/><span id="countMsg"></span>
											&nbsp;&nbsp;目標頁數：
											<input type="text" name="" id="destPageExcel" type="text" class="span1 m-wrap" style=""/><span id="countMsg"></span>
											&nbsp;&nbsp;目標欄(英文)：
											<input type="text" name="" id="destColExcel" type="text" class="span1 m-wrap" style=""/><span id="countMsg"></span>
											&nbsp;&nbsp;目標列(數字)：
											<input type="text" name="" id="destRowExcel" type="text" class="span1 m-wrap" style=""/><span id="countMsg"></span>
										</div>
									</div>

									<div class="control-group" id="goods">
										<label class="control-label">檢驗資料</label>
										<div class="controls" >
											<button class="btn green" type="button" name="addgoods" id="addgoods" class="btn" onclick="create()">增加匯入欄位 <i class="icon-plus"></i></button>
										</div>
										<div class="control-group"></div>
									</div>
									<div class="control-group" id="import" style="display: none">
										<!-- <label class="control-label">來源頁數<span class="required">*</span></label> -->
										<div class="controls">
											<button class="btn red icn-only" type="button" name="delegoods1" id="delegoods" class="btn" onclick="deltxt(0)"><i class="icon-remove"></i></button>
											&nbsp;&nbsp;來源字元：
											<input type="text" name="" id="origString" type="text" class="span1 m-wrap" style=""/><span id="countMsg"></span>
											&nbsp;&nbsp;來源欄(數字)：
											<input type="text" name="" id="origCol" type="text" class="span1 m-wrap" style=""/><span id="countMsg"></span>
											&nbsp;&nbsp;目標頁數：
											<input type="text" name="" id="destPage" type="text" class="span1 m-wrap" style=""/><span id="countMsg"></span>
											&nbsp;&nbsp;目標欄(英文)：
											<input type="text" name="" id="destCol" type="text" class="span1 m-wrap" style=""/><span id="countMsg"></span>
											&nbsp;&nbsp;目標列(數字)：
											<input type="text" name="" id="destRow" type="text" class="span1 m-wrap" style=""/><span id="countMsg"></span>
											<!-- &nbsp;&nbsp;目標資料格式：
												<input type="text" name="" id="destRow" type="text" class="span1 m-wrap" style=""/><span id="countMsg"></span> -->
											</div>
										</div>
										<div class="form-actions">
											<button type="submit" class="btn green">
												新增 <div class="icon-save"></div>
											</button>
											<a href="<?php echo base_url('goods');?>">
												<button type="button" class="btn">取消 <div class="icon-undo"></div></button>
											</a>
										</div>
									</form>
									<!-- END FORM-->
								</div>
							</div>
							<!-- END VALIDATION STATES-->
						</div>
					</div>
					<!-- END DASHBOARD STATS -->
					<div class="clearfix"></div>
				</div>
			</div>
			<!-- END PAGE CONTAINER-->    
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->

	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php echo $footer; ?>
	</div>
	<!-- END FOOTER -->
	<?php echo $script; ?>
	<script src="<?php echo base_url('public/globel/js/form-samples.js')?>" type="text/javascript"></script> 
	<script src="<?php echo base_url('public/globel/js/select2.min.js')?>" type="text/javascript"></script>
	<script src="<?php echo base_url('public/globel/js/jquery.validate.min.js')?>" type="text/javascript"></script>
	<script src="<?php echo base_url('public/globel/plugin/main/goods-validation.js')?>" type="text/javascript"></script>

	<script>
		jQuery(document).ready(function() {    
			FormValidation.init();
			FormSamples.init();
		});
	</script>
	<!-- BEGIN RULE -->

	<script>
  //set the default value
  var txtId = 0;
  var ExcelId = 0;
  // document.getElementById("test").innerHTML = ;
  count = new Array();
  var arraylength = 0;
  //add input block in showBlock
  $("#addgoods").click(function(){

  	txtId ++;
  	$copy = $("#import").clone(true).attr("id","import" + txtId).attr("style","display:block");
  	$("#goods").append($copy);
  	$copy.find("#origPage").attr("name","origPage[]").attr("value","").attr("id","origPage" + txtId);
  	$copy.find("#origString").attr("name","origString[]").attr("value","").attr("id","origString" + txtId);
  	$copy.find("#origCol").attr("name","origCol[]").attr("value","").attr("id","origCol" + txtId);
  	$copy.find("#destPage").attr("name","destPage[]").attr("value","").attr("id","destPage" + txtId);
  	$copy.find("#destCol").attr("name","destCol[]").attr("value","").attr("id","destCol" + txtId);
  	$copy.find("#destRow").attr("name","destRow[]").attr("value","").attr("id","destRow" + txtId);
  	$copy.find("#delegoods").attr("onclick","deltxt(" + txtId + ");").attr('id','delegoods'+txtId);
  });

  //remove div
  function deltxt(id) {
  	$("#import" + id).remove();
  }

  $("#addgoodsExcel").click(function(){
  	ExcelId ++;
  	$copy = $("#importExcel").clone(true).attr("id","import" + ExcelId).attr("style","display:block");
  	$("#addExcel").append($copy);
  	$copy.find("#origPageExcel").attr("name","origPageExcel[]").attr("value","").attr("id","origPageExcel" + txtId);
  	$copy.find("#origColExcel").attr("name","origColExcel[]").attr("value","").attr("id","origColExcel" + txtId);
  	$copy.find("#origRowExcel").attr("name","origRowExcel[]").attr("value","").attr("id","origRowExcel" + txtId);
  	$copy.find("#destPageExcel").attr("name","destPageExcel[]").attr("value","").attr("id","destPageExcel" + ExcelId);
  	$copy.find("#destColExcel").attr("name","destColExcel[]").attr("value","").attr("id","destColExcel" + ExcelId);
  	$copy.find("#destRowExcel").attr("name","destRowExcel[]").attr("value","").attr("id","destRowExcel" + ExcelId);
  	$copy.find("#delegoodsExcel").attr("onclick","delExcel(" + ExcelId + ");").attr('id','delegoodsExcel'+ExcelId);
  });

  //remove div
  function delExcel(id) {
  	$("#import" + id).remove();
  }

  $('#other').click(function() {
  	$('#target').submit();
  });
</script> 

<!-- END RULE -->
</body>
<!-- END BODY -->
</html>