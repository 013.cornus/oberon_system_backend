var FormValidation = function () {


    return {
        //main function to initiate the module
        init: function () {

            // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation

            var form1 = $('#submitform');
            var error1 = $('.alert-error', form1);
            var success1 = $('.alert-success', form1);

            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-inline', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    mainname: {
                        required: true
                    },

                    subname: {
                        required: true
                    },

                    icon: {
                        required: true
                    },

                    code: {
                        required: true
                    },

                    frontlink: {
                        required: true
                    },

                    backlink: {
                        required: true
                    },
                },

                 messages: { // custom messages for radio buttons and checkboxes

                    mainname: {
                        required: "此欄位必填"
                    },

                    subname: {
                        required: "此欄位必填"
                    },

                    icon: {
                        required: "此欄位必填"
                    },

                    code: {
                        required: "此欄位必填"
                    },

                    frontlink: {
                        required: "此欄位必填"
                    },

                    backlink: {
                        required: "此欄位必填"
                    },
                },

                invalidHandler: function (event, validator) { //display error alert on form submit              
                    success1.hide();
                    error1.show();
                    App.scrollTo(error1, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.help-inline').removeClass('ok'); // display OK icon
                        $(element)
                        .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
                    },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                    },

                    success: function (label) {
                        label
                        .addClass('valid').addClass('help-inline ok') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                },

                submitHandler: function (form) {
                    error1.hide();
                    document.getElementById("submitform").submit();
                }
            });

         }

     };

 }();