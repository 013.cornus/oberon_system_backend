$().ready(function() {
	$( "#createForm" ).validate({
		// 參數設定
		rules:{
			employee:{
				required:true,
				notEqual: ''
			},
			competence:{
				required:true,
				notEqual: ''
			},
			occupation:{
				required:true,
				notEqual: ''
			},
			account:{
				required:true,
			}
		},
		messages:{
			employee: {
				required: "<span style='color:red;'>必填欄位！請輸入用戶名稱</span>",
				notEqual: "<span style='color:red;'>必填欄位！請輸入用戶名稱</span>" 
			},
			competence: {
				required: "<span style='color:red;'>必填欄位！請輸入權限</span>",
				notEqual: "<span style='color:red;'>必填欄位！請輸入權限</span>" 
			},
			occupation: {
				required: "<span style='color:red;'>必填欄位！請輸入權限</span>",
				notEqual: "<span style='color:red;'>必填欄位！請輸入權限</span>" 
			},
			account: {
				required: "<span style='color:red;'>必填欄位！請輸入用戶帳號</span>",
			}
		},
		// errorPlacement: function( error, element ) {
		// 	error.appendTo(element.siblings("#errorMsg"));
		// 	return true;
		// }
	});
});
jQuery.validator.addMethod("notEqual", function(value, element, param) {
	return this.optional(element) || value != param;
}, "Please specify a different (non-default) value");


$( "#updateForm" ).validate({
	// 參數設定
	rules:{
		password_1: {
			required: "<span style='color:red;'>必填欄位！請輸入密碼</span>",
		},
		password_2:{
			required: true,
			equalTo: '#password_1', 
			rangelength: [6, 20], 
		}
	},
	messages:{
		password_1: {
			required: "<span style='color:red;'>必填欄位！請輸入密碼</span>",
		},
		password_2: { 
			required: "<span style='color:red;'>必填欄位！請輸入密碼</span>",
			equalTo: "<span style='color:red;'>密碼不一致</span>", 
			rangelength: "<span style='color:red;'>密碼長度必須介於 {0} 至 {1} 之間！</span>", 
		} 
	},
	errorPlacement: function( error, element ) {
		error.appendTo(element.siblings("#errorMsg"));
		return true;
	}
});
