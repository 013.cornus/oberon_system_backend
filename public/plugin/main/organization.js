

$( "#submitform" ).validate({
		// 參數設定
		rules:{
			organization:{
				required:true,
			},
			principal:{
				required:true,
			},
			parentunit:{
				required:true,
			},
			referrer:{
				required:true,
			},
			address:{
				required:true,
			},
			paddressdetail:{
				required:true,
			},
			tel:{
				required:true,
			},
			fax:{
				required:true,
			}
		},
		messages:{
			organization: {
				required: "請輸入營業處名稱！",
			},
			principal:{
				required: "請選擇副總經理！",
			},
			parentunit:{
				required: "請選擇母單位！",
			},
			referrer:{
				required: "請選擇推薦人！",
			},
			address:{
				required: "請選擇地址！",
			},
			paddressdetail:{
				required: "請輸入地址！",
			},
			tel:{
				required: "請輸入電話！",
			},
			fax:{
				required: "請輸入傳真！",
			}
		},
		errorPlacement: function( error, element ) {
			error.appendTo(element.siblings("#occupationMsg"));
			return true;
		}
	});

